<?php


/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

use \XDev\Dev\NativeAPI\NativeAPIRequestWrapper as RequestWrapper;

$args = $GLOBALS['argv'];

if (isset($args[2]) && $args[2] === 'native-api-call') {

    RequestWrapper::preprocessRequest($args);

    try {
        chdir(\XDev::getSoftwareDir());

        eval(RequestWrapper::getSoftwareConnector()->getConnectorCode());

        // Don't declare any global var below

        RequestWrapper::postprocessRequest();

    } catch (\Exception $e) {
        \XDev\Core\JsonAPI::addError($e->getMessage());
    }

    exit;
}