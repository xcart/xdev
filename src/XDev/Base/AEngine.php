<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */
namespace XDev\Base;

use XDev\EM;

/**
 * Class AEngine
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
abstract class AEngine {

    public static function getEngineCode() {
        return '';
    }

    final public function getCode() {
        return static::getEngineCode();
    }

    public static function detect($softwareDir) {
        return false;
    }

    /*
     * Gets a list of base version for version detector
     */
    abstract protected function getBaseVersions();

    public function detectVersion() {

        $version = false;

        foreach ($this->getBaseVersions() as $v) {
            EM::setVersion($v);

            try {
                $version = EM::get('API')->getVersion();

                if ($version) {
                    return $version;
                }

            } catch (\Exception $e) {
                throw new \Exception($e->getMessage(), 0, $e);
            }

        }

        return $version;
    }

}
