<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */
namespace XDev\Base;

/**
 * Class AAPI
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
abstract class AAPI {

    abstract public function getVersion();

    abstract public function getSoftwareName();

    abstract public function getSoftwareHttpHost();

    abstract public function getSoftwareHttpsHost();

    abstract public function getSoftwareWebDir();

    abstract public function testHttps();

    public function getSoftwareWebBaseURL($is_secure = false)
    {
        return ($is_secure ? 'https://' . $this->getSoftwareHttpsHost() : 'http://' . $this->getSoftwareHttpHost()) . $this->getSoftwareWebDir();
    }

}
