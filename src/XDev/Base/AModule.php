<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Base;

use Symfony\Component\Console\Exception\LogicException;

/**
 * Class AModule
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
abstract class AModule extends \XDev\Core\Singleton
{

    private $packageName;

    public function setPackageName($package_name)
    {
        if ($this->packageName) {
            throw new LogicException(sprintf('Package name has been already set (%s)', $this->packageName));
        }

        $this->packageName = $package_name;
    }

    final public function getPackageName()
    {
        return $this->packageName;
    }

    public function init() {
        
    }

    public function isBundle()
    {
        return false;
    }

    abstract function getModuleDescription();

    final public function getApplication()
    {
        return \XDev\Application::getInstance();
    }

    final public function getModuleDir()
    {
        $reflection = new \ReflectionClass(get_class($this));

        return dirname($reflection->getFileName());
    }
    
}
