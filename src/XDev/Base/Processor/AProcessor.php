<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Base\Processor;

use XDev\EM;
use XDev\Utils\Filesystem;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use XDev\Core\Symfony\Console\Helper as ConsoleHelper;

/**
 * Class Processor
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
abstract class AProcessor
{
    const STATUS_NONE           = 'NONE';
    const STATUS_COMPLETED      = 'C';
    const STATUS_FAILED         = 'F';
    const STATUS_INCOMPLETED    = 'I';

    private $options;
    private $cacheDir;
    private $cache = [];
    private $status = self::STATUS_NONE;
    private $steps = [];
    private $startTime;
    private $startTimeOffset = 0;
    private $ignoredSteps = [];

    public function __construct($options = [])
    {
        $this->options = $options;
    }

    abstract protected function getProcessId();

    protected function getTitle() {
        return '';
    }

    public function setCacheDir($dir)
    {
        $this->cacheDir = $dir;

        $this->loadCache();
    }

    protected function getCacheFilename()
    {
        return $this->cacheDir . \XDev::DS . $this->getProcessId() . '.p';
    }

    protected function saveCache()
    {
        $this->cache['status'] = $this->status;

        foreach ($this->getSteps() as $k => $step) {
            $this->cache['steps'][$k] = $step->getDataForSave();
        }

        Filesystem::dumpFile($this->getCacheFilename(), serialize($this->cache));
    }

    protected function loadCache()
    {
        if (Filesystem::exists($this->getCacheFilename())) {
            $this->cache = unserialize(file_get_contents($this->getCacheFilename()));
            $this->status = $this->cache['status'];
        }
    }

    public function getStartTime()
    {
        return $this->startTime;
    }

    public function setStartTimeOffset($offset)
    {
        $offsetTime = \DateTime::createFromFormat('H:i:s', $offset);
        $zeroTime = \DateTime::createFromFormat('H:i:s', '00:00:00');

        $this->startTimeOffset = $offsetTime->diff($zeroTime);
    }

    public function setIgnoredSteps($steps)
    {
        $this->ignoredSteps = $steps;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getOption($key)
    {
        return $this->options[$key];
    }

    protected function defineSteps()
    {
        return [];
    }

    public function getOutput()
    {
        return \XDev\Application::getInstance()->getCurrentOutput();
    }

    public function getInput()
    {
        return \XDev\Application::getInstance()->getCurrentInput();
    }

    public function shutdown()
    {
        $this->saveCache();
    }

    public function getHelper($name)
    {
        return \XDev\Application::getInstance()->getSCApp()->getHelperSet()->get($name);
    }

    public function getStep($key)
    {
        return $this->getSteps()[$key];
    }

    protected function prepareSteps()
    {
        $steps = [];

        foreach ($this->defineSteps() as $k => $stepClass) {

            if ($this->ignoredSteps && in_array($k, $this->ignoredSteps)) {
                continue;
            }

            $savedStepData = isset($this->cache['steps'][$k]) ? $this->cache['steps'][$k] : null;

            if (class_exists($stepClass)) {
                $steps[$k] = new $stepClass($this, $savedStepData);

            } else {
                $steps[$k] = EM::get($stepClass, $this, $savedStepData);
            }
        }

        return $steps;
    }

    public function getSteps()
    {
        if (!$this->steps) {
            $this->steps = $this->prepareSteps();
        }

        return $this->steps;
    }

    public function formatElapsedTime($short = true)
    {
        $now = new \DateTime();

        $startTime = clone $this->startTime;

        if ($this->startTimeOffset) {
            $startTime->add($this->startTimeOffset);
        }

        $diff = $startTime->diff($now);

        $hours = $diff->format('%H');

        return $hours > 0 || !$short ? $diff->format('%H:%I:%S') : $diff->format('%I:%S');
    }

    public function renderStep($step)
    {
        $this->getOutput()->write("\x0D");
        $this->getOutput()->write("\x1B[2K");

        $process_title = $step->getProcessTitle();
        $process_title = $process_title ? $process_title : $this->getTitle();
        $process_title = $this->formatElapsedTime() . ' ' . $process_title;

        $this->getOutput()->write(sprintf(
           '%s%s',
            $process_title ? '<fg=yellow;options=bold>[' . $process_title . ']</> ' : '',
            $step->getTitle()
        ));

        if ($step->getStatus()) {
            $color = 'green';

            if (!$step->isComplete()) {
                $color = 'red';
            }

            $this->getOutput()->write(' - <fg=' . $color . ';options=bold>[' . $step->getDisplayedStatus() . ']</>');

            if ($step->isSkipped()) {
                $this->getOutput()->write('<fg=yellow;options=bold> [SKIPPED]</>');
            }
        } else {
            $this->getOutput()->write('...');
        }

    }

    public function run()
    {
        $this->startTime = new \DateTime();

        register_shutdown_function([$this, 'shutdown']);

        $this->getOutput()->writeln('');

        $this->status = self::STATUS_INCOMPLETED;

        foreach ($this->getSteps() as $k => $step)
        {
            try {

                if (
                    !$step->isComplete()
                    && !$step->isSkipped()
                    || !$step->isOneTimeRun()
                ) {
                    $step->init();

                    if (!$step->isHidden()) {
                        $this->renderStep($step);
                    }

                    ConsoleHelper::startNewParagraph();

                    $step->run();

                    $step->complete();
                }

            } catch (\Exception $e) {
                $step->fail();

                $this->renderStep($step);

                $this->getOutput()->writeln('');

                $formatterHelper =  $this->getHelper('formatter');

                $message = preg_replace('#\\x1b[[][^A-Za-z]*[A-Za-z]#', '', $e->getMessage());
                $message = trim($message);
                $message = explode("\n", $message);

                foreach ($message as $k => $v) {
                    $message[$k] = trim($v);
                    if (!$message[$k]) {
                        unset($message[$k]);
                    }
                }

                $formattedBlock = $formatterHelper->formatBlock($message, 'error', true);
                ConsoleHelper::writeln($formattedBlock);
                ConsoleHelper::writeln();

                if (!ConsoleHelper::askYesNo('Continue anyway?')) {

                    ConsoleHelper::writeln('Operation canceled');
                    ConsoleHelper::writeln();

                    $this->status = self::STATUS_FAILED;
                    return;
                }

                $step->skip();

            }

            if (!$step->isHidden()) {
               $this->renderStep($step);
               $this->getOutput()->writeln('');
            }

        }

        $this->status = self::STATUS_COMPLETED;

        $this->getOutput()->writeln('');

        $this->finalize();
    }

    protected function finalize()
    {
        
    }
}
function hex_dump($string, array $options = null) {
    if (!is_scalar($string)) {
        throw new InvalidArgumentException('$string argument must be a string');
    }
    if (!is_array($options)) {
        $options = array();
    }
    $line_sep       = isset($options['line_sep'])   ? $options['line_sep']          : "\n";
    $bytes_per_line = @$options['bytes_per_line']   ? $options['bytes_per_line']    : 16;
    $pad_char       = isset($options['pad_char'])   ? $options['pad_char']          : '.'; # padding for non-readable characters

    $text_lines = str_split($string, $bytes_per_line);
    $hex_lines  = str_split(bin2hex($string), $bytes_per_line * 2);

    $offset = 0;
    $output = array();
    $bytes_per_line_div_2 = (int)($bytes_per_line / 2);
    foreach ($hex_lines as $i => $hex_line) {
        $text_line = $text_lines[$i];
        $output []=
            sprintf('%08X',$offset) . '  ' .
            str_pad(
                strlen($text_line) > $bytes_per_line_div_2
                ?
                    implode(' ', str_split(substr($hex_line,0,$bytes_per_line),2)) . '  ' .
                    implode(' ', str_split(substr($hex_line,$bytes_per_line),2))
                :
                implode(' ', str_split($hex_line,2))
            , $bytes_per_line * 3) .
            '  |' . preg_replace('/[^\x20-\x7E]/', $pad_char, $text_line) . '|';
        $offset += $bytes_per_line;
    }
    $output []= sprintf('%08X', strlen($string));
    echo @$options['want_array'] ? $output : join($line_sep, $output) . $line_sep;
}