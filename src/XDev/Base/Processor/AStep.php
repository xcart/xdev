<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Base\Processor;

use \XDev\Base\Processor\AProcessor;
/**
 * Class AStep
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
abstract class AStep
{
    protected $processor;
    protected $status;
    protected $skipped;

    public function __construct(AProcessor $processor, $data = [])
    {
        $this->processor = $processor;

        if ($data) {
            foreach ($this->defineStoredProperties() as $prop) {
                $this->{$prop} = $data[$prop];
            }
        }
    }

    protected function getProcessor()
    {
        return $this->processor;
    }

    public function __call($method_name, $arguments)
    {
        if (method_exists($this, $method_name)) {
            return call_user_func_array([$this, $method_name], $arguments);

        } else {
            return call_user_func_array([$this->getProcessor(), $method_name], $arguments);
        }

    }

    public function complete()
    {
        return $this->status = AProcessor::STATUS_COMPLETED;
    }

    public function isComplete()
    {
        return $this->status === AProcessor::STATUS_COMPLETED;
    }

    public function fail()
    {
        return $this->status = AProcessor::STATUS_FAILED;
    }

    public function init()
    {
        return $this->status = '';
    }

    public function skip()
    {
        return $this->skipped = true;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function isSkipped()
    {
        return $this->skipped;
    }

    public function getDisplayedStatus()
    {
        switch ($this->status) {
            case AProcessor::STATUS_FAILED:
                return 'FAILED';

                break;

            case AProcessor::STATUS_COMPLETED:
                return 'OK';
                break;

        };
    }

    public function getTitle()
    {
        return '';
    }

    public function getProcessTitle()
    {
        return '';
    }

    function isHidden()
    {
        return false;
    }

    public function isOneTimeRun()
    {
        return true;
    }

    protected function getOption($name)
    {
        return $this->getProcessor()->getOption($name);
    }

    public function defineStoredProperties()
    {
        return [
            'status',
            'skipped',
        ];
    }

    public function getDataForSave()
    {
        $data = [];

        foreach ($this->defineStoredProperties() as $propName) {
            $data[$propName] = $this->{$propName};
        }

        return $data;
    }

    abstract public function run();
}
