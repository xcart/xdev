<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Base\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Exception\RuntimeException;

/**
 * Class ACommand
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
abstract class ACommand extends Command
{
    protected function configure()
    {
        parent::configure();

        if ($this->isJsonOutputEnabled()) {
            $this
                ->addOption(
                    'json-output',
                    null,
                    InputOption::VALUE_OPTIONAL,
                    'JSON output format',
                    false
                )
            ;
        }
    }

    protected function isJsonOutputEnabled()
    {
        return false;
    }

    public function run(InputInterface $input, OutputInterface $output)
    {
        \XDev\Application::getInstance()->setCurrentInput($input);
        \XDev\Application::getInstance()->setCurrentOutput($output);

        parent::run($input, $output);

        if ($input->hasOption('json-output') && $input->getOption('json-output') !== false) {
            \XDev\Core\JsonAPI::outputFlush();
        }

    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        
        if ($input->hasOption('json-output') && $input->getOption('json-output') !== false) {
            $output->setVerbosity(OutputInterface::VERBOSITY_QUIET);
        }

        parent::initialize($input, $output);
    }
}
