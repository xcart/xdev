<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Base\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\RuntimeException;

/**
 * Class ASoftwareRelative
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
abstract class ASoftwareRelative extends ACommand
{
    protected function isInitRequired()
    {
        return true;
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

        if ($this->isInitRequired() && !\XDev::isInitialized()) {
            throw new RuntimeException('X-DEV Tools are not initialized. Please use \'xdev init\' command.');
        }

        if (\XDev::getDetectSoftwareError()) {
            throw new RuntimeException(\XDev::getDetectSoftwareErrorMessage());
        }

    }
}
