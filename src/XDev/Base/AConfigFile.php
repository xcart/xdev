<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Base;

use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Yaml\Yaml;

use XDev\Utils\Filesystem;

/**
 * Class AConfigFile
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
abstract class AConfigFile extends \XDev\Core\Singleton
{
    const FILE_EXT = 'config.yaml';
    const YAML_INLINE_DEPTH = 3;

    protected $params = [];

    protected $configFileExists = false;

    abstract protected function getConfigName();

    protected function getDir()
    {

        if (\XDev\Utils\Git::isRepoInitialized()) {
            return \XDev\Config::getXDevCurrentInstanceDir();
        }

        return \XDev\Config::getXDevDir();
    }

    abstract protected function getDefaultConfigData();

    public function getConfigFilename()
    {
        return $this->getConfigName() . '.' . static::FILE_EXT;
    }

    public function getConfigFilenameFull()
    {
        return $this->getDir() . DIRECTORY_SEPARATOR . $this->getConfigFilename();
    }

    public function configFileExists()
    {
        return $this->configFileExists;
    }

    public function save() {
        Filesystem::dumpFile($this->getConfigFilenameFull(), Yaml::dump($this->params, self::YAML_INLINE_DEPTH));

        $this->configFileExists = true;
    }

    public function load()
    {
        $configFile = $this->getConfigFilenameFull();

        if (Filesystem::exists($configFile)) {
            $data = Yaml::parseFile($configFile);

            $this->params = $data;

            $this->configFileExists = true;

            return true;

        } else {
           $this->configFileExists = false;
        }

        return false;
    }

    public function getParam($param) 
    {
        return isset($this->params[$param]) ? $this->params[$param] : null;
    }

    public function setParam($param, $value)
    {
        $this->params[$param] = $value;

        return $this;
    }

    public function getOptionKeys()
    {
        $r = new \ReflectionClass($this);

        foreach($r->getConstants() as $k => $v) {
            if (strpos($k, 'PARAM_') === 0) {
                $result[] = $v;
            }
        }

        return $result;
    }

    public function __construct()
    {
        if (!$this->load()) {
            $this->params = $this->getDefaultConfigData();
        };
    }

}
