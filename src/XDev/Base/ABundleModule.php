<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Base;

use Symfony\Component\Console\Exception\LogicException;

/**
 * Class ABundleModule
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
abstract class ABundleModule extends AModule
{
    const DIR_COMPILED = 'bin';

    public function isBundle()
    {
        return true;
    }

    public function getCompiledScriptsDir()
    {
        return $this->getModuleDir() . \XDev::DS . self::DIR_COMPILED;
    }
    
}
