<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\NativeAPI;

/**
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
interface ISoftwareConnector
{
    
    function getConnectorCode();
}
