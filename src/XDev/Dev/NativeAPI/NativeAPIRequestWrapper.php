<?php


/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\NativeAPI;

use XDev\Application;
use XDev\Core\JsonAPI;
use XDev\EM;
/**
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 *
 */
class NativeAPIRequestWrapper
{
    const ERROR_FILENAME = 'php-native-api-error.log';

    private static $api;
    private static $softwareConnector;
    private static $params;
    private static $isVerbose = false;

    public static function getSoftwareConnector() {
        return self::$softwareConnector;
    }

    public static function getPHPErrorsFilename() {
        return \XDev\Config::getXDevTmpDir() . \XDev::DS . 'php-native-api-error.log';
    }

    public static function shutdown() {
        $last_error = error_get_last();

        if (
            $last_error['type']    === E_ERROR
            || $last_error['type'] === E_COMPILE_ERROR
            || $last_error['type'] === E_CORE_ERROR
            || $last_error['type'] === E_RECOVERABLE_ERROR
            || $last_error['type'] === E_PARSE 

        ) {

            fwrite(STDERR, 'PHP Fatal error: ' . $last_error['message'] . ' in ' . $last_error['file'] . ' on line ' . $last_error['line']);

       }

        $output = ob_get_clean();

        if ($output && self::$isVerbose) {
            JsonAPI::addDebug("\n" . $output);
        }

        $errors = file_get_contents(self::getPHPErrorsFilename());

        if ($errors && self::$isVerbose) {
            JsonAPI::addDebug("\n" . $errors);
        }

        if (JsonAPI::getStatus() != JsonAPI::STATUS_OK) {
            JsonAPI::addError('Software connector error');
        }

        \XDev\Core\JsonAPI::outputFlush();

    }
    public static function preprocessRequest($args)
    {
        unlink(self::getPHPErrorsFilename()); // TODO use when verbose

        if (!file_exists(\XDev\Config::getXDevTmpDir())) {
            mkdir(\XDev\Config::getXDevTmpDir(), 0700, true);
        }

        ini_set('display_errors', '0');
        ini_set('error_log', self::getPHPErrorsFilename());

        // Capture all script output not to breake JSON format
        ob_start();

        register_shutdown_function([get_called_class(), 'shutdown']);

        $command = $args[3];

        $parts = explode(':', $command);

        if (count($parts) !== 2) {
            JsonAPI::addError('Invalid call, expected class:method');

            exit;
        }

        $api_namespace = 'API\\'. ucfirst($parts[0]) . '\\' . ucfirst($parts[1]);

        $_options = array_slice($args, 4);

        $options = [];
        $params = [];

        foreach ($_options as $option) {

            if (strpos($option, '--params=') === 0) {

                $option_value = substr($option, strlen('--params='));

                if ($option_value) {
                    if (($params = @unserialize($option_value)) === false) {
                        JsonAPI::addError('Invalid params format');

                        exit;
                    }

                }

                continue;
            }

            $tmp = explode('=', $option);

            $option_name = trim($tmp[0]);
            $option_value = trim($tmp[1]);

            if (preg_match('/^[\"](.?)[\"]$/', $option_value, $m)) {
                $option_value = $m[0];

            } elseif (preg_match("/^[\\\'](.?)[\\\']$/", $option_value, $m)) {
                $option_value = $m[0];
            }

            if (strpos($option_name, '--') !== 0) {
                JsonAPI::addError(sprintf('Option name should start with \'--\''));
                exit;
            }

            $option_name = ltrim($option_name, '-');

            if (!$option_name) {
                JsonAPI::addError(sprintf('Invalid option name'));
                exit;
            }

            $options[$option_name] = $option_value;

        }

        if (!isset($options['engine']) || !$options['engine']) {
            JsonAPI::addError(sprintf('Please specify --engine option'));
            exit;
        }

        if (!isset($options['version']) || !$options['version']) {
            JsonAPI::addError(sprintf('Please specify --version option'));
            exit;
        }

        $version = $options['version'];
        $engine_code = $options['engine'];
        self::$isVerbose = isset($options['verbose']);

        EM::setEngineCode($engine_code);
        EM::setVersion($version);

        try {
            self::$api = EM::get('NativeAPI\\' . $api_namespace);

        } catch (VMException $e) {
            JsonAPI::addError(sprintf('Invalid API call \'%s\'', $command));

            if ($e->getErrorCode() !== VersionManager::ERROR_NO_CLASS) {
                JsonAPI::addError("\n" . $e->getMessage());
            }

            exit;
        }

        self::$softwareConnector = EM::get('NativeAPI\SoftwareConnector');

        self::$params = $params;

    }

    public static function postprocessRequest()
    {
        JsonAPI::setStatus(JsonAPI::STATUS_OK);

        error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);

        ini_set('display_errors', '0');

        self::$api->process(self::$params);

    }

}