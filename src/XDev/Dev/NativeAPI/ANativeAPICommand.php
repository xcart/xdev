<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\NativeAPI;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use \XDev\Dev\NativeAPI\NativeAPI;

/**
 * Test command
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
abstract class ANativeAPICommand extends \XDev\Base\Command\ASoftwareRelative
{

    protected function call($method, $params, OutputInterface $output)
    {
        $result = NativeAPI::call($method, $params, $output->isVerbose());

        if ($result['debug']) {
            $output->writeln('');
            $formatterHelper = $this->getHelper('formatter');
            $outputStyle = new OutputFormatterStyle('black', 'white');
            $output->getFormatter()->setStyle('warning', $outputStyle);

            $formattedBlock = $formatterHelper->formatBlock($result['debug'], 'warning');
            $output->writeln($formattedBlock);

        }

        if ($result['warnings']) {
            $output->writeln('');
            $formatterHelper = $this->getHelper('formatter');
            $outputStyle = new OutputFormatterStyle('yellow');
            $output->getFormatter()->setStyle('warning', $outputStyle);

            $formattedBlock = $formatterHelper->formatBlock($result['warnings'], 'warning');
            $output->writeln($formattedBlock);

        }

        if ($result['errors']) {
            throw new RuntimeException(implode("\n", $result['errors']));
        }

        return $result;
    }

}
