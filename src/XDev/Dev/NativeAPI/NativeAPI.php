<?php


/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\NativeAPI;

/**
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 *
 */
class NativeAPI
{
    public static function call($method, $params, $is_verbose) {

        $engine_code = \XDev::getSoftwareEngine()->getEngineCode();
        $version = \XDev::getSoftwareVersion();

        $command = 'cd ' . \XDev::getSoftwareDir() . '; ';
        
        $entry_point = \XDev\Application::getinstance()->getEntryPointScript();

        $php_str = \XDev\Dev\Config\Main::getInstance()->getPhpPath();

        $command .=  "$php_str $entry_point --xdev-protected-mode native-api-call $method --engine=$engine_code --version=$version";

        if ($is_verbose) {
            $command .= ' --verbose=1';
        }

        if ($params) {
            $command .= ' --params=' . escapeshellarg(serialize($params));
        }

        $result = \XDev\Utils\Shell::exec($command);

        $result = \XDev\Core\JsonAPI::parseOutput($result);

        return $result;
    }
}