<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Config;

/**
 * Class Repos
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class LInstance extends \XDev\Base\AConfigFile
{
    const PARAM_HTTP_HOST       = 'http_host';
    const PARAM_WEB_DIR         = 'web_dir'; 

    protected function getConfigName()
    {
        return 'linstance';
    }

    public function getHttpHost() {
        return $this->getParam(self::PARAM_HTTP_HOST);
    }

    public function getWebDir()
    {
        return $this->getParam(self::PARAM_WEB_DIR);
    }

    public function setHttpHost($value) {
        $this->setParam(self::PARAM_HTTP_HOST, $value);
    }

    public function setWebDir($value) {
        $this->setParam(self::PARAM_WEB_DIR, $value);
    }

    protected function getDefaultConfigData()
    {
        return [
            self::PARAM_HTTP_HOST    => '',
            self::PARAM_WEB_DIR      => '',
        ];
    }
}
