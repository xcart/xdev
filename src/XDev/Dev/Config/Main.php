<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Config;

/**
 * Class Main
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Main extends \XDev\Base\AConfigFile
{
    const FILE_EXT = 'yaml';

    const DEFAULT_DEV_BRANCH    = 'main-dev';
    const PARAM_WEB_ROOT_DIR    = 'web_root_dir';
    const PARAM_DEPLOY_DIR      = 'deploy_dir';
    const PARAM_HTTP_HOST       = 'http_host';
    const PARAM_DB_HOST         = 'db_host';
    const PARAM_DB_SOCKET       = 'db_socket';
    const PARAM_DB_PORT         = 'db_port';
    const PARAM_DB_USER         = 'db_user';
    const PARAM_DB_PASS         = 'db_pass';
    const PARAM_DB_NAME         = 'db_name';
    const PARAM_PHP_PATH        = 'php_path';

    protected function getConfigName()
    {
        return 'config';
    }

    protected function getDir()
    {
        return \XDev\Config::getGlobalConfigDir();
    }

    protected function getDefaultConfigData()
    {
        return [
            self::PARAM_WEB_ROOT_DIR    => '/var/www/vhosts/{project}-{branch}',
            self::PARAM_DEPLOY_DIR      => '{web_root_dir}',
            self::PARAM_HTTP_HOST       => '{project}-{branch}.dev.local',
            self::PARAM_DB_HOST         => 'localhost',
            self::PARAM_DB_NAME         => '{project}_{branch}',
            self::PARAM_DB_SOCKET       => '',
            self::PARAM_DB_PORT         => '',
            self::PARAM_DB_USER         => '',
            self::PARAM_DB_PASS         => '',
            self::PARAM_PHP_PATH        => 'php',
        ];
    }

    public function getWebRootDir() {
        return $this->getParam(self::PARAM_WEB_ROOT_DIR);
    }

    public function setWebRootDir($value) {
        return $this->setParam(self::PARAM_WEB_ROOT_DIR, $value);
    }

    public function getDeployDir() {
        return $this->getParam(self::PARAM_DEPLOY_DIR);
    }

    public function setDeployDir($value) {
        return $this->setParam(self::PARAM_DEPLOY_DIR, $value);
    }

    public function getHttpHost() {
        return $this->getParam(self::PARAM_HTTP_HOST);
    }

    public function setHttpHost($value) {
        return $this->getParam(self::PARAM_HTTP_HOST, $value);
    }

    public function getDbName() {
        return $this->getParam(self::PARAM_DB_NAME);
    }

    public function setDbName() {
        return $this->setParam(self::PARAM_DB_NAME, $value);
    }

    public function getDbHost() {
        return $this->getParam(self::PARAM_DB_HOST);
    }

    public function setDbHost($value) {
        return $this->setParam(self::PARAM_DB_HOST, $value);
    }

    public function getDbSocket() {
        return $this->getParam(self::PARAM_DB_SOCKET);
    }

    public function setDbSocket($value) {
        return $this->setParam(self::PARAM_DB_SOCKET, $value);
    }

    public function getDbPort() {
        return $this->getParam(self::PARAM_DB_PORT);
    }

    public function setDbPort($value) {
        return $this->setParam(self::PARAM_DB_PORT, $value);
    }

    public function getDbUser() {
        return $this->getParam(self::PARAM_DB_USER);
    }

    public function setDbUser($value) {
        return $this->setParam(self::PARAM_DB_USER, $value);
    }

    public function getPhpPath() {
        return $this->getParam(self::PARAM_PHP_PATH);
    }

    public function setPhpPath($value) {
        return $this->setParam(self::PARAM_PHP_PATH, $value);
    }

    public function getDbPassword() {
        return $this->getParam(self::PARAM_DB_PASS);
    }

    public function setDbPassword($value) {
        return $this->getParam(self::PARAM_DB_PASS, $value);
    }

}
