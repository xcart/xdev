<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Config;

/**
 * Class Hosts
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Hosts extends \XDev\Base\AConfigFile
{
    const PARAM_SSH_PRIVATE_KEY         = 'SSH_privateKey';
    const PARAM_SSH_USE_MULTIPLEXING    = 'SSH_useMultiplexing';

    protected function getConfigName()
    {
        return 'hosts';
    }

    public function getHostParam($host, $param)
    {
        $params = isset($this->params[$host]) ? $this->params[$host] : $this->getHostDefaultParams();

        return isset($params[$param]) ? $params[$param] : null;
    }

    public function setHostParam($host, $param, $value)
    {
        if (!isset($this->params[$host])) {
            $this->params[$host] = $this->getHostDefaultParams();
        }

        $this->params[$host][$param] = $value;
    }

    protected function getDir()
    {
        return \XDev\Config::getGlobalConfigDir();
    }

    protected function getHostDefaultParams()
    {
        return [
            self::PARAM_SSH_USE_MULTIPLEXING => true
        ];
    }

    protected function getDefaultConfigData()
    {
        return [];
    }
}
