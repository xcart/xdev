<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Config;

/**
 * Class Hubs
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Hubs extends \XDev\Base\AConfigFile
{
    protected function getConfigName()
    {
        return 'hubs';
    }

    public function getPrivateKeyFilename($host, $vendor)
    {
        $key = $host . '/' . $vendor;

        return isset($this->params[$key]) ? $this->params[$key] : null;
    }

    public function setPrivateKeyFilename($host, $vendor, $value)
    {
        $key = $host . '/' . $vendor;

        $this->params[$key] = $value;
    }

    protected function getDir()
    {
        return \XDev\Config::getGlobalConfigDir();
    }

    protected function getDefaultConfigData()
    {
        return [];
    }
}
