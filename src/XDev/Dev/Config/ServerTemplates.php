<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Config;

/**
 * Class ServerTemplates
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class ServerTemplates extends \XDev\Base\AConfigFile
{
    const T_PARAM_WEB_ROOT_DIR    = 'web_root_dir';
    const T_PARAM_DEPLOY_DIR      = 'deploy_dir';
    const T_PARAM_HTTP_HOST       = 'http_host';
    const T_PARAM_SSH_HOST        = 'ssh_host';
    const T_PARAM_SSH_PORT        = 'ssh_port';
    const T_PARAM_SSH_USER        = 'ssh_user';
    const T_PARAM_SSH_PRIVATE_KEY = 'ssh_private_key';
    const T_PARAM_DB_HOST         = 'db_host';
    const T_PARAM_DB_PORT         = 'db_port';
    const T_PARAM_DB_USER         = 'db_user';
    const T_PARAM_DB_PASS         = 'db_pass';
    const T_PARAM_DB_SOCKET       = 'db_socket';
    const T_PARAM_HOME_DIR        = 'home_dir';

    protected function getConfigName()
    {
        return 'servers';
    }

    protected function getDir()
    {
        return \XDev\Config::getGlobalConfigDir() . '/templates/deploy/remote';
    }

    protected function getTemplateDefaultParams()
    {
        return [
            self::T_PARAM_WEB_ROOT_DIR    => '',
            self::T_PARAM_DEPLOY_DIR      => '',
            self::T_PARAM_HTTP_HOST       => '',
            self::T_PARAM_SSH_HOST        => '',
            self::T_PARAM_SSH_PORT        => '',
            self::T_PARAM_SSH_USER        => '',
            self::T_PARAM_DB_HOST         => '',
            self::T_PARAM_DB_PORT         => '',
            self::T_PARAM_DB_USER         => '',
            self::T_PARAM_DB_PASS         => '',
            self::T_PARAM_DB_SOCKET       => '',
            self::T_PARAM_HOME_DIR        => '',
        ];
    }

    public function templateExists($template) {
        return isset($this->params[$template]);
    }

    public function getTemplateParam($template, $param)
    {
        $params = isset($this->params[$template]) ? $this->params[$template] : [];

        return isset($params[$param]) ? $params[$param] : null;
    }

    public function setTemplateParam($template, $param, $value)
    {
        if (!isset($this->params[$template])) {
            $this->params[$template] = [];
        }

        $this->params[$template][$param] = $value;

        return $this;
    }

    protected function getDefaultConfigData()
    {
        return [];
    }
}
