<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev;

/**
 * Class Main
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Main
{
    public static function init()
    {
        $main = \XDev\Dev\Config\Main::getInstance();

        if (!$main->configFileExists()) {
            $main->save();
        }
    }
}
