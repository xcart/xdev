<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Exception\RuntimeException;

/**
 * Class Info
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Config extends \XDev\Base\Command\ACommand
{

    protected function getConfig()
    {
        return \XDev\Dev\Config\Main::getInstance();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        foreach ($this->getConfig()->getOptionKeys() as $k => $v) {
            $this->addOption($v, null, InputOption::VALUE_OPTIONAL, null);
        }

        $this
            ->setName('config-set')
            ->setDescription('Global configuration setup')
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $configOptions = $this->getConfig()->getOptionKeys();

        $options = [];

        foreach ($input->getOptions() as $k => $v) {
            if ($v !== null && in_array($k, $configOptions)) {
                $options[$k] = $v;
            }
        }

        if (!$options) {
            throw new RuntimeException('Please specify at least one --key=value pair');
        }

        $flag_save = false;

        foreach ($options as $k => $v) {
            $this->getConfig()->setParam($k, $v);
            $flag_save = true;
        }

        if ($flag_save) {
            $this->getConfig()->save();
        }

    }
}
