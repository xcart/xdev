<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use XDev\Dev\Processor\RemoteDeploy;
use XDev\EM;

/**
 * DeployRemote
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class DeployRemote extends \XDev\Base\Command\ASoftwareRelative
{

    protected function configure()
    {
        $this
            ->setName('deploy-remote')
            ->addArgument('template', InputArgument::OPTIONAL, 'Server template')
            ->setDescription('deploys the local code to the remote server')
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $options = [
            RemoteDeploy::PARAM_TEMPLATE => $input->getArgument('template'),
        ];

        $processor = EM::get('Processor\RemoteDeploy', $options);

        $processor->setCacheDir(\XDev\Config::getXDevCacheDir() . \XDev::DS . 'Processor');

        \XDev\Utils\Shell::setTimeout(600);

        $processor->run();
    }
}
