<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Exception\RuntimeException;
use XDev\Utils\Filesystem;
use XDev\Utils\Git;
use XDev\EM;
use XDev\Dev\Config\Main as MainConfig;
use XDev\Dev\Processor\LocalDeploy;

/**
 * Class Deploy
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Deploy extends \XDev\Base\Command\ACommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('deploy')
            ->addArgument('repoUrl', InputArgument::REQUIRED, 'Repository URL')
            ->addArgument('branch', InputArgument::OPTIONAL, 'Branch name')
            ->addOption('instance',  '-i', InputOption::VALUE_OPTIONAL, 'Instance name')
            ->addOption('directory', '-d', InputOption::VALUE_OPTIONAL, 'Directory to deploy', false)
            ->addOption('no-sync', null, InputOption::VALUE_NONE, 'Skip sync with remote instance')
            ->addOption('db-dump-path', null, InputOption::VALUE_OPTIONAL, 'Path to database dump', false)
            ->addOption('start-time-offset', null, InputOption::VALUE_OPTIONAL, 'Process start time offset')
            ->addOption('get-status', null, InputOption::VALUE_NONE, 'Skip sync with remote instance')
            ->addOption('ignore-steps', null, InputOption::VALUE_OPTIONAL, 'Ignore certain steps (comma separated)')
            ->setDescription('Deploys the remote repository to the local server')
        ;
    }
    protected function throwException($message)
    {
        $input = \XDev\Application::getInstance()->getCurrentInput();
        $output = \XDev\Application::getInstance()->getCurrentOutput();

        if ($input->getOption('get-status')) {
            $output->write('Failed');
        }

        throw new RuntimeException($message);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $devConfig = MainConfig::getInstance();

        $parsed_url = Git::parseRepoUrl($input->getArgument('repoUrl'));

        $repo_url = $parsed_url['ssh_url'];
        $project = $parsed_url['project'];
        $project = str_replace('_', '-', $project);

        $branch = $input->getArgument('branch') ? $input->getArgument('branch') : MainConfig::DEFAULT_DEV_BRANCH;

        $remote_instance_name = $input->getOption('instance') ? $input->getOption('instance') : \XDev\Config\Main::INSTANCE_NAME_LIVE;

        $branch = $input->getArgument('branch') ? $input->getArgument('branch') : MainConfig::DEFAULT_DEV_BRANCH;

        $database = str_replace(['{project}', '{branch}'], [$project, $branch], $devConfig->getDbName());
        $database = str_replace('-', '_', $database);

        $http_host = str_replace(['{project}', '{branch}'], [$project, $branch], $devConfig->getHttpHost());

        $web_root_dir = rtrim(str_replace(['{project}', '{branch}'], [$project, $branch], $devConfig->getWebRootDir()), \XDev::DS);

        if (!$web_root_dir) {
            $this->throwException(sprintf(
                'Web root directory is not set.' . "\n" . 'Specify the %s in %s',
                MainConfig::PARAM_WEB_ROOT_DIR, $devConfig->getConfigFilenameFull()
            ));
        }

        if ($input->getOption('directory')) {
            $deploy_dir = $input->getOption('directory') != '.' ? $input->getOption('directory') : getcwd();

        } else {

            $deploy_dir = str_replace(
                ['{' . MainConfig::PARAM_WEB_ROOT_DIR . '}', '{project}', '{branch}'],
                [$web_root_dir, $project, $branch],
                $devConfig->getDeployDir()
            );
        }

        $deploy_dir = rtrim($deploy_dir, \XDev::DS);

        $web_dir = trim(Filesystem::makePathRelative($deploy_dir, $web_root_dir), \XDev::DS);
        $web_dir = str_replace(\XDev::DS, '/', $web_dir);

        if (strpos($web_dir, '..') === 0) {
            $this->throwException(sprintf(
                "Deploy directory %s must be a sub-directiory of web root %s",
                $deploy_dir, $web_root_dir
            ));

        } elseif ($web_dir == '.') {
            $web_dir = '';
        }

        $options = [
            LocalDeploy::PARAM_PROJECT_NAME => $project,
            LocalDeploy::PARAM_BRANCH       => $branch,
            LocalDeploy::PARAM_INSTANCE     => $remote_instance_name,
            LocalDeploy::PARAM_DEPLOY_DIR   => $deploy_dir,
            LocalDeploy::PARAM_NO_SYNC      => $input->getOption('no-sync'),
            LocalDeploy::PARAM_DB_DUMP_PATH => $input->getOption('db-dump-path'),
        ];

        if ($input->getOption('get-status')) {
           $processor = new \XDev\Dev\Processor\LocalDeploy($options);

           if ($processor->getStatus() == LocalDeploy::STATUS_FAILED) {
                $output->write('Failed');

            } else {
                $output->write('Completed');
            }

            return;
        }

        if (!Filesystem::exists($deploy_dir)) {
            Filesystem::mkdir($deploy_dir);
        }

        \XDev::setSoftwareDir($deploy_dir);
        chdir($deploy_dir);

        $is_first_run = Filesystem::isDirEmpty($deploy_dir);

        \XDev\Utils\Shell::setTimeout(600);

        if ($is_first_run) {

            \XDev\Utils\Shell::setTranslateOutput(true);

            $clone_command = '';

            if ($input->getOption('no-interaction')) {
                $clone_command .= 'GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"';
            }

            $clone_command .= ' git clone ' . $repo_url . ' ' . $deploy_dir;

            if (\XDev\Utils\Git::getGitVersion() >= '2.10') {
                $clone_command .= ' --progress';
            }

            \XDev\Utils\Shell::exec($clone_command);
            \XDev\Utils\Shell::setTranslateOutput(false);

            if (Git::isRemoteBranchFetched($branch)) {
                \XDev\Utils\Shell::exec('git checkout ' . $branch);

            } else {
                \XDev\Utils\Shell::exec('git checkout -b ' . $branch);
            }

            \XDev::getConfig('Local')->setCurrentInstance(\XDev\Config\Main::INSTANCE_NAME_LOCAL);
            \XDev::getConfig('Local')->save();

            $dbCfg = new \XDev\Config\Db();

            $dbCfg->setHost($devConfig->getDbHost());
            $dbCfg->setUser($devConfig->getDbUser());
            $dbCfg->setPassword($devConfig->getDbPassword());
            $dbCfg->setDatabase($database);
            $dbCfg->setPort($devConfig->getDbPort());
            $dbCfg->setSocket($devConfig->getDbSocket());

            $dbCfg->save();

            $localInstanceCfg = new \XDev\Dev\Config\LInstance();

            $localInstanceCfg->setHttpHost($http_host);
            $localInstanceCfg->setWebdir($web_dir);

            $localInstanceCfg->save();

            $remote_instance_dir = \XDev\Config::getXDevInstanceDir($remote_instance_name);
            $local_instance_dir = \XDev\Config::getXDevInstanceDir(\XDev\Config\Main::INSTANCE_NAME_LOCAL);

            \XDev\Utils\Shell::exec("cp $remote_instance_dir/*.* $local_instance_dir/");
        }

        \XDev::getConfig('Local')->load();

        if (!\XDev::getSoftwareEngine()) {
            \XDev::detectSoftware();

            if (\XDev::getDetectSoftwareError()) {
                $this->throwException(
                    "Destination path '$deploy_dir' is not an empty directory" . "\n"
                    . \XDev::getDetectSoftwareErrorMessage()
                );
            }

        }

        $instance = \XDev::getConfig('main')->getRemoteInstance($remote_instance_name);

        if (!$instance) {
            $this->throwException(sprintf('Instance \'%s\' does not exist', $this->getInstanceName()));
        }

        \XDev::loadHooks('deploy');

        $processor = EM::get('Processor\LocalDeploy', $options);

        $processor->setCacheDir(\XDev\Config::getXDevCacheDir() . \XDev::DS . 'Processor');

        if (
            !$is_first_run
            && (
                $processor->getStatus() === LocalDeploy::STATUS_NONE
                || Git::getRepoUrl() !== $repo_url
                || Git::getCurrentBranch() !== $branch
            )
        ) {
            $this->throwException(
                "Destination path '$deploy_dir' is not an empty directory"
            );
        }

        if ($input->getOption('start-time-offset')) {
            $processor->setStartTimeOffset($input->getOption('start-time-offset'));
        }

        if ($input->getOption('ignore-steps')) {
            $ignored_steps = explode(',', $input->getOption('ignore-steps'));

            $processor->setIgnoredSteps($ignored_steps);
        }

        $processor->run();
    }
}
