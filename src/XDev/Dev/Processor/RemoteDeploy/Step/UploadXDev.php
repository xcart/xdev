<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Processor\RemoteDeploy\Step;

use XDev\Base\Processor\AStep;

/**
 * Class TestSSHConnection
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class UploadXDev extends AStep
{

    public function getTitle()
    {
        return 'Uploading xdev.phar';
    }

    public function run()
    {
        $ssh = $this->getSSHConnection();

        $local_filename = \XDev\Application::getInstance()->getCompiledScriptPathXDev();
        $remote_filename = '~/xdev.phar';

        \XDev\Dev\Helper\SSH::upload($ssh, $local_filename, $remote_filename);
    }
}
