<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Processor\RemoteDeploy\Step;

use XDev\Base\Processor\AStep;
use XDev\Dev\Config\ServerTemplates;
use XDev\Core\Symfony\Console\Helper as ConsoleHelper;

/**
 * Class SetServerParams
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class SetServerParams extends AStep
{
    protected $webRootDir;
    protected $deployDir;
    protected $httpHost;
    protected $dbHost;
    protected $dbSocket;
    protected $dbPort;
    protected $dbUser;
    protected $dbPass;

    public function getTitle()
    {
        return 'Setting server params';
    }

    public function defineStoredProperties()
    {
        return array_merge(parent::defineStoredProperties(), [
            'webRootDir',
            'deployDir',
            'httpHost',
            'dbHost',
            'dbSocket',
            'dbPort',
            'dbUser',
            'dbPass',
        ]);
    }

    public function getWebRootDir()
    {
        if ($this->webRootDir === null) {
            $this->webRootDir = $this->getTemplateParam(ServerTemplates::T_PARAM_WEB_ROOT_DIR);
        }

        return $this->webRootDir;
    }

    public function getDeployDir()
    {
        if ($this->deployDir === null) {
            $this->deployDir = $this->getTemplateParam(ServerTemplates::T_PARAM_DEPLOY_DIR);
        }

        return $this->deployDir;
    }

    public function getHttpHost()
    {
        if ($this->httpHost === null) {
            $this->httpHost = $this->getTemplateParam(ServerTemplates::T_PARAM_HTTP_HOST);
        }

        return $this->httpHost;
    }

    public function getDbHost()
    {
        if ($this->dbHost === null) {
            $this->dbHost = $this->getTemplateParam(ServerTemplates::T_PARAM_DB_HOST);
        }

        return $this->dbHost;
    }

    public function getDbSocket()
    {
        if ($this->dbSocket === null) {
            $this->dbSocket = $this->getTemplateParam(ServerTemplates::T_PARAM_DB_SOCKET);
        }

        return $this->dbSocket;
    }

    public function getDbPort()
    {
        if ($this->dbPort === null) {
            $this->dbPort = $this->getTemplateParam(ServerTemplates::T_PARAM_DB_PORT);
        }

        return $this->dbPort;
    }

    public function getDbUser()
    {
        if ($this->dbUser === null) {
            $this->dbUser = $this->getTemplateParam(ServerTemplates::T_PARAM_DB_USER);
        }

        return $this->dbUser;
    }

    public function getDbPass()
    {
        if ($this->dbPass === null) {
            $this->dbPass = $this->getTemplateParam(ServerTemplates::T_PARAM_DB_PASS);
        }

        return $this->dbPass;
    }

    public function run()
    {
        if ($this->getWebRootDir() === null) {
            $this->webRootDir = ConsoleHelper::askForParam('Enter web root dir', ConsoleHelper::V_TYPE_NONE_EMPTY);
        }

        if ($this->getDeployDir() === null) {
            $this->deployDir = ConsoleHelper::askForParam('Enter deploy dir', ConsoleHelper::V_TYPE_NONE_EMPTY);
        }

        if ($this->getHttpHost() === null) {
            $this->httpHost = ConsoleHelper::askForParam('Enter http host', ConsoleHelper::V_TYPE_NONE_EMPTY);
        }

        if ($this->getDbHost() === null) {
            $this->dbHost = ConsoleHelper::askForParam('Enter db host');
        }

        if ($this->getDbSocket() === null) {
            $this->dbSocket = ConsoleHelper::askForParam('Enter db socket');
        }

        if ($this->getDbPort() === null) {
            $this->dbPort = ConsoleHelper::askForParam('Enter db port');
        }

        if ($this->getDbUser() === null) {
            $this->dbUser = ConsoleHelper::askForParam('Enter db user', ConsoleHelper::V_TYPE_NONE_EMPTY);
        }

        if ($this->getDbPass() === null) {
            $this->dbPass = ConsoleHelper::askForParam('Enter db password', ConsoleHelper::V_TYPE_NONE_EMPTY);
        }

        $template = $this->getTemplateName();

        if (
            !$template
            || !ServerTemplates::getInstance()->templateExists($this->getTemplateName())
        ) {
            if (ConsoleHelper::askYesNo('Would you like to create a new template?')) {

                if (!$template) {
                    $template = ConsoleHelper::askForParam('Enter template name');
                }

                ServerTemplates::getInstance()
                    ->setTemplateParam($template, ServerTemplates::T_PARAM_WEB_ROOT_DIR,    $this->getWebRootDir())
                    ->setTemplateParam($template, ServerTemplates::T_PARAM_DEPLOY_DIR,      $this->getDeployDir())
                    ->setTemplateParam($template, ServerTemplates::T_PARAM_SSH_PORT,        $this->getSSHConnection()->getPort())
                    ->setTemplateParam($template, ServerTemplates::T_PARAM_SSH_USER,        $this->getSSHConnection()->getUser())
                    ->setTemplateParam($template, ServerTemplates::T_PARAM_SSH_PRIVATE_KEY, $this->getSSHConnection()->getPrivateKey())
                    ->setTemplateParam($template, ServerTemplates::T_PARAM_HTTP_HOST,       '')
                    ->setTemplateParam($template, ServerTemplates::T_PARAM_DB_HOST,         $this->getDbHost())
                    ->setTemplateParam($template, ServerTemplates::T_PARAM_DB_PORT,         $this->getDbPort())
                    ->setTemplateParam($template, ServerTemplates::T_PARAM_DB_SOCKET,       $this->getDbSocket())
                    ->setTemplateParam($template, ServerTemplates::T_PARAM_DB_USER,         $this->getDbUser())
                    ->setTemplateParam($template, ServerTemplates::T_PARAM_DB_PASS,         $this->getDbPass())
                    ->save()
                ;
            }

        }

        $params = [
            'web_root_dir'  => $this->getWebRootDir(),
            'deploy_dir'    => $this->getDeployDir(),
            'http_host'     => $this->getHttpHost(),
            'db_host'       => $this->getDbHost(),
            'db_socket'     => $this->getDbSocket(),
            'db_port'       => $this->getDbPort(),
            'db_user'       => $this->getDbUser(),
            'db_pass'       => $this->getDbPass(),
        ];

        $parts = [];

        foreach ($params as $k => $v) {
            $parts[] = '--' . $k . '=' . '"' . escapeshellarg($v) . '"';
        }

        $ssh = $this->getSSHConnection();

        $ssh->exec('php xdev.phar config-set ' . implode(' ', $parts));
    }
}
