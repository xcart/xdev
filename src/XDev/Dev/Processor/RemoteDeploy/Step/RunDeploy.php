<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Processor\RemoteDeploy\Step;

use XDev\Base\Processor\AStep;
use XDev\Dev\Processor\RemoteDeploy;
/**
 * Class TestSSHConnection
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class RunDeploy extends AStep
{

    public function getTitle()
    {
        return 'Running deploy on remote server';
    }

    public function isOneTimeRun()
    {
        return false;
    }

    public function run()
    {
        $this->getOutput()->writeln('');

        $ssh = $this->getSSHConnection();

        \XDev\Utils\Shell::setTranslateOutput(true);

        $ssh->enableVirtualTerminal();

        $dump_path = $this->getStep(RemoteDeploy::S_UPLOAD_DB_DUMP)->getDumpRemoteFilename();

        $command = (
            'php xdev.phar deploy '
            . \XDev::getConfig('Main')->getRepositoryUrl()
            . ' --no-interaction --no-sync'
            . ' --db-dump-path="' . $dump_path . '"'
            . ' --start-time-offset="' . $this->formatElapsedTime(false) . '"'
            . ' --ignore-steps="' . implode(',', $this->getLocalDeployIgnoredSteps()) . '"'
        );

        $ssh->exec($command, true);

        \XDev\Utils\Shell::setTranslateOutput(false);

        $ssh->disableVirtualTerminal();

        $result = $ssh->exec($command . ' --get-status');

        if ($result == 'Failed') {
            throw new \Exception('Deploy failed with errors');
        }
    }
}
