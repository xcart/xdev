<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Processor\RemoteDeploy\Step;

use XDev\Base\Processor\AStep;

/**
 * Class TestSSHConnection
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class CreateDbDump extends AStep
{
    protected $dumpFilename;

    public function defineStoredProperties()
    {
        return array_merge(parent::defineStoredProperties(), [
            'dumpFilename',
        ]);
    }

    public function getDumpFilename()
    {
        return $this->dumpFilename;
    }

    public function getTitle()
    {
        return 'Creating database dump';
    }

    public function run()
    {
        $this->dumpFilename = \XDev::getConfig('DbDump')->getDumpTmpFilename();

        \XDev\Database::getInstance()->doDump($this->dumpFilename, null, true);
    }
}
