<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Processor\RemoteDeploy\Step;

use XDev\Base\Processor\AStep;
use XDev\Dev\Processor\RemoteDeploy;
/**
 * Class TestSSHConnection
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class UploadDbDump extends AStep
{
    protected $dumpRemoteFilename;

    public function getTitle()
    {
        return 'Uploading database dump';
    }

    public function defineStoredProperties()
    {
        return array_merge(parent::defineStoredProperties(), [
            'dumpRemoteFilename',
        ]);
    }

    public function getDumpRemoteFilename()
    {
        return $this->dumpRemoteFilename;
    }

    public function run()
    {
        $ssh = $this->getSSHConnection();

        $local_filename = $this->getStep(RemoteDeploy::S_DB_DUMP)->getDumpFilename();

        $dump_filename = '~/' . basename($local_filename);
        $this->dumpRemoteFilename = $dump_filename;

        \XDev\Dev\Helper\SSH::upload($ssh, $local_filename, $this->dumpRemoteFilename);

    }
}
