<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Processor;

use XDev\Utils\Shell;
use XDev\Dev\Config\ServerTemplates;

/**
 * Class LocalDeploy
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class RemoteDeploy extends \XDev\Base\Processor\AProcessor
{
    const PARAM_TEMPLATE       = 'template';

    const S_DB_DUMP             = 'db_dump';
    const S_UPLOAD_XDEV         = 'upload_xdev';
    const S_SERVER_PARAMS       = 'set_server_params';
    const S_UPLOAD_DB_DUMP      = 'upload_db_dump';
    const S_RUN_DEPLOY          = 'remote_deploy';

    private $sshConnection;

    protected function getProcessId()
    {
        return 'remote_deploy';
    }

    protected function getTitle()
    {
        return 'Deploy';
    }

    protected function defineSteps()
    {
        return [
            self::S_DB_DUMP         =>'XDev\Dev\Processor\RemoteDeploy\Step\CreateDbDump',
            self::S_UPLOAD_XDEV     => 'XDev\Dev\Processor\RemoteDeploy\Step\UploadXDev',
            self::S_SERVER_PARAMS   => 'XDev\Dev\Processor\RemoteDeploy\Step\SetServerParams',
            self::S_UPLOAD_DB_DUMP  => 'XDev\Dev\Processor\RemoteDeploy\Step\UploadDbDump',
            self::S_RUN_DEPLOY      => 'XDev\Dev\Processor\RemoteDeploy\Step\RunDeploy',
        ];
    }

    public function getTemplateName()
    {
        return $this->getOption(self::PARAM_TEMPLATE); 
    }

    public function getTemplateParam($param)
    {
        $config = ServerTemplates::getInstance();

        if (!$config->templateExists($this->getTemplateName())) {
            return null;
        }

        return ServerTemplates::getInstance()->getTemplateParam($this->getTemplateName(), $param);
    }

    public function getRemoteInstance()
    {
        $branch = \XDev\Utils\Git::getCurrentBranch();
        $instance = \XDev::getConfig('main')->getRemoteInstance('DEV');

        if (!$instance) {
            $instance = \XDev::getConfig('main')->addRemoteInstance('DEV', $branch);
        }

        return $instance;
    }

    public function getSSHConnection()
    {
        if (!$this->sshConnection) {
            $instance = $this->getRemoteInstance();

            $template = $this->getTemplateName();

            $privateKey = '';

            if ($template && ServerTemplates::getInstance()->templateExists($template)) {

                if (!$instance->getSSHHost()) {
                    $ssh_host = $this->getTemplateParam(ServerTemplates::T_PARAM_SSH_HOST);
                    //$ssh_host = str_replace(['{project}', '{branch}'], [$project, $branch], $ssh_host);

                    $instance->setSSHHost($ssh_host);

                }

                if (!$instance->getSSHPort()) {
                    $instance->setSSHPort($this->getTemplateParam(ServerTemplates::T_PARAM_SSH_PORT));
                }

                if (!$instance->getSSHUser()) {
                    $instance->setSSHUser($this->getTemplateParam(ServerTemplates::T_PARAM_SSH_USER));
                }

                $privateKey = $this->getTemplateParam(ServerTemplates::T_PARAM_SSH_PRIVATE_KEY);

            }

            $hub_rsa = \XDev\Dev\Helper\SSH::askForPrivateKey($this->getRemoteInstance()->getRepositoryURL());

            $this->sshConnection = \XDev\Dev\Helper\SSH::createConnection($instance, $privateKey, [$hub_rsa]);

        }

        return $this->sshConnection;
    }

    public function getLocalDeployIgnoredSteps()
    {
        return [];
    }

}
