<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Processor;

use \XDev\Utils\Shell;

/**
 * Class LocalDeploy
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class LocalDeploy extends \XDev\Base\Processor\AProcessor
{
    const PARAM_HTTP_HOST       = 'httpHost';
    const PARAM_WEB_DIR         = 'webDir';
    const PARAM_PROJECT_NAME    = 'projectName';
    const PARAM_REPO_URL        = 'repoUrl';
    const PARAM_INSTANCE        = 'instance';
    const PARAM_BRANCH          = 'branch';
    const PARAM_DEPLOY_DIR      = 'deployDir';
    const PARAM_NO_SYNC         = 'noSync';
    const PARAM_DB_DUMP_PATH    = 'dbDumpPath';

    const S_CHECK_XDEV          = 'check_xdev';
    const S_DB_DUMP             = 'db_dump';
    const S_DOWNLOAD_DB_DUMP    = 'download_dbdump';
    const S_PUSH                = 'push_update';
    const S_PULL                = 'pull';
    const S_RESTORE_FILES       = 'restore_files';
    const S_TEST_DB_CONNECTION  = 'test_db';
    const S_CREATE_DB           = 'create_db';
    const S_EXTRACT_DB_DUMP     = 'extract_dbdump';
    const S_IMPORT_DB_DUMP      = 'import_dbdump';

    private $sshConnection;

    protected function getProcessId()
    {
        return 'local_deploy';
    }

    protected function getTitle()
    {
        return 'Deploy';
    }

    protected function defineSteps()
    {
        $steps = [
            self::S_CHECK_XDEV          => 'XDev\Dev\Processor\LocalDeploy\Step\SyncCheckXDev',
            self::S_DB_DUMP             => 'XDev\Dev\Processor\LocalDeploy\Step\SyncDbDump',
            self::S_DOWNLOAD_DB_DUMP    => 'XDev\Dev\Processor\LocalDeploy\Step\SyncDownloadDbDump',
            self::S_PUSH                => 'XDev\Dev\Processor\LocalDeploy\Step\SyncPushUpdate',
            self::S_PULL                => 'XDev\Dev\Processor\LocalDeploy\Step\SyncPull',
            self::S_RESTORE_FILES       => 'XDev\Dev\Processor\LocalDeploy\Step\RestoreFiles',
            self::S_TEST_DB_CONNECTION  => 'XDev\Dev\Processor\LocalDeploy\Step\TestDatabaseConnection',
            self::S_CREATE_DB           => 'XDev\Dev\Processor\LocalDeploy\Step\CreateDatabase',
            self::S_EXTRACT_DB_DUMP     => 'XDev\Dev\Processor\LocalDeploy\Step\ExtractDbDump',
            self::S_IMPORT_DB_DUMP      => 'XDev\Dev\Processor\LocalDeploy\Step\ImportDatabaseDump',
        ];

        if (\XDev::getConfig('dbDump')->isGitTrack()) {
            unset($steps[self::S_DOWNLOAD_DB_DUMP]);
        }

        if ($this->getOption(self::PARAM_NO_SYNC)) {
            unset($steps[self::S_PUSH]);
            unset($steps[self::S_PULL]);

            if (\XDev::getConfig('dbDump')->isGitTrack()) {
                unset($steps[self::S_DB_DUMP]);
            }

        }

        if ($this->getOption(self::PARAM_DB_DUMP_PATH)) {
            unset($steps[self::S_DB_DUMP]);
            unset($steps[self::S_DOWNLOAD_DB_DUMP]);
        }

        if (!isset($steps[self::S_DB_DUMP]) && !isset($steps[self::S_PUSH])) {
            unset($steps[self::S_CHECK_XDEV]);
        }

        return $steps;
    }

    protected function prepareSteps()
    {
        $steps = parent::prepareSteps();

        return $steps;
    }

    public function getDeployDir()
    {
        return $this->getOption(self::PARAM_DEPLOY_DIR);
    }

    public function getRepoURL()
    {
        return $this->getOption(self::PARAM_REPO_URL);
    }

    public function getInstanceName()
    {
        return $this->getOption(self::PARAM_INSTANCE);
    }

    public function getBranch()
    {
        return $this->getOption(self::PARAM_BRANCH);
    }

    public function getHttpHost() {
        return \XDev\Dev\Config\LInstance::getInstance()->getHttpHost();
    }

    public function getWebDir()
    {
        return \XDev\Dev\Config\LInstance::getInstance()->getWebDir();
    }

    public function getDbUser()
    {
        return \XDev::getConfig('Db')->getUser();
    }

    public function getDbPass()
    {
        return \XDev::getConfig('Db')->getPassword();
    }

    public function getDbHost()
    {
        return \XDev::getConfig('Db')->getHost();
    }

    public function getDbName()
    {
        return \XDev::getConfig('Db')->getDatabase();
    }

    public function getDbPort()
    {
        return \XDev::getConfig('Db')->getPort();
    }

    public function getDbSocket()
    {
        return \XDev::getConfig('Db')->getSocket();
    }

    public function getRemoteInstance()
    {
        return \XDev::getConfig('main')->getRemoteInstance($this->getInstanceName());
    }

    public function getSSHConnection()
    {
        if (!$this->sshConnection) {
            $instance = $this->getRemoteInstance();

            $hub_rsa = \XDev\Dev\Helper\SSH::askForPrivateKey($this->getRemoteInstance()->getRepositoryURL());

            $this->sshConnection = \XDev\Dev\Helper\SSH::createConnection($instance, null, [$hub_rsa]);

        }

        return $this->sshConnection;
    }

    public function buildMysqlCommandParams()
    {
        $params = 'mysql -h' . $this->getDbHost() . ' -u' . $this->getDbUser() . ' -p' . $this->getDbPass();

        if ($this->getDbPort()) {
            $params .= ' -P ' . $this->getDbPort();
        }

        if ($this->getDbSocket()) {
            $params .= ' --protocol=socket -S ' . $this->getDbSocket();
        }

        return $params;
    }

}
