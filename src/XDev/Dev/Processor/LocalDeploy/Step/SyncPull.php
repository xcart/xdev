<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Processor\LocalDeploy\Step;

use XDev\Base\Processor\AStep;

/**
 * Class SyncPull
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class SyncPull extends AStep
{

    public function getTitle()
    {
        return 'Pull remote changes';
    }

    public function getProcessTitle()
    {
        return sprintf('Sync with %s', $this->getInstanceName());
    }

    public function run()
    {
        $this->getOutput()->writeln('');

        $instance = \XDev::getConfig('main')->getRemoteInstance($this->getInstanceName());

        \XDev\Utils\Shell::setTranslateOutput(true);
        \XDev\Utils\Shell::exec('git pull origin ' . $instance->getMainBranch());
        \XDev\Utils\Shell::setTranslateOutput(false);
    }

}
