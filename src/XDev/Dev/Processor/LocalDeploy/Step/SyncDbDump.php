<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Processor\LocalDeploy\Step;

use XDev\Base\Processor\AStep;
use XDev\Utils\SSH;
use Symfony\Component\Console\Exception\RuntimeException;

/**
 * Class SyncDbDump
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class SyncDbDump extends AStep
{
    protected $dumpRemoteFilename;
    protected $dumpRemoteFilesize;

    public function getTitle()
    {
        return 'Creating a database dump';
    }

    public function getProcessTitle()
    {
        return sprintf('Sync with %s', $this->getInstanceName());
    }

    public function getDumpRemoteFilename()
    {
        return $this->dumpRemoteFilename;
    }

    public function getDumpRemoteFilesize()
    {
        return $this->dumpRemoteFilesize;
    }

    public function defineStoredProperties()
    {
        return array_merge(parent::defineStoredProperties(), [
            'dumpRemoteFilename',
            'dumpRemoteFilesize',
        ]);
    }

    public function run()
    {
        $ssh = $this->getSSHConnection();

        $o = $ssh->exec([
            "cd {$this->getRemoteInstance()->getSoftwareDir()}",
            'php xdev_live.phar dd:dump --json-output'
        ]);

        $o = \XDev\Core\JsonAPI::parseOutput($o, true);

        if (!isset($o['result']['dumpFilename'])) {
            throw new RuntimeException(
                "Error: empty dump filename"
            );
        }

        $this->dumpRemoteFilename = $o['result']['dumpFilename'];
        $this->dumpRemoteFilesize = $o['result']['dumpFilesize'];

    }
}
