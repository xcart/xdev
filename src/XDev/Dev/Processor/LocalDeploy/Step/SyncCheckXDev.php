<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Processor\LocalDeploy\Step;

use XDev\Base\Processor\AStep;
use XDev\Core\VersionManager\VersionManager;

/**
 * Class TestSSHConnection
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class SyncCheckXDev extends AStep
{

    public function getTitle()
    {
        return 'Checking xdev script';
    }

    public function run()
    {
        $ssh = $this->getSSHConnection();

        $local_filename = \XDev\Application::getInstance()->getCompiledScriptPathXDevLive();
        $remote_filename = $this->getRemoteInstance()->getSoftwareDir() . '/xdev_live.phar';

        try {
            $remote_version = trim($ssh->exec([
                "php $remote_filename app:version"
            ]));

            $this->getOutput()->writeln('Found X-Dev version ' . $remote_version);

        } catch (\Exception $e) {
            $remote_version = '';
        }

        $vm = new VersionManager();
        $vm->setVersionDelimiter('.');

        if (!$remote_version || $vm->compareVersions($remote_version, \XDev::VERSION) === VersionManager::IS_LESS) {
            $this->getOutput()->writeln('Uploading xdev script');
            \XDev\Dev\Helper\SSH::upload($ssh, $local_filename, $remote_filename);
        }

    }
}
