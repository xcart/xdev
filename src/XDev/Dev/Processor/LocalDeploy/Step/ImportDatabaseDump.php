<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */
namespace XDev\Dev\Processor\LocalDeploy\Step;

use XDev\Base\Processor\AStep;
use \XDev\Utils\Shell;
use XDev\Dev\Processor\LocalDeploy;

/**
 * Class ImportDatabaseDump
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class ImportDatabaseDump extends AStep
{

    public function getTitle()
    {
        return 'Importing database dump file';
    }

    public function run()
    {
        $dump_filename = $this->getStep(LocalDeploy::S_EXTRACT_DB_DUMP)->getExtractedDumpFilename();

        Shell::exec(
            $this->buildMysqlCommandParams()
            . ' ' . $this->getDbName() . ' < ' . $dump_filename
        );
    }
}
