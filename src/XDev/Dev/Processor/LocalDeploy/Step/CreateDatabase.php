<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */
namespace XDev\Dev\Processor\LocalDeploy\Step;

use XDev\Base\Processor\AStep;
use \XDev\Utils\Shell;

/**
 * Class TestDatabaseConnection
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class CreateDatabase extends AStep
{

    public function getTitle()
    {
        return 'Creating database';
    }

    public function run()
    {
        Shell::exec(
           $this->buildMysqlCommandParams()
            . ' --execute="create database ' . $this->getDbName() . '"'
        );
    }
}
