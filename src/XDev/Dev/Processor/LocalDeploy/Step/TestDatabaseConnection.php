<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */
namespace XDev\Dev\Processor\LocalDeploy\Step;

use XDev\Base\Processor\AStep;
use \XDev\Utils\Shell;

/**
 * Class TestDatabaseConnection
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class TestDatabaseConnection extends AStep
{

    public function getTitle()
    {
        return 'Checking database connection';
    }

    public function isHidden()
    {
        return true;
    }

    public function run()
    {
        try {
            Shell::exec($this->buildMysqlCommandParams()); 

        } catch (\Exception $e) {

            throw new \Exception(
                'Failed to conect to the MySQL server' . "\n" 
                . $e->getMessage() . "\n\n" 
                . 'Check if database access details are set in ' . \XDev\Dev\Config\Main::getInstance()->getConfigFilenameFull()
            );
        }
    }
}
