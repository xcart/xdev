<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Processor\LocalDeploy\Step;

use XDev\Base\Processor\AStep;
use XDev\Dev\Processor\LocalDeploy;

/**
 * Class SyncDbDump
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class SyncDownloadDbDump extends AStep
{
    protected $dumpLocalFilename;

    public function getTitle()
    {
        return 'Downloading dump file';
    }

    public function getProcessTitle()
    {
        return sprintf('Sync with %s', $this->getInstanceName());
    }

    public function defineStoredProperties()
    {
        return array_merge(parent::defineStoredProperties(), [
            'dumpLocalFilename',
        ]);
    }

    public function getDumpLocalFilename()
    {
        return $this->dumpLocalFilename;
    }

    public function run()
    {
        $this->getOutput()->writeln('');

        $dump_filename_remote = $this->getStep(LocalDeploy::S_DB_DUMP)->getDumpRemoteFilename();
        $dump_filesize = $this->getStep(LocalDeploy::S_DB_DUMP)->getDumpRemoteFilesize();

        $dump_filename = basename($dump_filename_remote);

        $dump_filename_local = \XDev\Config::getXDevTmpDir() . \XDev::DS . $dump_filename;

        $ssh = $this->getSSHConnection();

        \XDev\Dev\Helper\SSH::download($ssh, $dump_filename_remote, $dump_filename_local, $dump_filesize);

        $this->dumpLocalFilename = $dump_filename_local;

    }
}
