<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Processor\LocalDeploy\Step;

use XDev\Base\Processor\AStep;
use XDev\Utils\Shell;

/**
 * Class SyncPushUpdate
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class SyncPushUpdate extends AStep
{

    public function getTitle()
    {
        return 'Pushing remote changes to the repository';
    }

    public function getProcessTitle()
    {
        return sprintf('Sync with %s', $this->getInstanceName());
    }

    public function run()
    {
        $this->getOutput()->writeln('');

        $instance = $this->getRemoteInstance();

        Shell::setTranslateOutput(true);

        $ssh = $this->getSSHConnection();

        $ssh->exec([
            "cd {$instance->getSoftwareDir()}",
            'php xdev_live.phar push-update'
        ], true);

        $ssh->stopAgentForwarding();

        Shell::setTranslateOutput(false);
    }

}
