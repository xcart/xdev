<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Processor\LocalDeploy\Step;

use XDev\Base\Processor\AStep;
use XDev\Dev\Processor\LocalDeploy;

/**
 * Class ExtractDbDump
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class ExtractDbDump extends AStep
{
    protected $extractedDumpFilename;

    public function getTitle()
    {
        return 'Importing database dump file';
    }

    public function isHidden()
    {
        return true;
    }

    public function defineStoredProperties()
    {
        return array_merge(parent::defineStoredProperties(), [
            'extractedDumpFilename',
        ]);
    }

    public function getExtractedDumpFilename()
    {
        return $this->extractedDumpFilename;
    }

    public function run()
    {
        if ($this->getOption(LocalDeploy::PARAM_DB_DUMP_PATH)) {
            $dump_local_filename = $this->getOption(LocalDeploy::PARAM_DB_DUMP_PATH);

        } elseif (\XDev::getConfig('dbDump')->isGitTrack()) {
            $dump_local_filename = \XDev::getConfig('DbDump')->getDumpFilename();

        } else {
            $dump_local_filename = $this->getStep(LocalDeploy::S_DOWNLOAD_DB_DUMP)->getDumpLocalFilename();
        }

        $pathinfo = pathinfo($dump_local_filename);

        if (strtolower($pathinfo['extension']) != 'sql' ) {

            $extracted_filename = $pathinfo['dirname'] . \XDev::DS . 'db_dump.sql';
            \XDev\Utils\Shell::extractFile($dump_local_filename, $extracted_filename);

        } else {
            $extracted_filename = $dump_local_filename;
        }

        $this->extractedDumpFilename = $extracted_filename;

    }
}
