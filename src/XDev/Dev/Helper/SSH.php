<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Dev\Helper;

use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Exception\RuntimeException;
use XDev\Core\SSHConnection;
use XDev\Utils\Filesystem;
use XDev\Utils\Git;
use XDev\Core\Symfony\Console\Helper as ConsoleHelper;

/**
 * Class Helper
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class SSH
{
    public static function createConnection($instance, $private_key = null, $forwarded_keys = [])
    {

        $ssh_host = $instance->getSSHHost();
        $ssh_port = $instance->getSSHPort();
        $ssh_user = $instance->getSSHUser();

        if (!$ssh_host) {
            $a = ConsoleHelper::askForParam('Enter SSH host', ConsoleHelper::V_TYPE_NONE_EMPTY);

            $tmp = explode('@', $a);

            if (isset($tmp[1])) {
                $ssh_user = $tmp[0];
                $ssh_host_port = $tmp[1];

                $tmp = explode(':', $ssh_host_port);
                $ssh_host = $tmp[0];

                if (isset($tmp[1])) {
                    $ssh_port = $tmp[1];
                }

            } else {
                $ssh_host = $tmp[0];
            }
        }

        if (!$ssh_port) {
            $ssh_port = 22;
        }

        if (!$ssh_user) {
            $ssh_user = ConsoleHelper::askForParam('Eenter SSH user', ConsoleHelper::V_TYPE_NONE_EMPTY);

        }

        $hostsCfg = \XDev\Dev\Config\Hosts::getInstance();

        $saved_private_key = '';

        if (!$private_key) {
            $private_key = $saved_private_key = $hostsCfg->getHostParam($ssh_host, \XDev\Dev\Config\Hosts::PARAM_SSH_PRIVATE_KEY);
        }

        if (!$private_key) {
            $private_key = ConsoleHelper::askForParam('Enter id_rsa', ConsoleHelper::V_TYPE_NONE_EMPTY);
        }

        if ($private_key && !Filesystem::exists($private_key)) {
            throw new RuntimeException(sprintf('File %s does not exist', $private_key));
        }

        $conn = new \XDev\Core\SSHConnection($ssh_host, $ssh_user, $ssh_port, $private_key);

        if ($hostsCfg->getHostParam($ssh_host, \XDev\Dev\Config\Hosts::PARAM_SSH_USE_MULTIPLEXING)) {
            $conn->enableMultiplexing();
        }

        if ($forwarded_keys) {
            $conn->startAgentForwarding();

            foreach ($forwarded_keys as $key) {
                $conn->agentAddKey($key);
            }
        }

        if (
            !$instance->getSSHHost()
            || !$instance->getSSHPort()
            || !$instance->getSSHUser()
        ) {
            ConsoleHelper::writeln('Testing connection to ' . $ssh_host);

            $instance->setSSHHost($ssh_host);
            $instance->setSSHPort($ssh_port);
            $instance->setSSHUser($ssh_user);

            if ($conn->multiplexingEnabled()) {
                $conn->connect();

            } else {
                $conn->test();
            }

            \XDev::getConfig('Main')->save();

        }

        if (
            $private_key
            && !$saved_private_key
        ) {
            $hostsCfg->setHostParam($ssh_host, \XDev\Dev\Config\Hosts::PARAM_SSH_PRIVATE_KEY, $private_key);
            $hostsCfg->save();
        }

        return $conn;
    }

    public static function upload($conn, $local_filename, $remote_filename)
    {
        $output = \XDev\Application::getInstance()->getCurrentOutput();

        $local_filesize = filesize($local_filename);

        if (!Filesystem::exists($local_filename)) {
            throw new RuntimeException(sprintf('File %s does not exist', $local_filename));
        }

        $background = $conn->multiplexingEnabled();

        if ($background) {
            $conn->exec('rm -f ' . $remote_filename);
        }

        $process = $conn->upload($local_filename, $remote_filename, $background);

        if ($background) {

            $progressBar = ConsoleHelper::fileTransferPogressBar($local_filesize);

            $progressBar->start();

            while ($process->isRunning())
            {
                usleep(600000);

                $result = $conn->exec('du -b ' . $remote_filename);
                $tmp = explode(' ', $result);
                $size = intval($tmp[0]);

                $progressBar->setProgress($size);
            }

            $progressBar->finish();
            $progressBar->clear();
        }
    }

    public static function download($conn, $remote_filename, $local_filename, $remote_filesize = false)
    {
        $output = \XDev\Application::getInstance()->getCurrentOutput();

        Filesystem::remove($local_filename);

        $dir = dirname($local_filename);

        Filesystem::remove($local_filename);

        if (!Filesystem::exists($dir)) {
            Filesystem::mkdir($dir);
        }

        $process = $conn->download($remote_filename, $local_filename, true);

        if (!$remote_filesize && $conn->multiplexingEnabled()) {
            $result = $conn->exec('du -b ' . $remote_filename);
            $tmp = explode(' ', $result);
            $remote_filesize = intval($tmp[0]);
        }

        $progressBar = ConsoleHelper::fileTransferPogressBar($remote_filesize);

        $progressBar->start();

        while ($process->isRunning())
        {
            usleep(600000);

            clearstatcache();

            $progressBar->setProgress(filesize($local_filename));
        }

        if (!Filesystem::exists($local_filename)) {
            throw new RuntimeException(
                'Error while downloading dump file'
            );
        }

        $progressBar->finish();
        $progressBar->clear();
    }

    public static function askForPrivateKey($repo_url)
    {
        $parsed_url = Git::parseRepoUrl($repo_url);

        $hubsCfg = \XDev\Dev\Config\Hubs::getInstance();
        $hub_rsa = $hubsCfg->getPrivateKeyFilename($parsed_url['host'], $parsed_url['vendor']);

        if (!$hub_rsa) {
            $hub_rsa = ConsoleHelper::askForParam(sprintf('Please enter privite key file path for %s:', $parsed_url['host'] . '/' . $parsed_url['vendor']));

            if ($hub_rsa) {
                $hubsCfg->setPrivateKeyFilename($parsed_url['host'], $parsed_url['vendor'], $hub_rsa);
                $hubsCfg->save();
            }
        }

        return $hub_rsa;
    }
}
