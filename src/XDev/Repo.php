<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev;

use XDev\Config\Repo as RepoConfig;
use XDev\Utils\Filesystem;
use XDev\Utils\Git;
use Symfony\Component\Console\Exception\RuntimeException;

/**
 * Class Repo
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Repo extends \XDev\Core\Singleton
{
    protected function getConfig()
    {
        return \XDev::getConfig('Repo');
    }

    public function init($remote_url)
    {
        if (!Git::checkGitMinRequiredVersion()) {

            throw new RuntimeException(
                sprintf(
                    'Minimum required git version is \'%s\', found %s',
                    Git::GIT_MIN_REQUIRED_VERSION,
                    Git::getGitVersion()
                )
            );
        }

        if (Git::isRepoInitialized() && Git::getCurrentBranch() !== RepoConfig::BRANCH_NAME_LIVE) {

            throw new RuntimeException(
                sprintf(
                    'Cannot proceed: the name of current branch should be \'%s\''
                    . "\n" . 'Current branch: %s',
                    RepoConfig::BRANCH_NAME_LIVE,
                    Git::getCurrentBranch()
                )
            );
        }

        $gitignore_filename = $this->getConfig()->getGitignoreFilenameFull();

        if (Filesystem::exists($gitignore_filename)) {
            Filesystem::backupFile($gitignore_filename, '~gitignore');
        }

        Filesystem::dumpFile($gitignore_filename, $this->getConfig()->getGitignoreContent());

        if (!Git::isRepoInitialized()) {
            \XDev\Utils\Shell::exec('git init');
            \XDev\Utils\Shell::exec('git checkout -b' . ' ' . RepoConfig::BRANCH_NAME_LIVE);
            \XDev\Utils\Shell::exec('git remote add -t ' . RepoConfig::BRANCH_NAME_LIVE . ' origin ' . $remote_url);
        }
    }

    public function backupPreservedFiles()
    {
        foreach ($this->getConfig()->getPreservedFiles() as $file) {
            $file_to_backup_filename_rel = $file['file'];
            $backup_dirname_full = $this->getConfig()->getPreservedFilesDir() . \XDev::DS . dirname($file_to_backup_filename_rel);
            $backup_filename = basename($file_to_backup_filename_rel);
            $backup_filename_full = $backup_dirname_full . \XDev::DS . $backup_filename;

            $content = $filteredContent = file_get_contents(\XDev::getSoftwareDir() . \XDev::DS . $file_to_backup_filename_rel);

            if (isset($file['contentFilter']) && method_exists($this->getConfig(), $file['contentFilter'])) {
                $filteredContent = call_user_func([$this->getConfig(), $file['contentFilter']], $content);
            }

            Filesystem::mkdir($backup_dirname_full);

            Filesystem::dumpFile($backup_filename_full, $filteredContent);
        }
    }
}
