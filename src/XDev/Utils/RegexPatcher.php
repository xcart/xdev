<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Utils;

use XDev\Utils\Filesystem;

/**
 * Class RegexPatcher
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class RegexPatcher
{
    const MODE_INSERT_BERORE    = 'before';
    const MODE_INSERT_AFTER     = 'after';
    const MODE_REPLACE          = 'replace';

    protected $content = '';
    protected $patchedContent = '';

    public function load($str)
    {
        $this->patchedContent = $this->content = $str;
    }

    public function loadFromFile($filename)
    {
        $this->patchedContent = $this->content = file_get_contents($filename);
    }

    public function savePatchedContent($filename)
    {
        Filesystem::dumpFile($filename, $this->patchedContent);
    }

    public function getPatchedContent()
    {
        return $this->patchedContent;
    }

    public static function applyPatch($text, $pattern, $patch, $mode = self::MODE_REPLACE) {

        if (preg_match($pattern, $text, $m, PREG_OFFSET_CAPTURE)) {

            if ($mode === self::MODE_REPLACE && count($m) > 1) {
                $replace = is_array($patch) ? $patch : [$patch];
                $current_pos = 0;

                for ($i = 1; $i < count($m); $i++) {
                    $start_pos = $m[$i][1];
                    $length = strlen($m[$i][0]);

                    $parts[] = substr($text, $current_pos, $start_pos - $current_pos);
                    $parts[] = $replace[$i - 1];

                    $current_pos = $start_pos + $length;
                }

                $parts[] = substr($text, $current_pos, strlen($text) - $current_pos);
            }

            return implode('', $parts);

        }

        return $text;

    }
}
