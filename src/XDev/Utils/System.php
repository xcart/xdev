<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Utils;

use XDev\Utils\Shell;

/**
 * Class System
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class System
{
    protected static $home_dir = '';

    public static function getHomeDir()
    {
        if (self::$home_dir) {
            return self::$home_dir;
        }

        if (isset($_SERVER['HOME'])) {
            self::$home_dir = $_SERVER['HOME'];
        } else {
            self::$home_dir = getenv('HOME');
        }

        if (empty(self::$home_dir)) {
            if (strncasecmp(PHP_OS, 'WIN', 3) === 0) {
                self::$home_dir = Shell::exec("echo %userprofile%", true);
            } else {
                self::$home_dir = Shell::exec("echo ~", true);
            }
        }

        return self::$home_dir;
    }

}
