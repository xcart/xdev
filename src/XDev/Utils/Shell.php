<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Utils;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Console\Exception\RuntimeException;

/**
 * Class Shell
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */

class Shell
{
    protected static $debug_mode = false;
    protected static $timeout = 60;
    protected static $translateOutput = false;
    protected static $lastOutputLineCount = 0;
    protected static $lastOutput = '';

    public static function isDebugMode()
    {
        return self::$debug_mode;
    }

    public static function setDebugMode($mode = true)
    {
        self::$debug_mode = $mode;
    }

    public static function setTimeout($secs)
    {
        self::$timeout = $secs;
    }

    public static function setTranslateOutput($flag = true)
    {
        self::$translateOutput = (bool) $flag;
    }

    public static function isTranslateOutput()
    {
        return self::$translateOutput;
    }

    public static function getLastCommandOutput()
    {
        return self::$lastOutput;
    }

    public static function clearLastCommandOutput($output)
    {

        // Move the cursor to the beginning of the line
        $output->write("\x0D");

        // Erase the line
        $output->write("\x1B[2K");

        // Erase previous lines

        if (self::$lastOutputLineCount > 0) {
            $output->write(str_repeat("\x1B[1A\x1B[2K", self::$lastOutputLineCount));
        }
    }
    public static function extractFile($source_file, $dest_file)
    {
        preg_match("/.*\.(\S+)/", $source_file, $ext1); // single extension
        preg_match("/.*\.(\S+\.\S+)/", $source_file, $ext2); // double extension

        $c = '';

        foreach ([$ext1[1], $ext2[1]] as $v) {

            switch ($v) {
                case 'tar.bz2' :
                    $c = "tar xvjf $source_file -C $dest_file";
                    break;
                case 'tar.gz'  :
                    $c = "tar xvzf $source_file -C $dest_file";
                    break;
                case 'tgz'     :
                    $c = "tar xvzf $source_file -C $dest_file";
                    break;
                case 'gz'      :
                    $c = "gunzip -kf $source_file > $dest_file";
                    break;
                case 'zip'     :
                    $c = "unzip $source_file -d $dest_file";
                    break;
                case 'tar'     :
                    $c = "tar xvjf $source_file -C $dest_file";
                    break;
            }

            if ($c) {
                self::exec($c);
                break;
            }

        }

    }

    public static function exec($command, $return = false, $background = false, $tty = false)
    {
        self::$lastOutputLineCount = 0;
        self::$lastOutput = '';

        if (self::isDebugMode()) {
            echo "$command\n";
        }

        if (!is_array($command)) {
            $command = [$command];
        }

        $process = new Process(implode(PHP_EOL, $command));

        $process->setTimeout(self::$timeout);

        $process->setTty($tty);

        if (!$background) {
            if (self::$translateOutput) {
                $process->run(function ($type, $buffer) {
                    self::$lastOutput .= $buffer;
                    self::$lastOutputLineCount += substr_count($buffer, "\n");

                    if (Process::ERR === $type) {
                        echo $buffer;
                    } else {
                        echo $buffer;
                    }
                });
            } else {
                $process->run();
            }

            if (!$process->isSuccessful()) {
                $outputText = $process->getOutput();
                $errorText = $process->getErrorOutput();

                throw new RuntimeException($outputText . ($outputText ? "\n" : '') . $errorText);
            }

            $return = $process->getOutput();

            if ($return) { 
                return $return;

            } else {
                echo $return;
            }

        } else {
            $process->start();

            return $process;
        }

    }
}
