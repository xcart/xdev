<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Utils;

/**
 * Class Converter
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
abstract class Converter
{
    const FORMAT_B  = 0;
    const FORMAT_KB = 1;
    const FORMAT_MB = 2;
    const FORMAT_GB = 3;
    const FORMAT_TB = 4;
    const FORMAT_PB = 5;

    protected static $byteMultipliers = ['b', 'kB', 'MB', 'GB', 'TB', 'PB'];

    /**
     * Convert a string like "testFooBar" into the underline style (like "test_foo_bar")
     *
     * @param string $className String to convert
     *
     * @return string
     */
    public static function camelCaseToDashes($className)
    {
        return strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $className));
    }

    /**
     * Prepare human-readable output for file size
     *
     * @param integer $size      Size in bytes
     * @param string  $separator To return a string OPTIONAL
     *
     * @return string
     */
    public static function formatFileSize($size)
    {
        $multiplier = 0;

        while (1024 < $size) {

            $size /= 1024;

            $multiplier++;
        }

        // Do not display numbers after decimal point if size is in kilobytes.
        // When size is greater than display one number after decimal point.
        $result = number_format($size, $multiplier > 1 ? 1 : 0) . ' ' . static::$byteMultipliers[$multiplier];

        return $result;
    }

    public static function convertBytes($bytes, $format, $decimal_places = 2)
    {
        $multiplier = 0;
        $size = $bytes;

        while ($multiplier <= count(self::$byteMultipliers) - 1) {

            if ($multiplier == $format) {
                break;
            }

            $size /= 1024;

            $multiplier++;
        }

        return number_format($size, $decimal_places) . ' ' . self::$byteMultipliers[$multiplier];

    }

}
