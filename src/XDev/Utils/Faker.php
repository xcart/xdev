<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */
namespace XDev\Utils;

/**
 * Class Faker
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Faker
{
    const FAKE_EMAIL_DOMAIN = 'fake.fake';
}