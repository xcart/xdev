<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Utils;

use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Filesystem\Filesystem as SymfonyFilesystem;
use Symfony\Component\Finder\Finder;

/**
 * Class Filesystem
 *
 * @method exists
 * @method makePathRelative
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Filesystem
{
    /**
     * @var SymfonyFilesystem
     */
    private static $filesystem;

    /**
     * @return SymfonyFilesystem
     */
    public static function getFilesystem()
    {
        if (!self::$filesystem) {
            self::$filesystem = new SymfonyFilesystem();
        }

        return self::$filesystem;
    }

    /**
     * @var Finder
     */
    private static $finder;

    /**
     * @return Finder
     */
    public static function getFinder()
    {
        if (!self::$finder) {
            self::$finder = new Finder();
        }

        return self::$finder;
    }

    /**
     * @param $name
     * @param $arguments
     *
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        return call_user_func_array([self::getFilesystem(), $name], $arguments);
    }

    /**
     * @param $path
     * @param $callback
     */
    public static function iterateDirsIn($path, $callback)
    {
        $finder = new Finder();
        if (self::getFilesystem()->exists($path)) {
            foreach ($finder->in($path)->directories()->depth('== 0') as $dir) {
                call_user_func($callback, $dir->getPathname());
            }
        }
    }

    public static function isDirEmpty($dir)
    {
        if (!is_readable($dir))
            return null;

        return (count(scandir($dir)) == 2);
    }

    public static function backupFile($filename, $backup_filename = false)
    {
        if (!$backup_filename) {
            $backup_filename = basename($filename) . '.bkp';
        }

        $backup_filename_path = dirname($filename);
        $max_attempts         = 10;
        $i                    = 0;

        $backup_filename_orig = $backup_filename;

        while (self::getFileSystem()->exists($backup_filename_full = $backup_filename_path . \XDev::DS . $backup_filename)) {
            $i++;
            $backup_filename = $backup_filename_orig . $i;

            if ($i > 10) {
                throw new RuntimeException(sprintf('Cannot backup %s. File %s already exists.', $filename, $backup_filename_full));
            }
        }

        self::getFileSystem()->rename($filename, $backup_filename_full);
    }
}
