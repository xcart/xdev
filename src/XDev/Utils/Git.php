<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Utils;

use XDev\Core\VersionManager\VersionManager;
use \XDev\Utils\Shell;

/**
 * Class Git
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Git
{
    static $vm;
    const GIT_VERSION_DELIMITER = '.';
    const GIT_MIN_REQUIRED_VERSION = '1.7.10'; // checkout -b without initial commit

    private static function getVM() {
        if (!self::$vm) {
            self::$vm = new VersionManager();
            self::$vm->setVersionDelimiter(self::GIT_VERSION_DELIMITER);

        }

        return self::$vm;
    }

    public static function checkGitMinRequiredVersion() {
        $result = self::getVM()->compareVersions(self::getGitVersion(), self::GIT_MIN_REQUIRED_VERSION);

        return $result === VersionManager::IS_GREATER || $result === VersionManager::IS_EQUAL;
    }

    public static function parseRepoUrl($url)
    {
        if (strpos($url, 'https')  === 0) {
            return self::parseRepoWebUrl($url);

        } elseif (strpos($url, 'git@') === 0) {
            return self::parseRepoSshUrl($url);

        } else {
            return false;
        }
    }

    public static function buildRepoSshUrl($host, $vendor, $project) {
        return 'git@' . $host . ':' . $vendor . '/' . $project . '.git';
    }

    public static function buildRepoHttpsUrl($host, $vendor, $project) {
        return 'https://' . $host . '/' . $vendor . '/' . $project . '.git';
    }

    public static function parseRepoSshUrl($url)
    {
        if (preg_match('/^git\@(\S+)\:(\S+)\/(\S+)\.git/', $url, $m)) {

            $host = $m[1];
            $vendor = $m[2];
            $project = $m[3];

            return [
                'host' => $host,
                'ssh_url' => self::buildRepoSshUrl($host, $vendor, $project),
                'https_url' => self::buildRepoHttpsUrl($host, $vendor, $project),
                'vendor' => $vendor,
                'project' => $project
            ];

        } else {
            return false;
        }
    }

    public static function parseRepoWebUrl($url)
    {
        $parsed_url = parse_url($url);
        $path = ltrim($parsed_url['path'], '/');

        $path_parts = explode('/', $path);

        $host = $parsed_url['host'];
        $vendor = $path_parts[0];
        $project = $path_parts[1];
        $project = str_replace('.git', '', $project);

        return [
            'host' => $host,
            'ssh_url' => self::buildRepoSshUrl($host, $vendor, $project),
            'https_url' => self::buildRepoHttpsUrl($host, $vendor, $project),
            'vendor' => $vendor,
            'project' => $project
        ];

    }

    public static function getRepoUrl() {
        return trim(Shell::exec('git config --get remote.origin.url'));
    }

    public static function testRepoUrl($url) {
        Shell::exec('git ls-remote ' . $url);
    }

    public static function getGitVersion() {
        $output = Shell::exec('git --version');

        if (preg_match('/[0-9]\S[0-9]\S+/', $output, $m)) {
            return $m[0];
        }

        return false;
    }

    public static function isRepoInitialized() {
        return file_exists(\XDev::getSoftwareDir() . \XDev::DS . '.git');;
    }

    public static function isFileModified($filename) {
        return Shell::exec('git diff --exit-code ' . $filename) > 0;
    }

    public static function checkCurrentBranchIsUpToDate()
    {
        Shell::exec('git fetch');

        $branch = self::getCurrentBranch();

        $output = trim(Shell::exec("git log HEAD..origin/$branch --oneline"));

        return empty($output);
    }

    public static function isRemoteBranchExists($branch)
    {
        $output = trim(Shell::exec('git ls-remote origin ' . $branch));

        return empty($output);
    }

    public static function isRemoteBranchFetched($branch)
    {
        try {
            $output = trim(Shell::exec('git rev-parse --symbolic-full-name origin/' . $branch));

        } catch (\Exception $e) {
            return false;
        }

        return !empty($output);
    }

    public static function getRemoteBranchAheadCommits() {
        $output = Shell::exec('git log HEAD..@{u}');

        return $output;
    }

    public static function isInitialCommitExists() {
        try {
            Shell::exec('git rev-parse HEAD');

        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    public static function getCurrentBranch() {
        $output = Shell::exec('git symbolic-ref HEAD');

        $parts = explode('/', $output);

        $branch = end($parts);

        preg_match('/(\S+)/', $branch, $m);

        return trim($m[1]);
    }

}
