<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Utils;

/**
 * Class Crypt
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Crypt
{
    static $chars = [
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
        'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
        'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
        'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
        'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
        'Y', 'Z', '_', '|', '!', '^', '*', '-', '~','='
    ];

    public static function generateToken($length = 32, array $chars = array())
    {
        if (!$chars) {
            $chars = self::$chars;
        }

        $limit = count($chars) - 1;
        $x = explode('.', uniqid('', true));
        mt_srand((int)microtime(true) + (int) hexdec($x[0]) + $x[1]);

        $result = '';
        for ($i = 0; $length > $i; $i++) {
            $result .= $chars[mt_rand(0, $limit)];
        }

        return $result;
    }

}
