<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */
namespace XDev\Utils;

/**
 * Class PhpParser
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class PhpParser {
    const T_SINGLE_TOKEN = 'single';
    const T_SCALAR = 'scalar';

    private $tokens;

    public function load($filename) {
        if (!file_exists($filename)) { 
            throw new \Exception('File does not exist: ' . $filename);
        }

        $c = file_get_contents($filename);

        $this->tokens = token_get_all($c);

    }

    private function searchToken($type, $pos = 0, $value = null) {

        $found = false;

        for ($i = $pos; $i < count($this->tokens); $i++) {

            $t = $this->tokens[$i];

            $type_matched = false;

            if ($type === self::T_SCALAR) {
                $type_matched = is_array($t) && in_array($t[0], [T_STRING, T_CONSTANT_ENCAPSED_STRING, T_DNUMBER, T_LNUMBER]);
                $token_value = is_array($t) ? $t[1] : $t;

            } elseif ($type === self::T_SINGLE_TOKEN) {
                $type_matched = !is_array($t);
                $token_value = $t;

            } else {
                $type_matched = is_array($t) && $t[0] === $type;
                $token_value = is_array($t) ? $t[1] : $t;
            }

            if ($type_matched && ($value === null || $token_value == $value)) {

                $index = $i;
                $found = true;
                break;
            }

        }

        return $found ? $index : false;

    }

    private function getTokenValue($index) {
        $t = $this->tokens[$index];

        if (is_array($t) && in_array($t[0], [T_DNUMBER, T_LNUMBER])) {

            if ($this->tokens[$index -1] === '-') {
                $token_value = -1 * $t[1];
            } else {
                $token_value = $t[1];
            }
        } elseif (!is_array($t)) {
            $token_value = $t;

        } elseif (is_array($t) && $t[0] === T_CONSTANT_ENCAPSED_STRING) {
            $token_value = $t[1];

            if ($token_value[0] === $token_value[strlen($token_value) - 1] && $token_value[0] === '\'') {
                $token_value = trim($token_value, '\'');

            } elseif ($token_value[0] === $token_value[strlen($token_value) - 1] && $token_value[0] === '"') {
                $token_value = trim($token_value, '"');
            }

        } else {
            $token_value = $t[1];
        }

        return $token_value;
    }

    public function parseClassConstValue($class_name, $const_name) {

        $pos = $this->searchToken(T_CLASS);

        if ($pos === false) {
            return false;
        }

        $pos = $this->searchToken(T_STRING, $pos + 1, $class_name);

        if ($pos === false) {
            return false;
        }

        $pos = $this->searchToken(T_CONST, $pos + 1);

        if ($pos === false) {
            return false;
        }

        $pos = $this->searchToken(T_STRING, $pos + 1, $const_name);

        if ($pos === false) {
            return false;
        }

        $pos = $this->searchToken(self::T_SCALAR, $pos + 1);

        if ($pos === false) {
            return false;
        }

        return $this->getTokenValue($pos);
    }

    public function parseVarValue($var_name) {

        $pos = $this->searchToken(T_VARIABLE, 0, '$' . $var_name);

        if ($pos === false) {
            return false;
        }

        $pos = $this->searchToken(self::T_SINGLE_TOKEN, $pos + 1, '=');

        if ($pos === false) {
            return false;
        }

        $pos = $this->searchToken(self::T_SCALAR, $pos + 1);

        if ($pos === false) {
            return false;
        }

        return $this->getTokenValue($pos);
    }

}
