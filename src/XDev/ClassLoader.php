<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev;

/**
 * Class ClassLoader
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class ClassLoader
{
    private $loader;
    private $namespaces = [];

    public function __construct($loader)
    {
        $this->loader = $loader;
    }

    public function registerNamespace($namespace)
    {
        $this->namespaces[] = $namespace;
    }

    public function loadClass($class)
    {
        $allowed = false;

        foreach ($this->namespaces as $namespace) {

            if (strpos($class, $namespace) === 0) {
                $allowed = true;
                break;
            }
        }

        if (!$allowed) {
            return false;
        }

        $result = $this->loader->loadClass($class);

        if ($result && method_exists($class, '__static')) {
            call_user_func(array($class, '__static'));
        }

        return $result;
    }

}
