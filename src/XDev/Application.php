<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use XDev\EM;
use XDev\Utils\Filesystem;

/**
 * Console Application.
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 *
 */
class Application extends \XDev\Core\Singleton
{
    private $scApp;
    private $protectedMode;
    private $liveMode;
    private $isPhar;
    private $loader;
    private $customLoader;
    private $modules;
    private $bundleModule;
    private $currentInput;
    private $currentOutput;

    public function getSCApp()
    {
        if (!$this->scApp) {
            if ($this->protectedMode) {
                throw new \Exception('Cannot use Symphony app in protected mode');
            }

            $this->scApp = new \XDev\Core\Symfony\Console\Application('X-Dev', \XDev::VERSION);
        }

        return $this->scApp;
    }

    public function setLoader($loader)
    {
        $this->loader = $loader;
    }

    public function getLoader()
    {
        return $this->loader;
    }

    private function getCustomLoader()
    {
        if (!$this->customLoader) {
            $this->customLoader = new \XDev\ClassLoader($this->getLoader());
        }

        return $this->customLoader;
    }

    public function registerNamespaces($namespaces)
    {
        foreach ($namespaces as $namespace) {
            $this->getCustomLoader()->registerNamespace($namespace);
        }
    }

    public function setProtectedMode($mode)
    {
        $this->protectedMode = (bool) $mode;

        if ($mode === true) {
            $custom_loader = $this->getCustomLoader();

            $this->getLoader()->unregister();

            spl_autoload_register([$custom_loader, 'loadClass']);
        }
    }

    public function isPharMode()
    {
        if ($this->isPhar === null) {
            $phar_file = \Phar::running(false);

            $this->isPhar = !empty($phar_file);
        }

        return $this->isPhar;
    }

    public function isLiveMode()
    {
        if ($this->liveMode === null) {
            $this->liveMode = !class_exists('\XDev\Dev\Main');
        }

        return $this->liveMode;

    }

    public function getEntryPointScript()
    {
        if (!$this->isPharMode()) {
            return Config::getSelfSourceDir() . \XDev::DS . 'bin' . \XDev::DS . 'xdev';

        } else {
            return \Phar::running(false);
        }
    }

    public function getCompiledScriptPathXDev()
    {
        return Config::getBuildOutputDir() . \XDev::DS . 'xdev.phar';
    }

    public function getCompiledScriptPathXDevLive()
    {
        return Config::getBuildOutputDir() . \XDev::DS . 'xdev_live.phar';
    }

    public function getModules()
    {
        return $this->modules;
    }

    /**
     * @return bool
     */
    protected function lookupModulesInDependencies()
    {
        $need_cache_rebuild = false;

        if (!$this->isPharMode()) {
            $installed_packages_filename = Config::getVendorDir() . \XDev::DS . 'composer' . \XDev::DS . 'installed.json';

            $mtime = filemtime($installed_packages_filename);

            if (\XDev\Config\Modules::getInstance()->getMtime() < $mtime) {

                \XDev\Config\Modules::getInstance()->setMtime($mtime);

                $tmp = file_get_contents($installed_packages_filename);
                $packages = json_decode($tmp, true);

                foreach ($packages as $p) {
                    if (isset($p['extra']['xdev-type']) && $p['extra']['xdev-type'] == 'module') {
                        \XDev\Config\Modules::getInstance()->addModule($p['name'], $p['extra']['xdev-module-name']);
                        \XDev\Config\Modules::getInstance()->save();

                        $need_cache_rebuild = true;
                    }
                }
            }
        }

        return $need_cache_rebuild;
    }

    /**
     * @return bool
     */
    protected function lookupModulesInLocalFiles()
    {
        $need_cache_rebuild = false;

        if (!$this->isPharMode()) {
            Filesystem::iterateDirsIn(Config::getLocalModulesDir(), function($dir) use (&$need_cache_rebuild) {
                $metadata = file_get_contents($dir . \XDev::DS . 'composer.json');
                if (!$metadata) {
                    return;
                }

                $p = json_decode($metadata, true);

                if ($p && isset($p['extra']['xdev-type']) && $p['extra']['xdev-type'] == 'module') {
                    \XDev\Config\Modules::getInstance()->addModule($p['name'], $p['extra']['xdev-module-name']);
                    \XDev\Config\Modules::getInstance()->save();

                    $need_cache_rebuild = true;
                }
            });
        }

        return $need_cache_rebuild;
    }

    public function loadModules()
    {
        $need_cache_rebuild = $this->lookupModulesInDependencies()
            || $this->lookupModulesInLocalFiles();

        foreach (\XDev\Config\Modules::getInstance()->getInstalledModules() as $package_name => $cfg_module) {
            $class_name = '\XDev\Module\\' . $cfg_module['moduleName'] . '\Module';

            if (!class_exists($class_name)) {
                continue;
            }

            $module = $class_name::getInstance();
            $module->setPackageName($package_name);

            $module->init();

            if ($module->isBundle()) {
                $this->bundleModule = $module;
            }

            $this->modules[] = $module;

        }

        if ($need_cache_rebuild) {
            $this->buildCache();
        }

        if (!$this->isLiveMode()) {
            \XDev\Dev\Main::init();
        }
    }

    public function buildCache()
    {
        foreach ($this->modules as $module) {
            EM::addSearchDir($module->getModuleDir() . \XDev::DS . 'EngineBased');
            EM::addSearchDir($module->getModuleDir() . \XDev::DS . 'Dev' . \XDev::DS . 'EngineBased');
        }

        EM::buildCache();
    }

    public function add($command) {
        $this->getSCApp()->add($command);
    }

    protected function defineGlobalCommands()
    {
        $this->add(new \XDev\Command\AppListModules());
        $this->add(new \XDev\Command\Info());
        $this->add(new \XDev\Command\AppVersion());

        if (!$this->isPharMode()) {
            $this->add(new \XDev\Command\AppBuild());
        }

        if (!$this->isLiveMode()) {
            $this->add(new \XDev\Dev\Command\Deploy());
            $this->add(new \XDev\Dev\Command\Config());
            $this->add(new \XDev\Dev\Command\DeployRemote());
        }

    }

    public function defineCommands()
    {
        $this->add(new \XDev\Command\Init());
        $this->add(new \XDev\Command\PushUpdate());
        $this->add(new \XDev\Command\DbDump());
        $this->add(new \XDev\Command\DbDumpAdd());
        $this->add(new \XDev\Command\DbDumpReset());
        $this->add(new \XDev\Command\DbDumpStatus());
        $this->add(new \XDev\Command\DbDumpHookAdd());
        $this->add(new \XDev\Command\DbDumpHookRemove());
        $this->add(new \XDev\Command\FilesDump());
        $this->add(new \XDev\Command\FilesDumpStatus());
        $this->add(new \XDev\Command\Test());

        $commandClasses = EM::findClasses('App\\Command');

        foreach ($commandClasses as $commandClass) {
            if (class_exists($commandClass)) {
                $this->add(new $commandClass());
            }
        }

    }

    public function init()
    {
        EM::setCacheDir(Config::getCacheDir() . \XDev::DS . 'EM');

        $this->loadModules();
    }

    public function run()
    {
        if (!$this->protectedMode) {

            $this->defineGlobalCommands();

            \XDev\Core\Hook::addListener('after-detect-software', [$this, 'defineCommands']);

            \XDev::detectSoftware();

            $this->getSCApp()->run();
        }
    }

    public function addSoftwareEngine($class_name)
    {
        \XDev::addSoftwareEngine($class_name);
    }

    public function setCurrentInput(InputInterface $input)
    {
        return $this->currentInput = $input;
    }

    public function setCurrentOutput(OutputInterface $output)
    {
        return $this->currentOutput = $output;
    }

    public function getCurrentInput()
    {
        return $this->currentInput;
    }

    public function getCurrentOutput()
    {
        return $this->currentOutput;
    }

}
