<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Config;

use XDev\Core\Instance;

/**
 * Class PushUpdate
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */

class Main extends \XDev\Base\AConfigFile
{
    const INSTANCE_NAME_LIVE    = 'LIVE';
    const INSTANCE_NAME_LOCAL   = 'LOCAL';
    const PARAM_ENGINE          = 'engine';
    const PARAM_VERSION         = 'version';
    const PARAM_REPO_URL        = 'repositoryUrl';
    const PARAM_BLOWFISH        = 'blowfish';
    const PARAM_INSTANCES       = 'instances';

    private $instances;

    protected function getConfigName()
    {
        return 'main';
    }

    public function getBlowfishKey()
    {
        return $this->getParam(self::PARAM_BLOWFISH);
    }

    protected function getDir()
    {
        return \XDev\Config::getXDevDir();
    }

    public function getRepositoryURL()
    {
        return $this->getParam(self::PARAM_REPO_URL);
    }

    public function setRepositoryURL($url)
    {
        return $this->setParam(self::PARAM_REPO_URL, $url);
    }

    public function getEngineCode()
    {
        return $this->getParam(self::PARAM_ENGINE, $code);
    }

    public function setEngineCode($code)
    {
        return $this->setParam(self::PARAM_ENGINE, $code);
    }

    public function getSoftwareVersion($version)
    {
        return $this->getParam(self::PARAM_VERSION, $version);
    }

    public function setSoftwareVersion($version)
    {
        return $this->setParam(self::PARAM_VERSION, $version);
    }

    public function getRemoteInstances()
    {
        $result = [];

        foreach ($this->getParam(self::PARAM_INSTANCES) as $k => $v) {
            $result[$k] = $this->getRemoteInstance($k);
        }

        return $result;
    }

    public function getRemoteInstance($name)
    {
        $name = strtoupper($name);

        if (!isset($this->instances[$name])) {
            $params = @$this->getParam(self::PARAM_INSTANCES)[$name];

            if (!$params) {
                return false;
            }

            $params[Instance::PARAM_NAME] = $name;
            $params[Instance::PARAM_REPO_URL] = $this->getRepositoryURL();

            $this->instances[$name] = new Instance($params, [$this, 'setRemoteInstanceParam']);
        }

        return $this->instances[$name];
    }

    public function setRemoteInstanceParam($instance, $name, $value)
    {
        if (isset($this->params[self::PARAM_INSTANCES][$instance->getName()])) {
            $this->params[self::PARAM_INSTANCES][$instance->getName()][$name] = $value;
        }
    }

    public function setRemoteInstanceParams($instance_name, $params)
    {
        foreach ($params as $key => $value) {
            if (isset($this->params[self::PARAM_INSTANCES][$instance_name])) {
                $this->params[self::PARAM_INSTANCES][$instance_name][$key] = $value;
            }
        }
    }

    protected function getDefaultInstanceParams() {
        return  [
            Instance::PARAM_SOFTWARE_DIR    => '',
            Instance::PARAM_HTTP_HOST       => '',
            Instance::PARAM_HTTPS_HOST      => '',
            Instance::PARAM_SSL_ENABLED     => '',
            Instance::PARAM_WEB_DIR         => '',
            Instance::PARAM_SSH_HOST        => '',
            Instance::PARAM_SSH_USER        => '',
            Instance::PARAM_SSH_PORT        => '22',
        ];
    }

    public function addRemoteInstance($instance_name, $branch)
    {
        if (!isset($this->params[self::PARAM_INSTANCES][$instance_name])) {
            $this->params[self::PARAM_INSTANCES][$instance_name] = array_merge($this->getDefaultInstanceParams(),  [
                Instance::PARAM_MAIN_BRANCH => $branch,
            ]);

            return $this->getRemoteInstance($instance_name);

        }
    }

    protected function getDefaultConfigData()
    {
        return [
            self::PARAM_ENGINE          => '',
            self::PARAM_VERSION         => '',
            self::PARAM_REPO_URL        => '',
            self::PARAM_BLOWFISH    => \XDev\Utils\Crypt::generateToken(),
            self::PARAM_INSTANCES   => [
                self::INSTANCE_NAME_LIVE => array_merge($this->getDefaultInstanceParams(),  [
                    Instance::PARAM_MAIN_BRANCH => \XDev\Config\Repo::BRANCH_NAME_LIVE,
                ])
            ],
        ];
    }

}
