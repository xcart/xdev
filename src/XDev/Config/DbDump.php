<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Config;

/**
 * Class DbDump
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class DbDump extends \XDev\Base\AConfigFile
{
    const PARAM_EXCLUDE_TABLES      = 'excludeTables';
    const PARAM_MODIFIERS           = 'modifiers';
    const PARAM_COMPRESSION         = 'compression';
    const PARAM_DUMP_PROVIDER       = 'dumpProvider';
    const PARAM_GIT_TRACK           = 'gitTrack';

    const DUMP_PROVIDER_MYSQLDUMP   = 'mysqldump';
    const DUMP_PROVIDER_PDO         = 'pdo';

    const COMPRESS_GZIP = 'gzip';
    const COMPRESS_BZIP = 'bzip';
    const COMPRESS_NONE = 'none';

    protected $dumpProvider;

    const SQL_DUMP_FILENAME = 'db_dump.sql';

    protected function getConfigName()
    {
        return 'db_dump';
    }

    public function addTable($table_name, $schema_only)
    {
        $excludeTables = $this->getParam(self::PARAM_EXCLUDE_TABLES);

        if (isset($excludeTables[$table_name])) {
            if ($schema_only !== false) {
                $excludeTables[$table_name] = ['S' => 'Y'];

            } else {
                unset($excludeTables[$table_name]);

            }
        }

        $this->setParam(self::PARAM_EXCLUDE_TABLES, $excludeTables);
    }

    public function resetTable($table_name, $exclude_schema)
    {
        $excludeTables = $this->getParam(self::PARAM_EXCLUDE_TABLES);

        if ($exclude_schema !== false) {
            $excludeTables[$table_name] = '';

        } else {
            $excludeTables[$table_name] = ['S' => 'Y'];
        }

        $this->setParam(self::PARAM_EXCLUDE_TABLES, $excludeTables);

    }

    public function addModifier($table, $column, $modifier, $condition)
    {
        $modifiers = $this->getParam(self::PARAM_MODIFIERS);
        $modifiers[$table][$column] = [$modifier, $condition];

        ksort($modifiers);
        ksort($modifiers[$table]);

        $this->setParam(self::PARAM_MODIFIERS, $modifiers);

    }

    public function removeModifier($table, $column, $modifier, $condition)
    {
        $modifiers = $this->getParam(self::PARAM_MODIFIERS);

        unset($modifiers[$table][$column]);

        $this->setParam(self::PARAM_MODIFIERS, $modifiers);

    }

    public function getDumpTmpFilename($compress = null)
    {
        $filename = \XDev\Config::getXDevTmpDir() . \XDev::DS . self::SQL_DUMP_FILENAME;

        return $filename . $this->getCompressionExtension($compress);
    }

    public function getDumpFilename($compress = null)
    {
        $filename = \XDev\Config::getSQLDir() . \XDev::DS . self::SQL_DUMP_FILENAME;

        return $filename . $this->getCompressionExtension($compress);
    }

    private function getCompressionExtension($compress = null)
    {
        if ($compress === null) {
            $compress = $this->getCompressionMethod();
        }

        if ($compress == self::COMPRESS_GZIP) {
            $ext = '.gz';

        } elseif ($compress == self::COMPRESS_BZIP) {
            $ext = '.bz2';
        }

        return $ext;
    }

    public function isGitTrack()
    {
        return $this->getParam(self::PARAM_GIT_TRACK);
    }

    protected function getCompressionMethod()
    {
        return $this->getParam(self::PARAM_COMPRESSION);
    }

    protected function getDefaultConfigData()
    {
        return [
            self::PARAM_DUMP_PROVIDER   => self::DUMP_PROVIDER_PDO,
            self::PARAM_COMPRESSION     => self::COMPRESS_GZIP,
            self::PARAM_GIT_TRACK       => false
        ];
    }
}
