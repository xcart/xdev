<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Config;

/**
 * Class Db
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Db extends \XDev\Base\AConfigFile
{
    const DEFAULT_HOST      = 'localhost';

    const PARAM_HOST        = 'host';
    const PARAM_PORT        = 'port';
    const PARAM_USER        = 'username';
    const PARAM_PASS        = 'password';
    const PARAM_SOCKET      = 'socket';
    const PARAM_DATABASE    = 'database';

    protected function getConfigName()
    {
        return 'db';
    }

    public function getHost()
    {
        return $this->getParam(self::PARAM_HOST) ? $this->getParam(self::PARAM_HOST) : self::DEFAULT_HOST;
    }

    public function getPort()
    {
        return $this->getParam(self::PARAM_PORT);
    }

    public function getUser()
    {
        return $this->getParam(self::PARAM_USER);
    }

    public function getPassword()
    {
        return $this->getParam(self::PARAM_PASS);
    }

    public function getSocket()
    {
        return $this->getParam(self::PARAM_SOCKET);
    }

    public function getDatabase()
    {
        return $this->getParam(self::PARAM_DATABASE);
    }

    public function setHost($value)
    {
        $this->setParam(self::PARAM_HOST, $value);
    }

    public function setPort($value)
    {
        $this->setParam(self::PARAM_PORT, $value);
    }

    public function setUser($value)
    {
        $this->setParam(self::PARAM_USER, $value);
    }

    public function setPassword($value)
    {
        $this->setParam(self::PARAM_PASS, $value);
    }

    public function setSocket($value)
    {
        $this->setParam(self::PARAM_SOCKET, $value);
    }

    public function setDatabase($value)
    {
        $this->setParam(self::PARAM_DATABASE, $value);
    }

    protected function getDefaultConfigData()
    {
        return [
            self::PARAM_HOST        => 'localhost',
            self::PARAM_PORT        => '',
            self::PARAM_SOCKET      => '',
            self::PARAM_USER        => '',
            self::PARAM_PASS        => '',
            self::PARAM_DATABASE    => '',
        ];
    }

}
