<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Config;

use XDev\Utils\System;

/**
 * Class Modules
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Modules extends \XDev\Base\AConfigFile
{
    const PARAM_MODULES = 'modules';
    const PARAM_MTIME = 'mtime';
    const FILE_EXT = 'yaml';

    protected function getConfigName() {
        return 'installed_modules';
    }

    protected function getDefaultConfigData() {
        return [];
    }

    protected function getDir()
    {
        return \XDev\Config::getSelfSourceDir();
    }

    public function getInstalledModules()
    {
        return $this->getParam(self::PARAM_MODULES);
    }

    public function getMtime()
    {
        return $this->getParam(self::PARAM_MTIME);
    }

    public function setMtime($value)
    {
        return $this->setParam(self::PARAM_MTIME, $value);
    }

    public function removeModule($name)
    {
        $modules = $this->getParam(self::PARAM_MODULES);

        unset($modules[$name]);

        $this->setParam(self::PARAM_MODULES, $modules);
    }

    public function addModule($package_name, $module_name)
    {
        $modules = $this->getParam(self::PARAM_MODULES);

        $modules[$package_name] = [
            'moduleName' => $module_name
        ];

        ksort($modules);

        $this->setParam(self::PARAM_MODULES, $modules);

    }
}
