<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Config;

/**
 * Class Local
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Local extends \XDev\Base\AConfigFile
{
    const PARAM_INSTANCE = 'instance';

    protected function getConfigName()
    {
        return 'local';
    }

    protected function getDir()
    {
        return \XDev\Config::getXDevDir();
    }

    public function setCurrentInstance($instance_name)
    {
        $this->setParam(self::PARAM_INSTANCE, $instance_name);
    }

    public function getCurrentInstance()
    {
        return $this->getParam(self::PARAM_INSTANCE);
    }

    protected function getDefaultConfigData() {
        return [
            self::PARAM_INSTANCE => ''
        ];
    }

}
