<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Config;

use XDev\Utils\Filesystem;
use XDev\Utils\Git;
use Symfony\Component\Console\Exception\RuntimeException;
use XDev\Core\Component\GitignoreBuilder\Builder as GitignoreBuilder;

/**
 * Class Repo
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Repo extends \XDev\Base\AConfigFile
{
    const FILENAME_GITIGNORE = '.gitignore';
    const BRANCH_NAME_LIVE = 'live'; // Do not change
    const DIR_PRESERVED = 'preserved';

    final protected function getConfigName() {
        return '';
    }

    final protected function getDefaultConfigData() {
        return [];
    }

    protected function getDir() {

        return \XDev\Config::getXDevDir();
    }

    final public function getPreservedFilesDir() {
        return \XDev\Config::getXDevInstanceDir(\XDev\Config\Main::INSTANCE_NAME_LIVE) . \XDev::DS . self::DIR_PRESERVED;
    }

    final public function getGitignoreFilenameFull() {
        return \XDev::getSoftwareDir() . \XDev::DS . self::getGitignoreFilename();
    }

    final public function getGitignoreFilename() {
        return self::FILENAME_GITIGNORE;
    }

    final public function getGitignoreContent()
    {

        $builder = $this->createGitignoreBuilder();
;
        return $this->getCoreGitignore() . "\n" . $builder->getSource();
    }

    final public function getCoreGitignore() {
        $db_cfg_filename = \XDev::getConfig('db')->getConfigFilename();
        $local_cfg_filename = \XDev::getConfig('local')->getConfigFilename();

        $xdev_dir = \XDev\Config::DIR_XDEV;
// TODO: Add link to custom gitignore file
        return <<<EOT

#
# (!) WARNING! This file is generated automatically, your changes might be overwriten."
#

*
!*/
!/.gitignore

#**********************************
# X-DEV Tools secure files
#**********************************
!$xdev_dir/**
$xdev_dir/**/var/*
$xdev_dir/**/$db_cfg_filename
$xdev_dir/$local_cfg_filename

EOT;
    }

    protected function createGitignoreBuilder() {
        return new GitignoreBuilder();
    }

    /*
     * List of ignored files to restore on deployment
     * [
     *  'file' => 'path/to/file',
     *  'contentFilter' => 'contentFilter', // filtering method
     * ]
     */
    public function getPreservedFiles()
    {
        return [];
    }
}
