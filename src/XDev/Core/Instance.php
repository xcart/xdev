<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Core;

/**
 * Class Instance
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Instance
{
    const PARAM_NAME            = 'name';
    const PARAM_REPO_URL        = 'repo_url';
    const PARAM_SOFTWARE_DIR    = 'software_dir';
    const PARAM_MAIN_BRANCH     = 'main_branch';
    const PARAM_HTTP_HOST       = 'http_host';
    const PARAM_HTTPS_HOST      = 'https_host';
    const PARAM_WEB_DIR         = 'web_dir';
    const PARAM_SSL_ENABLED     = 'ssl_enabled';
    const PARAM_SSH_HOST        = 'ssh_host';
    const PARAM_SSH_PORT        = 'ssh_port';
    const PARAM_SSH_USER        = 'ssh_user';

    private $params;
    private $setterCallback;

    public function __construct($params, callable $setterCallback = null) 
    {
        $this->params = $params;
        $this->setterCallback = $setterCallback;
    }

    protected function getParam($name)
    {
        return isset($this->params[$name]) ? $this->params[$name] : null;
    }

    public function setParam($name, $value)
    {
        $this->params[$name] = $value;

        if ($this->setterCallback) {
            call_user_func_array($this->setterCallback, [$this, $name, $value]);
        }
    }

    public function getName()
    {
        return $this->getParam(self::PARAM_NAME);
    }

    public function getRepositoryUrl()
    {
        return $this->getParam(self::PARAM_REPO_URL);
    }

    public function getMainBranch()
    {
        return $this->getParam(self::PARAM_MAIN_BRANCH);
    }

    public function getSoftwareDir()
    {
        return $this->getParam(self::PARAM_SOFTWARE_DIR);
    }

    public function getHttpHost()
    {
        return $this->getParam(self::PARAM_HTTP_HOST);
    }

    public function getHttpsHost()
    {
        return $this->getParam(self::PARAM_HTTPS_HOST);
    }

    public function getWebDir()
    {
        return $this->getParam(self::PARAM_WEB_DIR);
    }

    public function getWebBaseURL($is_secure = false)
    {
        return ($is_secure ? 'https://' . $this->getHttpsHost() : 'http//' . $this->getHttpHost()) . $this->getWebDir();
    }

    public function isSSLEnabled()
    {
        return $this->getParam(self::PARAM_SSL_ENABLED);
    }

    public function getSSHHost()
    {
        return $this->getParam(self::PARAM_SSH_HOST);
    }

    public function getSSHPort()
    {
        return $this->getParam(self::PARAM_SSH_PORT);
    }

    public function getSSHUser()
    {
        return $this->getParam(self::PARAM_SSH_USER);
    }

    public function setMainBranch($value)
    {
        $this->setParam(self::PARAM_MAIN_BRANCH, $value);
    }

    public function setSoftwareDir($value)
    {
        $this->setParam(self::PARAM_SOFTWARE_DIR, $value);
    }

    public function setHttpHost($value)
    {
        $this->setParam(self::PARAM_HTTP_HOST, $value);
    }

    public function setHttpsHost($value)
    {
        $this->setParam(self::PARAM_HTTPS_HOST, $value);
    }

    public function setWebDir($value)
    {
        $this->setParam(self::PARAM_WEB_DIR, $value);
    }

    public function setSSLEnabled($value)
    {
        $this->setParam(self::PARAM_SSL_ENABLED, (bool) $value);
    }

    public function setSSHHost($value)
    {
        $this->setParam(self::PARAM_SSH_HOST, $value);
    }

    public function setSSHPort($value)
    {
        $this->setParam(self::PARAM_SSH_PORT, $value);
    }

    public function setSSHUser($value)
    {
        $this->setParam(self::PARAM_SSH_USER, $value);
    }
}
