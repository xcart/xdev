<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Core;

/**
 * Class Hook
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Hook {
    private static $listeners;

    public static function invoke($hook)
    {
        if (isset(self::$listeners[$hook])) {
            foreach (self::$listeners[$hook] as $func) {
                if (is_callable($func)) {
                    call_user_func($func);
                }
            }
        }
    }

    public static function addListener($hook, callable $call)
    {
        self::$listeners[$hook][] = $call;
    }

}
