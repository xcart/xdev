<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Core;

/**
 * Software engine detector Class
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */

class EngineDetector
{
    private $engineClasses = [];
    private $engines = [];

    private $searchDir = '';

    public function getEngineByCode($code) {

        if (!isset($this->engines[$code])) {

            if (!isset($this->engineClasses[$code])) {
                throw new \Exception(sprintf('Invalid engine code: \'%s\'', $code));
            }

            $engineClass = $this->engineClasses[$code];

            $this->engines[$code] = new $engineClass();
        }

        return $this->engines[$code];
    }

    public function getEngineCodes() {
        return array_keys($this->engineClasses);
    }

    public function addEngine($className) {

        if (!class_exists($className)) {
            throw new \Exception(sprintf('Engine \'%s\' does not exist', $className));
        }

        $code = $className::getEngineCode();

        if (!$code) {
            throw new \Exception('Engine code cannot be empty');
        }

        if (isset($this->engineClasses[$code])) {
            throw new \Exception(sprintf('Engine for \'%s\' has already been added %s', $code, $this->engineClasses[$code]));
        }

        $this->engineClasses[$code] = $className;

    }

    public function setSearchDir($dir) {
        $this->searchDir = $dir;
    }

    public function detectEngine() {
        if (!$this->searchDir) {
            throw new \Exception('Search directory is not set. Use the setSearchDir() method.');
        }

        $found = false;

        foreach ($this->engineClasses as $engineCode => $engineClass) {

            if ($engineClass::detect($this->searchDir)) {
                $found = true;
                break;
            }
        }

        if ($found === true) {
            return $this->getEngineByCode($engineCode);
        }

        return false;

    }

}
