<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Core\Component\GitignoreBuilder;

/**
 * Class Section
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Section
{
    private $rules = [];
    private $name;
    private $description;
    private $builder;
    private $currentIndex;

    public function __construct($builder, $name, $description)
    {
        $this->builder = $builder;
        $this->name = $name;
        $this->description = $description;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function after($rule)
    {
        $index = array_search($rule, $this->rules);
        $this->currentIndex = $index !== false ? $index : count($this->rules);

        return $this;
    }

    public function addRule($rule)
    {
        if ($this->currentIndex == count($this->rules) - 1) {
            $this->rules[] = $rule;

        } else {
            array_splice($this->rules, $this->currentIndex, 0, [$rule]); 
        }

        $this->currentIndex++;

        return $this;
    }

    public function replaceRule($rule, $newRule)
    {
        $index = array_search($rule, $this->rules);
        $this->rules[$index] = $newRule;

        return $this;
    }

    public function removeRule($rule)
    {
        $index = array_search($rule, $this->rules);
        unset($this->rules[$index]);

        return $this;
    }

    public function end()
    {
        return $this->builder;
    }

    public function getRules()
    {
        return $this->rules;
    }

}
