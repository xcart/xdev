<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Core\Component\GitignoreBuilder;

/**
 * Class GitignoreBuilder
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Builder
{
    private $sections = [];

    public function addSection($name, $description)
    {
        $section  = new \XDev\Core\Component\GitignoreBuilder\Section($this, $name, $description);
        $this->sections[$name] = $section;

        return $section;
    }

    public function section($name)
    {
        return $this->sections[$name];
    }

    public function getSource()
    {
        $result = '';

        foreach ($this->sections as $section) {
            $result .= '#' . str_repeat('*', 34) . "\n";
            $result .= '# ' . $section->getDescription() . "\n";
            $result .= '#' . str_repeat('*', 34) . "\n";

            $result .= implode("\n", $section->getRules());

            $result .= "\n\n";
        }

        return $result;
    }
}
