<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Core\VersionManager;

use XDev\Core\VersionManager\Exception as VMException;

/**
 * Version manager
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class VersionManager
{
    const MAP_FILENAME = 'versions_map';

    const IS_GREATER = 1;
    const IS_LESS = -1;
    const IS_EQUAL = 0;
 
    const ERROR_NO_ERRORS = 0;
    const ERROR_NO_CLASS = 1;
    const ERROR_INTERNAL = 2;

    private $ds = DIRECTORY_SEPARATOR;
    private $vmDir;
    private $cacheDir;
    private $version;
    private $versionDelim = '.';

    private $versionsMapTree;
    private $versionsMapPlain;

    private $mvInstances = [];
    private $floorClosestClassesCache = [];

    private $isCacheLoaded = false;

    private $vmDirChecked = false;
    private $cacheDirChecked = false;

    public function setVMDir($dir) {
        $this->vmDir = $dir;
    }

    public function setCacheDir($dir) {
        $this->cacheDir = $dir;
    }

    public function setVersion($version) {

        $this->version = $version;
    }

    public function setVersionDelimiter($delimiter) {
        $this->versionDelim = $delimiter;
    }

    public function buildCache() {
        $this->buildVersionsMapCache();
    }

    private function loadCache() {

        if (!file_exists($this->getMapFilename())) {
            $this->buildCache();
        }

        include $this->getMapFilename();

        $this->versionsMapTree = $__VM_TREE;
        $this->versionsMapPlain = $__VM_PLAIN;

        $this->isCacheLoaded = true;

        return $this->versionsMapTree;

    }

    private function checkVMDir() {

        if (!$this->vmDirChecked) {
            if ($this->vmDir === null) {
                throw new VMException('VM dir is not set. Use setVMDir() method to set the VM dir');
            }

            if (!$this->vmDir) {
                throw new VMException('VM dir is empty');
            }

            if (!file_exists($this->vmDir)) {
                throw new VMException('VM dir ' . $this->vmDir . ' does not exist');
            }

            if (!is_readable($this->vmDir)) {
                throw new VMException('VM dir is not readable');
            }

            $this->vmDirChecked = true;
        }

    }

    private function checkCacheDir() {

        if (!$this->cacheDirChecked) {
            if ($this->cacheDir === null) {
                throw new VMException('Cache dir is not set. Use setCacheDir() method to set the cache dir');
            }

            if (!$this->cacheDir) {
                throw new VMException('Cache dir is empty');
            }

            if (!is_readable($this->cacheDir)) {
                throw new VMException('Cache dir is not readable');
            }

            if (!file_exists($this->cacheDir)) {

                throw new VMException('Cache dir does not exist. Create a new dir ' . $this->cacheDir);
            }

            $this->cacheDirChecked = true;
        }

    }

    private function getMapFilename() {
        return $this->cacheDir . $this->ds . self::MAP_FILENAME . '.php';
    }

    public function getClassName($path)
    {
        if (!$path) {
            throw new VMException('Empty path \'$path\'');
        }

        if (!$this->version) {
            throw new VMException('Version is not set. Use setVersion() method to set the proper version');
        }

        if (!$this->isCacheLoaded) {
            $this->loadCache();
        }

        if (!isset($this->mvInstances[$path])) {

            $class_name = $this->getFloorClosestClass($path, $this->version);

            if (!$class_name) {
                throw new VMException('Cannot find class implementation for ' . $path, self::ERROR_NO_CLASS);
            }

            return $class_name;
        }

        return get_class($this->mvInstances[$path]);

    }

    public function clear($namespace)
    {
        if (isset($this->mvInstances[$namespace])) {
            unset($this->mvInstances[$namespace]);
        }
    }

    public function getClass($namespace)
    {
        if (!$namespace) {
            throw new VMException('Empty namespace');
        }

        if (!$this->version) {
            throw new VMException('Version is not set. Use setVersion() method to set the proper version');
        }

        if (!$this->isCacheLoaded) {
            $this->loadCache();
        }

        if (!isset($this->mvInstances[$namespace])) {

            $class_name = $this->getClassName($namespace);

            $args = func_get_args();

            array_shift($args);

            $reflection = new \ReflectionClass($class_name); 
            $this->mvInstances[$namespace] = $reflection->newInstanceArgs($args); 

        }

        return $this->mvInstances[$namespace];
    }

    public function getNamespaces()
    {
        return array_keys($this->versionsMapPlain);
    }

    public function getClassNames($namespace) {

        if (!$this->isCacheLoaded) {
            $this->loadCache();
        }

        $result = [];

        foreach ($this->versionsMapPlain as $path => $value) {
            if (strpos($path, $namespace) === 0) {
                $result[] = $this->getClassName($path);
            }
        }

        return $result;

    }

    private function getFloorClosestClass($path, $version) {

        if (!isset($this->floorClosestClassesCache[$path][$version])) {

            if (isset($this->versionsMapPlain[$path][$version])) {
                $this->floorClosestClassesCache[$path][$version] = $this->versionsMapPlain[$path][$version];

            } else {

                $this->floorClosestClassesCache[$path][$version] = $this->searchFloorClosestVersion($path, $version);
            }
        }

        return $this->floorClosestClassesCache[$path][$version];
    }

    private function searchFloorClosestVersion($path, $version) {

        $result = false;

        if (isset($this->versionsMapTree[$path])) {
            $v_parts = explode($this->versionDelim, $version);

            $result = $this->searchFloorClosestVersionRecursive($this->versionsMapTree[$path], $v_parts);
        }

        return $result;
    }

    private function searchFloorClosestVersionRecursive(array $vmap, array $v_parts) {

        if (isset($vmap[$v_parts[0]])) { 
            $closest_digit = $v_parts[0];
        } else {

            foreach (array_reverse($vmap, true) as $k => $v) {
                if ($k <= $v_parts[0]) {
                    $closest_digit = $k;

                    break;
                }
            }
        }

        if (is_array($vmap[$closest_digit])) {
            $result = self::searchFloorClosestVersionRecursive($vmap[$closest_digit], array_slice($v_parts, 1));

        } else {
            $result = $vmap[$closest_digit];
        }

        return $result;

    }

    protected function versionToNamespace($v) {

        return 'v' . str_replace($this->versionDelim, $v);
    }

    protected function namespaceToVersion($v) {
        $v = ltrim($v, 'v');

        return str_replace('_', $this->versionDelim, $v);
    }

    protected function isVersionNamespaceValid($v) {
        return preg_match('/^v(\d+[a-zA-z]*)(\_\d+[a-zA-z]*)*$/', $v);
    }

    protected function buildVersionsMapCache() {
        if (\Phar::running(false)) {
            //throw new VMException('Cannot build version map in Phar mode');
        }

        if (!file_exists($this->cacheDir)) {
            mkdir($this->cacheDir, 0700, true);
        }

        $this->checkVMDir();

        $this->checkCacheDir();

        $map = [];

        $rdt = new \RecursiveDirectoryIterator($this->vmDir);
        $it = new \RecursiveIteratorIterator($rdt);
        $files = new \RegexIterator($it, '/^.+\.php$/', \RegexIterator::GET_MATCH);

        foreach ($files as $file) {

            foreach ($file as $f) {
                $splFileInfo = new \SplFileInfo($f);

                $class_name = $this->getClassFromFile($splFileInfo->getPathname());

                if (!$class_name) {
                    continue;
                }

                if (!class_exists($class_name)) {
                    throw new VMException('Class ' . $class_name . ' does not exist!');
                }

                if (!$class_name) {
                    continue;
                }

                $parts = explode('\\', $class_name);

                if (count($parts) < 3) {
                    continue;
                }

                $version_n = $parts[count($parts) - 2];

                if ($this->isVersionNamespaceValid($version_n)) {
                    $version = $this->namespaceToVersion($version_n);

                    $tmp = explode($this->ds, $files->getSubPath());
                    $class_name_short = implode('\\', array_slice($tmp, 0, count($tmp) - 1));

                    if (!isset($map[$class_name_short])) {
                        $map[$class_name_short] = [];
                    }

                    $map[$class_name_short][$version] = $class_name;
                }

            }
        }

        foreach ($map as $k => &$v) {
            uksort($v, function($a, $b) {
                return $this->compareVersions($a, $b) === 1;
            });
        }

        foreach ($map as $class_name_short => $versions) {
            foreach ($versions as $version => $class_name) {
                $parts = explode($this->versionDelim, $version);

                $code = '$this->versionsMapTree[' . '\'' . $class_name_short . '\'' . ']';

                foreach ($parts as $k => $p) {
                    $code .= '[\'' . $parts[$k] . '\']';
                }

                $code .= " = '$class_name';\n";

                $code .= "\$this->versionsMapPlain['$class_name_short']['$version'] = '$class_name';";

                eval($code);
            }
        }

        $content = <<<END
<?php
\$__VM_TREE = [];
\$__VM_PLAIN = [];\n\n
END;

        $map_tree = var_export($this->versionsMapTree, true);
        $map_plain = var_export($this->versionsMapPlain, true);

        $content .= <<<END
\$__VM_TREE = $map_tree;\n
\$__VM_PLAIN = $map_plain;
END;
        file_put_contents($this->getMapFilename(), $content);

        return $this->versionsMapTree;

    }

    public function compareVersions($ver1, $ver2) {
        $p1 = explode($this->versionDelim, $ver1);
        $p2 = explode($this->versionDelim, $ver2);

        $compared = self::IS_EQUAL;

        foreach ($p1 as $k => $v) {
            if (isset($p2[$k])) {
                if ($p1[$k] > $p2[$k]) {
                    $compared = self::IS_GREATER;
                } elseif ($p1[$k] < $p2[$k]) {
                    $compared = self::IS_LESS;
                }
            } else {
                $compared = self::IS_GREATER;
            }

            if ($compared !== self::IS_EQUAL) {
                break;
            }
        }

        if ($compared === self::IS_EQUAL) {
            foreach ($p2 as $k => $v) {
                if (isset($p1[$k])) {
                    if ($p2[$k] > $p1[$k]) {
                        $compared = self::IS_LESS;
                    } elseif ($p2[$k] < $p1[$k]) {
                        $compared = self::IS_GREATER;
                    }
                } else {
                    $compared = self::IS_LESS;
                }

                if ($compared !== self::IS_EQUAL) {
                    break;
                }
            }
        }

        return $compared;

    }

    private function getClassFromFile($path_to_file)
    {
        //Grab the contents of the file
        $contents = file_get_contents($path_to_file);

        //Start with a blank namespace and class
        $namespace = $class = "";

        //Set helper values to know that we have found the namespace/class token and need to collect the string values after them
        $getting_namespace = $getting_class = false;

        //Go through each token and evaluate it as necessary
        foreach (token_get_all($contents) as $token) {

            //If this token is the namespace declaring, then flag that the next tokens will be the namespace name
            if (is_array($token) && $token[0] == T_NAMESPACE) {
                $getting_namespace = true;
            }

            //If this token is the class declaring, then flag that the next tokens will be the class name
            if (is_array($token) && $token[0] == T_CLASS) {
                $getting_class = true;
            }

            //While we're grabbing the namespace name...
            if ($getting_namespace === true) {

                //If the token is a string or the namespace separator...
                if(is_array($token) && in_array($token[0], [T_STRING, T_NS_SEPARATOR])) {

                    //Append the token's value to the name of the namespace
                    $namespace .= $token[1];

                }
                else if ($token === ';') {

                    //If the token is the semicolon, then we're done with the namespace declaration
                    $getting_namespace = false;

                }
            }

            //While we're grabbing the class name...
            if ($getting_class === true) {

                //If the token is a string, it's the name of the class
                if(is_array($token) && $token[0] == T_STRING) {

                    //Store the token's value as the class name
                    $class = $token[1];

                    //Got what we need, stope here
                    break;
                }
            }
        }

        //Build the fully-qualified class name and return it
        return $namespace ? $namespace . '\\' . $class : $class;

    }
}
