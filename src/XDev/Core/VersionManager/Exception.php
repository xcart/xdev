<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Core\VersionManager;

use XDev\Core\VersionManager\VersionManager;

/**
 * Class Exception
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Exception extends \Exception
{

    private $errorCode = VersionManager::ERROR_NO_ERRORS;

    public function getErrorCode() {
        return $this->errorCode;
    }
    public function __construct($message, $error_code = VersionManager::ERROR_INTERNAL) {
        $this->errorCode = $error_code;

        parent::__construct('[VersionManager] ' . $message);
    }
}
