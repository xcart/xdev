<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Core;

use mysqli;

/**
 * Class DbConnection
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class DbConnection
{
    protected $conn;

    /**
     * {@inheritDoc}
     */
    public function __construct($host, $username, $password, $database, $port = null, $socket = null)
    {
        $port = $port ? $port : null;
        $socket = $socket ? $socket : null;

        $this->conn = new mysqli($host, $username, $password, $database, $port, $socket);

        if ($this->conn->connect_error) {
            throw new \Symfony\Component\Console\Exception\RuntimeException(
                'Connection with the database failed: ' .  $this->conn->connect_errno . ' ' . $this->conn->connect_error
            );
        }

    }

    public static function createNewConnection($host, $username, $password, $database)
    {
        $calledClass = get_called_class();

        $class = new $calledClass();

    }

    public function getConnection()
    {
        return $this->conn;
    }

    public function freeResult($res)
    {
        return mysqli_free_result($res);
    }

    public function querySingleScalar($query)
    {
        $query .= ' LIMIT 0,1';
        $result = $this->query($query);
        reset($result[0]);

        return current($result[0]);
    }

    public function query($query)
    {
        $result =  [];

        if ($res = $this->conn->query($query)) {
            while ($row = $res->fetch_assoc()) {
                $result[] = $row;
            }

        }

        return $result;
    }

    public function getTables()
    {
        $database = \XDev::getConfig('db')->getParam('database');

        $query = (
            'SELECT TABLE_NAME'
            . ' FROM information_schema.TABLES'
            . " WHERE table_schema = '$database'"
        );

        $tables = $this->queryColumnn($query);

        return $tables;
    }

    public function queryColumnn($query)
    {
        $result =  [];

        if ($res = $this->conn->query($query)) {
            while ($row = $res->fetch_row()) {
                $result[] = $row[0];
            }

        }

        return $result;
    }

}
