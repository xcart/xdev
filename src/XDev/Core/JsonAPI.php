<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Core;

/**
 * Class JsonAPI
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class JsonAPI
{
    const STATUS_OK = 'ok';
    const START_MARKER = '<<<JSON_54872';
    const END_MARKER = '>>>JSON_54872';

    static $output = [
        'errors'    => [],
        'warnings'  => [],
        'debug'     => [],
        'result'    => [],
    ];

    public static function outputFlush()
    {
        echo self::START_MARKER . json_encode(self::$output) . self::END_MARKER;
    }

    public static function parseOutput($output)
    {
        if (($startpos = strpos($output, self::START_MARKER)) !== false) {
            $startpos = $startpos + strlen(self::START_MARKER);
            $endpos = strpos($output, self::END_MARKER);
            $o = substr($output, $startpos, $endpos - $startpos);

            return json_decode($o, true);
        }

        return false;
    }

    public static function setResult($result)
    {
        self::$output['result'] = $result;
    }

    public static function setStatus($status)
    {
        self::$output['status'] = $status;
    }

    public static function getStatus()
    {
        return self::$output['status'];
    }

    public static function addError($message)
    {
        $message = trim($message);

        if ($message) {
            self::$output['errors'][] = $message;
        }
    }

    public static function addDebug($message)
    {
        $message = trim($message);

        if ($message) {
            self::$output['debug'][] = $message;
        }
    }

    public static function addWarning($message)
    {
        $message = trim($message);

        if ($message) {
            self::$output['warnings'][] = $message;
        }
    }

}
