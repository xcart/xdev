<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Core;

use Symfony\Component\Console\Exception\LogicException;
use XDev\Utils\Shell;
use XDev\Utils\Filesystem;
use XDev\Core\Symfony\Console\Helper as ConsoleHelper;

/**
 * Class SSHConnection
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class SSHConnection
{
    const SCP_UPLOAD    = 'upload';
    const SCP_DOWNLOAD  = 'download';

    private $host;
    private $port;
    private $user;
    private $privateKey;
    private $useAgentForwarding = false;
    private $useMultiplexing = false;
    private $controlPersist = 300;
    private $virtualTerminal = false;
    private $multiplexing = false;

    public function __construct($host, $user, $port = 22, $private_key = null)
    {
        $this->host = $host;
        $this->user = $user;
        $this->port = $port;

        $this->privateKey = $private_key;

        register_shutdown_function(function()
        {
            if ($this->useAgentForwarding) {
                $this->stopAgentForwarding();
            }

            if ($this->useMultiplexing) {
                try {
                    $this->disableMultiplexing();
                } catch (\Exception $e) {
                    
                }
            }
        });
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getPrivateKey()
    {
        return $this->privateKey;
    }

    public function multiplexingEnabled()
    {
        return $this->useMultiplexing;
    }

    public function enableVirtualTerminal()
    {
        $this->virtualTerminal = true;
    }

    public function disableVirtualTerminal()
    {
        $this->virtualTerminal = false;
    }

    public function enableMultiplexing()
    {
        $this->useMultiplexing = true;

        $dir = dirname($this->getControlPath());

        if (!Filesystem::exists($dir)) {
            Filesystem::mkdir($dir);
        }
    }

    public function disableMultiplexing()
    {
        $this->useMultiplexing = false;

        Shell::exec(sprintf('ssh -o ControlPath=\'%s\' -O exit %s', $this->getControlPath(), $this->getHost()));

        $this->multiplexing = false;
    }

    private function getControlPath()
    {
        return \XDev\Config::getGlobalConfigDir() . '/.ssh/xdev_' . substr(md5($this->getHost() . $this->getUser() . $this->getPort()), 1, 8);
    }

    public function startAgentForwarding()
    {
        $this->useAgentForwarding = true;

        $translateOutput = Shell::isTranslateOutput();

        Shell::setTranslateOutput(false);

        $this->agentCode = Shell::exec('ssh-agent -s');

        Shell::setTranslateOutput($translateOutput);
    }

    public function stopAgentForwarding()
    {
        $this->useAgentForwarding = false;

        Shell::exec([
            $this->agentCode,
            'ssh-add -D',
            'eval $(ssh-agent -k)'
        ]);
    }

    public function agentAddKey($id_rsa)
    {
        if (!$this->useAgentForwarding) {
            throw new LogicException('Agent forwarding is not enabled');
        }

        $commands = Shell::exec([
            $this->agentCode,
            'ssh-add ' . $id_rsa
        ]);

    }

    public function test()
    {
        $translateOutput = Shell::isTranslateOutput();
        Shell::setTranslateOutput(false);

        $result = Shell::exec('ssh -o ConnectTimeout=15 -T ' . $this->buildSSHParams());

        Shell::setTranslateOutput($translateOutput);

    }

    public function connect()
    {
        $c = [];

        $command = 'ssh';

        $this->multiplexing = true;

        if ($this->useAgentForwarding) {
            // Agent must be added to the very first ssh command
            $c[] = $this->agentCode;
            $command .= ' -A';
        }

        $command .= ' -o ConnectTimeout=15 -T ' . $this->getMultiplexingParams() . ' ' . $this->buildSSHParams();

        $c[] = $command;

        $translateOutput = Shell::isTranslateOutput();
        Shell::setTranslateOutput(false);

        $result = Shell::exec($c);

        Shell::setTranslateOutput($translateOutput);

    }

    public function execSSHCommand($command, $agent_forwarding = false, $background = false)
    {
        if ($this->useMultiplexing && !$this->multiplexing) {
            ConsoleHelper::write('Connecting to ' . $this->getHost() . '... ');

            $this->multiplexing = true;
            $this->connect();

            ConsoleHelper::writeln('OK');
        }

        $c = [];

        if ($this->useAgentForwarding && $agent_forwarding) {
            $c[] = $this->agentCode;
        }

        $c[] = $command;

        return Shell::exec($c, false, $background);
    }

    public function exec($command, $agent_forwarding = false, $background = false)
    {
        $command = $this->buildSSHCommand($command);

        return $this->execSSHCommand($command, $agent_forwarding, $background);
    }

    public function upload($local_filename, $remote_filename, $background = false)
    {
        $command = $this->buildSCPCommand($remote_filename, $local_filename, self::SCP_UPLOAD);

        return $this->execSSHCommand($command, false, $background);
    }

    public function download($remote_filename, $local_filename, $background = false)
    {
        $command = $this->buildSCPCommand($remote_filename, $local_filename, self::SCP_DOWNLOAD);

        return $this->execSSHCommand($command, false, $background);
    }

    private function getMultiplexingParams()
    {
        return ' -o ControlMaster=auto -o ControlPersist=' . $this->controlPersist . '  -o ControlPath=\'' . $this->getControlPath() . '\'';
    }

    private function buildSCPCommand($remote_filename, $local_filename, $mode = self::SCP_DOWNLOAD)
    {
        $result = 'scp';

        if ($this->useMultiplexing) {
            $result .= $this->getMultiplexingParams();
        }

        if ($this->privateKey) {
            $result .= ' -i ' . $this->privateKey;
        }

        if ($this->getPort()) {
            $result .= ' -P' . $this->getPort();
        }

        if ($mode == self::SCP_UPLOAD) {
            $result .= ' ' . $local_filename;
        }

        $result .= ' ' . $this->getUser() . '@' . $this->getHost();

        $result .= ':' . $remote_filename;

        if ($mode == self::SCP_DOWNLOAD) {
            $result .= ' ' . $local_filename;
        }

        return $result;
    }

    private function buildSSHParams()
    {
        $result = '';

        if ($this->privateKey) {
            $result .= ' -i ' . $this->privateKey;
        }

        if ($this->getPort()) {
            $result .= ' -p' . $this->getPort();
        }

        $result .= ' ' . $this->getUser() . '@' . $this->getHost();

        return $result;

    }

    private function buildSSHCommand($command)
    {
        if (!is_array($command)) {
            $command = [$command];
        }

        $result = 'ssh';

        if ($this->useAgentForwarding) {
            $result .= ' -A';
        }

        if ($this->virtualTerminal) {
            $result .= ' -tt ';
        }

        if ($this->useMultiplexing) {
            $result .= $this->getMultiplexingParams();
        }

        $result .= $this->buildSSHParams();

        $result .= ' "' . implode('; ', $command) . '"';

        return $result;
    }

}
