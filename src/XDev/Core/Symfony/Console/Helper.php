<?php

namespace XDev\Core\Symfony\Console;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Helper\ProgressBar;

class Helper
{
    const V_TYPE_INTEGER    = 1;
    const V_TYPE_STRING     = 2;
    const V_TYPE_NONE_EMPTY = 3;

    const FG_BLACK    = 30;
    const FG_RED      = 31;
    const FG_GREEN    = 32;
    const FG_YELLOW   = 33;
    const FG_BLUE     = 34;
    const FG_MAGENTA  = 35;
    const FG_CYAN     = 36;
    const FG_WHITE    = 37;
    const FG_DEFAULT  = 39;
    const FG_DARK_GRAY     = 90;

    private static $paragraphStarted = false;

    public static function startNewParagraph()
    {
        self::$paragraphStarted = true;
    }

    public static function writeln($message = '')
    {
        $output = \XDev\Application::getInstance()->getCurrentOutput();

        if (self::$paragraphStarted) {
            $output->writeln('');
            self::$paragraphStarted = false;
        }

        $output->writeln($message);

    }

    public static function write($message = '')
    {
        $output = \XDev\Application::getInstance()->getCurrentOutput();

        if (self::$paragraphStarted) {
            $output->writeln('');
            self::$paragraphStarted = false;
        }

        $output->write($message);

    }

    public static function askYesNo($message)
    {

        $input = \XDev\Application::getInstance()->getCurrentInput();
        $output = \XDev\Application::getInstance()->getCurrentOutput();

        if (self::$paragraphStarted) {
            $output->writeln('');
            self::$paragraphStarted = false;
        }

        $question = new Question(sprintf('<fg=yellow;options=bold>%s </> (yes/no): </>', $message));

        $helper = \XDev\Application::getInstance()->getSCApp()->getHelperSet()->get('question');

        $question->setValidator(function ($answer) {
            if ($answer && !preg_match('/^y/i', $answer)) {
                throw new \RuntimeException(
                    'Please type \'yes\' or \'no\''
                );
            }

            return $answer;
        });

        $question->setMaxAttempts(3);

        return $helper->ask($input, $output, $question);

    }

    public static function askForParam($message, $validation_type = self::V_TYPE_STRING)
    {

        $input = \XDev\Application::getInstance()->getCurrentInput();
        $output = \XDev\Application::getInstance()->getCurrentOutput();

        if (self::$paragraphStarted) {
            $output->writeln('');
            self::$paragraphStarted = false;
        }

        $helper = \XDev\Application::getInstance()->getSCApp()->getHelperSet()->get('question');

        $question = new Question(sprintf('<fg=yellow;options=bold>%s: </>', $message));

        $question->setNormalizer(function ($value) {
            return $value ? trim($value) : '';
        });

        $question->setValidator(function ($answer) use ($validation_type) {
            switch ($validation_type) {
                case self::V_TYPE_INTEGER:
                    if (!is_integer($answer)) {
                        throw new \RuntimeException(
                            'Entered value must be and integer number'
                        );
                    }
                    break;

                case self::V_TYPE_STRING:
                    if (!is_string($answer)) {
                        throw new \RuntimeException(
                            'Entered value must be string'
                        );
                    }
                    break;

                case self::V_TYPE_NONE_EMPTY:
                    if (!$answer) {
                        throw new \RuntimeException(
                            'Entered value must not be empty'
                        );
                    }
                    break;
                default:
                    break;
            }

            return $answer;
        });

        $question->setMaxAttempts(2);

        return $helper->ask($input, $output, $question);

    }

    public static function fileDumpPogressBar($filesize)
    {
        $output = \XDev\Application::getInstance()->getCurrentOutput();

        $progressBar = new ProgressBar($output, $filesize);
        $format = '    %current_mb%';

        $progressBar->setFormat($format);

        return $progressBar;

    }

    public static function fileTransferPogressBar($filesize)
    {
        $output = \XDev\Application::getInstance()->getCurrentOutput();

        $progressBar = new ProgressBar($output, $filesize);
        $format = '    %current_mb% / %total_mb% [%bar%] %percent:3s%% %estimated:-6s%';

        if (self::$paragraphStarted) {
            $format = "\n" . $format;
            self::$paragraphStarted = false;
        }

        $progressBar->setFormat($format);

        return $progressBar;

    }

    public static function colorize($text, $color, $background = false)
    {
        if (30 <= $color && $color < 40 || 90 <= $color && $color < 100) {
            $value = $color;
        } else {
            $value = '38;5;' . $color;
        }

        if ($background !== false) {
            $value = $value . ';48;5;' . $background;
        }

        return sprintf("\033[%sm%s\033[0m", $value, $text);
    }
}

ProgressBar::setPlaceholderFormatterDefinition(
    'current_mb',
    function (ProgressBar $progressBar, OutputInterface $output) {
        return \XDev\Utils\Converter::formatFileSize($progressBar->getProgress());
    }
);

ProgressBar::setPlaceholderFormatterDefinition(
    'total_mb',
    function (ProgressBar $progressBar, OutputInterface $output) {
        return \XDev\Utils\Converter::formatFileSize($progressBar->getMaxSteps());
    }
);
