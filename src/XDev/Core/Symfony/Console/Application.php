<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Core\Symfony\Console;

use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Application
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Application extends \Symfony\Component\Console\Application
{
    public function renderException(\Exception $e, OutputInterface $output)
    {
        if ($e instanceof \XDev\Core\Exception) {
            $this->doRenderException($e, $output);

        } else {
             parent::renderException($e, $output);
        }
    }
}
