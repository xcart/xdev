<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Core\EngineManager;

use XDev\Core\EngineManager\EngineManager;
use XDev\Core\VersionManager\VersionManager;

/**
 * Engine manager
 * 
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class EngineManager
{
    const MAP_FILENAME = 'namespaces';

    private $vm;
    private $namespacesMap;
    private $searchDirs;
    private $cacheDir;
    private $engineCode;
    private $version;
    private $isCacheLoaded = false;

    public function addSearchDir($dir)
    {
        if (empty($dir)) {
            throw new Exception(sprintf('EM directory cannnot be empty', $dir));
        }

        if (file_exists($dir)) {
            $this->searchDirs[] = $dir;
        }

    }

    public function setCacheDir($dir)
    {
        $this->cacheDir = $dir;
    }

    public function setEngineCode($engineCode)
    {
        $this->engineCode = $engineCode;
    }

    public function getEngineCode()
    {
        return $this->engineCode;
    }

    public function setVersion($version)
    {
        $this->version = $version;

        $this->resetVM();
    }

    public function getVersion()
    {
        return $this->version;
    }

    private function resetVM()
    {
        $this->vm = null;
    }

    public function buildCache()
    {
        $map = [];

        \XDev\Utils\Filesystem::remove($this->cacheDir);

        foreach ($this->searchDirs as $dir) {
            foreach (new \DirectoryIterator($dir) as $fileInfo) {

                if (
                    $fileInfo->isDot()
                    || !$fileInfo->isDir()
                )  {
                    continue;
                }

                $engine_code = $fileInfo->getFilename();

                if (!isset($map[$engine_code])) {
                    $map[$engine_code] = [];
                }

                $vm = new VersionManager();

                $vm_dir = $dir . \XDev::DS . $engine_code;
                $vm->setVMDir($vm_dir);

                $vmap_code = substr(md5($vm_dir), 0, 15);

                $vm->setCacheDir($this->cacheDir . \XDev::DS . $engine_code . \XDev::DS . $vmap_code);

                $vm->buildCache();

                foreach ($vm->getNamespaces() as $namespace) {
                    $map[$engine_code][$namespace] = $vmap_code;
                }

            }
        }

        $content = <<<END
<?php
\$__NS_MAP = [];\n
END;

        $map = var_export($map, true);

        $content .= <<<END
\$__NS_MAP = $map;
END;
        file_put_contents($this->getMapFilename(), $content);

    }

    private function getMapFilename() {
        return $this->cacheDir . DIRECTORY_SEPARATOR . self::MAP_FILENAME . '.php';
    }

    private function loadCache()
    {
        if (!$this->isCacheLoaded) {
            if (!file_exists($this->getMapFilename())) {
                return false;
            }

            include $this->getMapFilename();

            $this->namespacesMap = $__NS_MAP;

            $this->isCacheLoaded = true;
        }
    }

    public function getVM($namespace)
    {
        if (!$this->engineCode) {
            throw new \RuntimeException('Engine code is not set!');
        }

        $this->loadCache();

        if (!isset($this->namespacesMap[$this->engineCode][$namespace])) {
            return false;
        }

        $id = $this->namespacesMap[$this->engineCode][$namespace];

        if (!isset($this->vm[$id])) {
            $id = $this->namespacesMap[$this->engineCode][$namespace];

            $this->vm[$id] = new VersionManager();
            $this->vm[$id]->setCacheDir($this->cacheDir . DIRECTORY_SEPARATOR . $this->getEngineCode() . DIRECTORY_SEPARATOR . $id);
            $this->vm[$id]->setVersion($this->version);

        }

        return $this->vm[$id];
    }

    public function clear($namespace)
    {
        $this->getVM($namespace)->clear($namespace);
    }

    public function get($namespace)
    {
        $args = func_get_args();

        return call_user_func_array([$this->getVM($namespace), 'getClass'], $args);

     }

    public function findClasses($path)
    {
        $this->loadCache();

        $result = [];
        $vms = [];

        if ($this->namespacesMap) {
            foreach ($this->namespacesMap[$this->engineCode] as $namespace => $id) {

                if (strpos($namespace, $path) === 0) {
                    $vms[$id] = $this->getVM($namespace);
                }
            }
        }

        foreach ($vms as $vm) {
            $result = array_merge($result, $vm->getClassNames($path));
        }

        return $result;
    }
}
