<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Core\EngineManager;

use XDev\Core\EngineManager\EngineManager;

/**
 * Class Exception
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Exception extends \Exception
{

    public function __construct($message)
    {
        parent::__construct('[EngineManager] ' . $message);
    }
}
