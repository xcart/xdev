<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev;

use XDev\Utils\Filesystem;

/**
 * Config
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Config
{

    const DIR_XDEV = '.xdev';
    const DIR_XDEV_GLOBAL = '.xdev';
    const DIR_XDEV_CACHE = 'var';
    const DIR_XDEV_TMP = 'tmp';

    const DIR_SQL = 'sql';
    const DIR_VAR = 'var';
    const DIR_CACHE = 'cache';
    const DIR_OUTPUT = 'output';
    const DIR_MODULES = 'Module';
    const DIR_XDEV_DEV_COMMON = 'dev.common';

    private $data = [];

    public function getNode($k)
    {

        if (array_key_exists($k, $this->data)) {
            return $this->data[$k];
        }

        // first try to use Engine-specific implementation if exists
        if (\XDev\EM::getEngineCode()) {
            $this->data[$k] = \XDev\EM::get('App\Config\\' . ucfirst($k));
        }

        if (empty($this->data[$k])) {
            $class_name = '\XDev\Config\\' . ucfirst($k);

            if (class_exists($class_name)) {
                $this->data[$k] = new $class_name();

            } else {
                throw new \Exception('Class "' . $class_name . '" does not exist.');
            }

       }

        return $this->data[$k];
    }

    public static function getXDevDir()
    {
        return \XDev::getSoftwareDir() . \XDev::DS . self::DIR_XDEV;
    }

    public static function getXDevCacheDir()
    {
        return self::getXDevDir() . \XDev::DS . self::DIR_XDEV_CACHE;
    }

    public static function getXDevTmpDir()
    {
        return self::getXDevCacheDir() . \XDev::DS . self::DIR_XDEV_TMP;
    }

    public static function getXDevCurrentInstanceDir()
    {
        $instance = \XDev::getCurrentInstance();

        return self::getXDevDir() . \XDev::DS . $instance;
    }

    public static function getXDevInstanceDir($name)
    {
        return self::getXDevDir() . \XDev::DS . $name;
    }

    public static function getXDevCommonDevDir()
    {
        return self::getXDevDir() . \XDev::DS . self::DIR_XDEV_DEV_COMMON;
    }

    public static function getSQLDir()
    {
        return \XDev::getSoftwareDir() . \XDev::DS . self::DIR_SQL;
    }

    public static function getCacheDir()
    {
        return self::getSelfSourceDir() . \XDev::DS . self::DIR_VAR . \XDev::DS . self::DIR_CACHE;
    }

    public static function getSelfSourceDir()
    {
        return __DIR__ . \XDev::DS . '..'. \XDev::DS . '..';
    }

    public static function getLocalModulesDir()
    {
        return __DIR__ . \XDev::DS . self::DIR_MODULES;
    }

    public static function getBuildOutputDir()
    {
        return self::getSelfSourceDir() . \XDev::DS . self::DIR_OUTPUT;
    }

    public static function getModulesDir()
    {
        return self::getSelfSourceDir() . \XDev::DS . self::DIR_VAR . \XDev::DS . self::DIR_CACHE;
    }

    public static function getVendorDir()
    {
        if (Filesystem::exists(\XDev\Config::getSelfSourcedir() . \XDev::DS . 'vendor')) {
            return \XDev\Config::getSelfSourcedir() . \XDev::DS . 'vendor';

        } else {
            return \XDev\Config::getSelfSourcedir() . \XDev::DS . '..' . \XDev::DS . '..' . \XDev::DS . '..' . \XDev::DS . 'vendor';
        }

    }

    public static function getGlobalConfigDir()
    {
        return \XDev\Utils\System::getHomeDir() . \XDev::DS . '.xdev';
    }

}
