<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev;

use XDev\Core\EngineManager\EngineManager;

/**
 * Engine manager static wrapper
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class EM extends \XDev\Core\Singleton
{
    private $em;

    private function getEM()
    {
        if (!$this->em) {
            $this->em = new EngineManager();
        }

        return $this->em;
    }

    public static function setCacheDir($dir)
    {
        self::getInstance()->getEM()->setCacheDir($dir);
    }

    public static function addSearchDir($dir)
    {
        self::getInstance()->getEM()->addSearchDir($dir);
    }

    public static function buildCache()
    {
        self::getInstance()->getEM()->buildCache();
    }

    public static function setEngineCode($engine_code)
    {
        self::getInstance()->getEM()->setEngineCode($engine_code);
    }

    public static function getEngineCode()
    {
        return self::getInstance()->getEM()->getEngineCode();
    }

    public static function setVersion($version)
    {
        self::getInstance()->getEM()->setVersion($version);
    }

    public static function getVersion()
    {
        return self::getInstance()->getEM()->getVersion();
    }

    public static function clear($namespace)
    {
        self::getInstance()->getEM()->clear($namespace);
    }

    public static function get($namespace)
    {
        $args = func_get_args();

        return call_user_func_array([self::getInstance()->getEM(), 'get'], $args);

    }

    public static function findClasses($namespace)
    {
        return self::getInstance()->getEM()->findClasses($namespace);
    }

}
