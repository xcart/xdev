<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

/**
 * Class Module
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
namespace XDev\Module\XC4;

class Module extends \XDev\Base\AModule
{

    public function getModuleDescription()
    {
        return 'X-Cart 4 engine';
    }

    public function init()
    {
        $this->getApplication()->addSoftwareEngine('XDev\Module\XC4\Engines\XCart4');
    }
}
