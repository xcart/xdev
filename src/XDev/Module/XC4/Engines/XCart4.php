<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\Engines;

/**
 * Class XCart4
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class XCart4 extends \XDev\Base\AEngine
{
    public static function getEngineCode() {
        return 'XC4';
    }

    protected function getBaseVersions() {
        return ['4.0.0'];
    }

    public static function detect($softwareDir) {
        // TODO: DRAFT VERSION BELOW

        if (file_exists($softwareDir . \XDev::DS . 'config.php')) {
            return true;
        }

        return false;
    }
}
