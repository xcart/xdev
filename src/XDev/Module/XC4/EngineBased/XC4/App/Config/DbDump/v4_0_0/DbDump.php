<?php

namespace XDev\Module\XC4\EngineBased\XC4\App\Config\DbDump\v4_0_0;

class DbDump extends \XDev\Config\DbDump
{

    protected function getDefaultConfigData()
    {
        return parent::getDefaultConfigData() + [
            self::PARAM_EXCLUDE_TABLES => [
                'xcart_cc_pp3_data' =>                  ['S' => 'Y'],
                'xcart_customers' =>                    ['S' => 'Y'],
                'xcart_xcart_address_book' =>           ['S' => 'Y'],
                'xcart_discount_coupons' =>             ['S' => 'Y'],
                'xcart_download_keys' =>                ['S' => 'Y'],
                'xcart_giftcerts' =>                    ['S' => 'Y'],
                'xcart_ge_products' =>                  ['S' => 'Y'],
                'xcart_login_history' =>                ['S' => 'Y'],
                'xcart_newslist_subscription' =>        ['S' => 'Y'],
                'xcart_old_passwords' =>                ['S' => 'Y'],
                'xcart_order_details' =>                ['S' => 'Y'],
                'xcart_order_extras' =>                 ['S' => 'Y'],
                'xcart_orders' =>                       ['S' => 'Y'],
                'xcart_sessions_data' =>                ['S' => 'Y'],
                'xcart_sessions_data' =>                ['S' => 'Y'],
                'xcart_session_history' =>              ['S' => 'Y'],
                'xcart_session_unknown_sid' =>          ['S' => 'Y'],
                'xcart_stats_adaptive' =>               ['S' => 'Y'],
                'xcart_stats_cart_funnel' =>            ['S' => 'Y'],
                'xcart_stats_customers_products' =>     ['S' => 'Y'],
                'xcart_stats_pages' =>                  ['S' => 'Y'],
                'xcart_stats_pages_paths' =>            ['S' => 'Y'],
                'xcart_stats_pages_views' =>            ['S' => 'Y'],
                'xcart_stats_search' =>                 ['S' => 'Y'],
                'xcart_stats_shop' =>                   ['S' => 'Y'],
                'xcart_stop_list' =>                    ['S' => 'Y'],
                'xcart_subscription_customers' =>       ['S' => 'Y'],
                'xcart_subscriptions' =>                ['S' => 'Y'],
                'xcart_users_online' =>                 ['S' => 'Y'],
                'xcart_wishlist' =>                     ['S' => 'Y'],
                'xcart_wishlist' =>                     ['S' => 'Y'],
            ],
/*            self::PARAM_MODIFIERS => [
                'xcart_customers' => [
                    'email' => ['u-{firstname}@' . \XDev\Utils\Faker::FAKE_EMAIL_DOMAIN]
                ],
                'xcart_address_book' => [
                    'phone' => ['555555555']
                ]
            ],*/
        ];
    }

}
