<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\EngineBased\XC4\App\Config\Repo\v4_0_0;

/**
 * Class API
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Repo extends \XDev\Config\Repo
{
    protected function createGitignoreBuilder() {
// TODO detect admin dir thtough the API
        $builder = parent::createGitignoreBuilder();

        $builder
            ->addSection('xc4_allow', 'Allow X-Cart 4 files')
                ->addRule('!/admin/**')
                ->addRule('!/catalog/**')
                ->addRule('!/customer/**')
                ->addRule('!/include/**') 
                ->addRule('!/mail/**')
                ->addRule('!/modules/**')
                ->addRule('!/partner/**')
                ->addRule('!/payment/**')
                ->addRule('!/provider/**')
                ->addRule('!/shipping/**')
                ->addRule('!/skin1/**')
                ->addRule('!/*.php')
                ->addRule('!/favicon.ico')
                ->addRule('!/default_icon.gif')
                ->addRule('!/default_image.gif')
                ->addRule('!/default_logo.gif')
                ->addRule('!/fb_icon.gif')
                ->addRule('!/preview_image.gif')
                ->addRule('!/flash_container.swf')
                ->addRule('!/shop_closed.html')
                ->addRule('!/permission_denied.html')
                ->addRule('!/message.html')
                ->addRule('!/VERSION')
                ->end()
            ->addSection('xc4_ignore', 'Ignore X-Cart 4 files')
                ->addRule('error_log')
                ->addRule('/config.php')
                ->addRule('/config.local.php')
                ->addRule('/.htaccess')
                ->addRule('/admin/.htaccess')
                ->addRule('/provider/.htaccess')
                ->addRule('/partner/.htaccess')
        ;

        return $builder;

    }

    public function filterConfigFileContent($content)
    {
        $patcher = \XDev\EM::get('ConfigPatcher');
        $patcher->load($content);
        $patcher->applyPatchClearSecureData();

        return $patcher->getPatchedContent($content);
    }

    public function getPreservedFiles()
    {
        return [
            [
                'file' => 'config.php',
                'contentFilter' => 'filterConfigFileContent'
            ],
        ];
    }
}
