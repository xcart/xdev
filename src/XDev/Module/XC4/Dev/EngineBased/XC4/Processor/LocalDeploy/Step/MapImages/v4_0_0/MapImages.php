<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\Dev\EngineBased\XC4\Processor\LocalDeploy\Step\MapImages\v4_0_0;

use XDev\Base\Processor\AStep;

/**
 * Class CreateLocalConfig
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class MapImages extends AStep
{
    public function getTitle()
    {
        return 'Mapping images to LIVE instance';
    }

    protected function doMapImages($tbl, $id, $field, $base_url)
    {
        $conn = \XDev::getDB()->getConnection();

        if ($res = $conn->query("SELECT $id, $field FROM $tbl"))
        {
            while ($row = $res->fetch_assoc()) {
                if (preg_match('/^\.\//', $row[$field], $tmp)) {
                    $image_url = $base_url . ltrim($row[$field], '.');

                    $conn->query("UPDATE $tbl SET $field='$image_url' WHERE $id='$row[$id]'");
                }
            }
        }

        \XDev::getDB()->freeResult($res);
    }

    public function run()
    {
        $instanceLive = \XDev::getConfig('Main')
            ->getRemoteInstance(\XDev\Config\Main::INSTANCE_NAME_LIVE)
        ;

        $base_url = $instanceLive->getWebBaseURL($instanceLive->isSSLEnabled());

        foreach (\XDev::getDB()->getTables() as $tbl)
        {
            if (strpos($tbl, 'xcart_images_') === 0) {
                $this->doMapImages($tbl, 'imageid', 'image_path', $base_url);

            } elseif ($tbl == 'xcart_quick_flags') {
                $this->doMapImages($tbl, 'productid', 'image_path_T', $base_url);
            }
        }
    }
}
