<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\Dev\EngineBased\XC4\Processor\LocalDeploy\Step\PatchConfigFile\v4_5_5;

use \XDev\Module\XC4\Dev\EngineBased\XC4\Generator\ConfigFile\AConfigFile;

/**
 * Class CreateLocalConfig
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class PatchConfigFile extends \XDev\Module\XC4\Dev\EngineBased\XC4\Processor\LocalDeploy\Step\PatchConfigFile\v4_0_0\PatchConfigFile
{
    protected function applyPatch($patcher)
    {
        parent::applyPatch($patcher);

        $patcher->applyPatchDisableCheckConfigIntegrity();

    }
}
