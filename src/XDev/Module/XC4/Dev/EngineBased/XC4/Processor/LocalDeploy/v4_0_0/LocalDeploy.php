<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\Dev\EngineBased\XC4\Processor\LocalDeploy\v4_0_0;

use \XDev\Module\XC4\Dev\EngineBased\XC4\Generator\ConfigFile\AConfigFile;
use \XDev\EM;

/**
 * Class LocalDeploy
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class LocalDeploy extends \XDev\Dev\Processor\LocalDeploy
{
    const S_CREATE_LOCAL_CONFIG = 'create_local_config';
    const S_PATCH_CONFIG        = 'patch_config';
    const S_CREATE_USERS        = 'create_users';
    const S_MAP_IMAGES          = 'map_images';

    protected function defineSteps()
    {
        return array_merge(parent::defineSteps(), [
            self::S_CREATE_LOCAL_CONFIG => 'Processor\LocalDeploy\Step\CreateLocalConfig',
            self::S_PATCH_CONFIG        => 'Processor\LocalDeploy\Step\PatchConfigFile',
            self::S_CREATE_USERS        => 'Processor\LocalDeploy\Step\CreateDemoUsers',
            self::S_MAP_IMAGES          => 'Processor\LocalDeploy\Step\MapImages',
        ]);
    }

    protected function finalize()
    {
        $this->getOutput()->writeln(sprintf('<fg=green;options=bold>Store succesfully deployed to: </><fg=yellow;options=bold>%s</>',$this->getDeployDir()));
        $this->getOutput()->writeln('');

        EM::clear('Config');

        $this->getOutput()->writeln(sprintf('<fg=green;options=bold>Storefront: </><fg=yellow;options=bold>%s</>', EM::get('API')->getSoftwareStorefrontUrl()));
        $this->getOutput()->writeln(sprintf('<fg=green;options=bold>Admin Zone: </><fg=yellow;options=bold>%s</>', EM::get('API')->getSoftwareAdminZoneUrl()));
        $this->getOutput()->writeln('');
    }
}
