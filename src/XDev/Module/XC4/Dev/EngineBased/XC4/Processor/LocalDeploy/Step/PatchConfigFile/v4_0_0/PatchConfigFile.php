<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\Dev\EngineBased\XC4\Processor\LocalDeploy\Step\PatchConfigFile\v4_0_0;

use XDev\Base\Processor\AStep;
use XDev\Module\XC4\Dev\EngineBased\XC4\Generator\ConfigFile\AConfigFile;
use XDev\EM;

/**
 * Class CreateLocalConfig
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class PatchConfigFile extends AStep
{
    public function getTitle()
    {
        return 'Patching config file';
    }

    public function isHidden()
    {
        return true;
    }

    protected function applyPatch($patcher)
    {
        $patcher->applyPatchGenerateSecureData(\XDev::getConfig('Main')->getBlowfishkey());
    }

    public function run()
    {
        $patcher = EM::get('ConfigPatcher');
        $patcher->loadFromFile(EM::get('Config')->getConfigFilename());

        $this->applyPatch($patcher);

        return $patcher->savePatchedContent(EM::get('Config')->getConfigFilename());
    }
}
