<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\Dev\EngineBased\XC4\App\Command\CreateDemoUsers\v4_0_0;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Exception\RuntimeException;

use XDev\Module\XC4\Dev\EngineBased\XC4\NativeAPI\API\Users\Create\v4_0_0\Create;

/**
 * Test command
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class CreateDemoUsers extends \XDev\Dev\NativeAPI\ANativeAPICommand
{
    const OPTION_VALUE_RANDOM_PASS = 'RANDOM';
    const OPTION_VALUE_XDEV_BLOWFISH_BASED = 'XDEV_BLOWFISH_BASED';

    public function isInitRequired() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('users:create-demo')
            ->setDescription('Creates test users')
            ->addOption(
                'alogin',
                '-L',
                InputOption::VALUE_OPTIONAL,
                'Admin login',
                'bit-bucket@x-cart.com'
            )
            ->addOption(
                'apass',
                '-P',
                InputOption::VALUE_OPTIONAL,
                'Admin password',
                self::OPTION_VALUE_XDEV_BLOWFISH_BASED
            )
            ->addOption(
                'login',
                '-l',
                InputOption::VALUE_OPTIONAL,
                'Customer login',
                'bit-bucket@cdev.ru'
            )
            ->addOption(
                'pass',
                '-p',
                InputOption::VALUE_OPTIONAL,
                'Customer\'s password',
                'test123'
            )
        ;
    }

    protected function createAdminProfile(InputInterface $input, OutputInterface $output)
    {
        $admin_login = $input->getOption('alogin');
        $admin_password = $input->getOption('apass');

        if ($admin_password === self::OPTION_VALUE_RANDOM_PASS) {
            $admin_password = substr(md5(time()),0, 6);

        } elseif ($admin_password === self::OPTION_VALUE_XDEV_BLOWFISH_BASED) {
            $admin_password = substr(md5(\XDev::getConfig('Main')->getBlowfishKey()), 0, 6);
        }

        $result = $this->call('users:create', [
           'usertype' => Create::TYPE_ADMIN,
           'username' => $admin_login,
           'login' => $admin_login,
           'firstname' => 'X-Tester',
           'lastname' => 'Admin',
           'email' => $admin_login,
           'password' => $admin_password,
           'status' => Create::STATUS_ENABLED,
           'b_title' => 'Mr.',
           'b_firstname' => 'X-Tester',
           'b_lastname' => 'Admin',
           'b_address' => '2687 Drainer Avenue',
           'b_city' => 'New York',
           'b_state' => 'NY',
           'b_country' => 'US',
           'b_zipcode' => '10001',
           'b_zip4' => '',
           'b_city' => 'Alford',
           'b_phone' => '850-579-5476',
           'b_fax' => '850-579-5476',
           'phone' => '850-579-5476',
           'fax' => '850-579-5476',
           'ship2diff' => false,
        ], $output);

        return $result;

    }

    protected function createCustomerProfile(InputInterface $input, OutputInterface $output)
    {
        $customer_login = $input->getOption('login');
        $customer_password = $input->getOption('pass');

        $result = $this->call('users:create', [
           'usertype' => Create::TYPE_CUSTOMER,
           'username' => $customer_login,
           'login' => $customer_login,
           'firstname' => 'X-Tester',
           'lastname' => 'Customer',
           'email' => $customer_login,
           'password' => $customer_password,
           'status' => Create::STATUS_ENABLED,
           'b_title' => 'Mr.',
           'b_firstname' => 'X-Tester',
           'b_lastname' => 'Customer',
           'b_address' => '2687 Drainer Avenue',
           'b_city' => 'New York',
           'b_state' => 'NY',
           'b_country' => 'US',
           'b_zipcode' => '10001',
           'b_zip4' => '',
           'b_city' => 'Alford',
           'b_phone' => '850-579-5476',
           'b_fax' => '850-579-5476',
           'phone' => '850-579-5476',
           'fax' => '850-579-5476',
           'ship2diff' => false,
        ], $output);

        return $result;

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $result = $this->createAdminProfile($input, $output);

        if (isset($result['result']['user'])) {
            $output->writeln(sprintf(
                '<fg=green;options=bold>Created admin profile:</><fg=yellow;options=bold> %s / %s</>',
                $result['result']['user']['login'],
                $result['result']['user']['password']
             ));
        }

        $result = $this->createCustomerProfile($input, $output);

        if (isset($result['result']['user'])) {
            $output->writeln(sprintf(
                '<fg=green;options=bold>Created customer profile:</><fg=yellow;options=bold> %s / %s</>',
                $result['result']['user']['login'],
                $result['result']['user']['password']
             ));
        }

        $output->writeln('');
    }
}
