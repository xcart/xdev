<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\Dev\EngineBased\XC4\NativeAPI\API\Users\Create\v4_6_2;

use XDev\Core\JsonAPI;

/**
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Create extends \XDev\Module\XC4\Dev\EngineBased\XC4\NativeAPI\API\Users\Create\v4_5_5\Create
{
    protected function getCryptedPassword()
    {
        return text_crypt(text_hash(stripslashes($this->params['password'])));
    }

    protected function getDecryptedPasswordForDisplay($password) {
        $password = parent::getDecryptedPasswordForDisplay($password);

        return substr($password, 0, 3) . '..' . substr($password, strlen($password) - 3, 3);
    }
}
