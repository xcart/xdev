<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\Dev\EngineBased\XC4\NativeAPI\API\Users\Create\v4_0_0;

use XDev\Core\JsonAPI;

/**
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Create
{
    const TYPE_ADMIN        = 'A';
    const TYPE_CUSTOMER     = 'C';
    const STATUS_ENABLED    = 'Y';
    const STATUS_DISABLED   = 'N';

    protected $isXCProEdition = null;

    protected $createdUserId;
    protected $createdUsertype;

    protected $params;

    public function process($params = [])
    {
        x_load('user', 'crypt');

        $this->params = $params;

        $this->checkIfUserExists();

        $this->createUser();

        JsonAPI::setResult([
            'user' => [
                'id' => $this->getCreatedUserId(),
                'login' => $this->params['login'],
                'password' => $this->params['password'],
            ]
        ]);

    }

    protected function checkIfUserExists()
    {
        global $sql_tbl;

        $user = func_query_first("SELECT login, password FROM $sql_tbl[customers] WHERE login='" . addslashes($this->params['login']) . "'");

        if ($user) {
            $password = $this->getDecryptedPasswordForDisplay($user['password']);

            JsonAPI::addWarning("User $user[login] / $password already exists!");

            exit;
        }

    }

    protected function getCreatedUserId() {
        return $this->createdUserId;
    }

    protected function createUser() {
        $this->checkUserParams();

        $data = $this->getUserData();

        $this->createdUserId = func_array2insert('customers', array_map('addslashes', $data));

        $this->createdUsertype = $data['usertype'];

    }

    protected function isXCProEdition() {
        global $sql_tbl;

        if ($this->isXCProEdition === null) {
            $this->isXCProEdition = func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[modules] WHERE module_name = 'Simple_Mode'") ? true : false;
        }

        return $this->isXCProEdition;
    }

    protected function getUserRequiredParams() {
        return [
            'login',
            'usertype',
            'email',
            'firstname',
            'lastname',
            'password'
        ];
    }

    protected function checkUserParams() {
        $has_errors = false;

        foreach ($this->getUserRequiredParams() as $param) {
            if (!isset($this->params[$param])) {
                JsonAPI::addError(sprintf('Param \'%s\' is required', $param));
                $has_errors = true;
            }
        }

        if ($has_errors) {
            exit;
        }

        foreach ($this->params as $param => $param_value) {
            $method_name = 'checkUserParam' . ucfirst($param);

            if (method_exists($this, $method_name)) {
                $result = $this->{$method_name}($param_value);

                if (!$result) {
                    JsonAPI::addError(sprintf('Invalid param value \'%s\' for %s', $param_value, $param));

                    exit;
                }
            }
        }
    }

    protected function checkUserParamUsertype($value) {
        return in_array($value, ['A', 'P', 'C']);
    }

    protected function prepareUserParamUsertype($value) {
        if ($value == 'A' && !$this->isXCProEdition()) {
            $value = 'P';

        }
        return $value;
    }

    protected function prepareUserParamStatus($value) {
        return $value === 'Y' ? 'Y' : 'N';
    }

    protected function getCryptedPassword() {
        return text_crypt($this->params['password']);
    }

    protected function getDecryptedPasswordForDisplay($password) {
        return text_decrypt($password);
    }

    protected function getUserDataFields() {
        return [
            'login',
            'usertype',
            'title',
            'firstname',
            'lastname',
            'company',
            'email',
            'status',
            'password',
            'b_title',
            'b_firstname',
            'b_lastname',
            'b_address',
            'b_city' ,
            'b_county',
            'b_state',
            'b_country',
            'b_zipcode',
            's_title',
            's_firstname',
            's_lastname',
            's_address',
            's_city' ,
            's_county',
            's_state',
            's_country',
            's_zipcode',
            'phone',
            'fax',
        ];
    }

    protected function getUserData()
    {

        $result = [];

        foreach ($this->getUserDataFields() as $field) {
            if (isset($this->params[$field])) {
                $result[$field] = $this->params[$field];

                $method_name = 'prepareUserParam' . ucfirst($field);

                if (method_exists($this, $method_name)) {
                    $result[$field] = $this->{$method_name}($result[$field]);
                }
            }

        }

        $result['password'] = $this->getCryptedPassword();

        return $result;

    }


}
