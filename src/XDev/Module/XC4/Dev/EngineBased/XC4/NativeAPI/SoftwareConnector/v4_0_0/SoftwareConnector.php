<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\Dev\EngineBased\XC4\NativeAPI\SoftwareConnector\v4_0_0;
 
/**
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class SoftwareConnector implements \XDev\Dev\NativeAPI\ISoftwareConnector
{
    public function getConnectorCode()
    {
        $dir = \XDev::getSoftwareDir();

        return <<<EOT
require '$dir/auth.php';
EOT;

    }
}
