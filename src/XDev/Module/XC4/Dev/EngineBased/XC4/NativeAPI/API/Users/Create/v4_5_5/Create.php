<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\Dev\EngineBased\XC4\NativeAPI\API\Users\Create\v4_5_5;

use XDev\Core\JsonAPI;

/**
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Create extends \XDev\Module\XC4\Dev\EngineBased\XC4\NativeAPI\API\Users\Create\v4_4_0\Create
{
    protected function getCryptedPassword()
    {
        return text_crypt(text_hash($this->params['password']));
    }

    protected function createUser() {

        parent::createUser();

        $this->updateUserSignature();
    }

    protected function updateUserSignature() {
        global $active_modules, $current_area;

        require_once \XDev::getSoftwareDir() . \XDev::DS . 'include' . \XDev::DS . 'classes' . \XDev::DS . 'class.XCSignature.php';

        $fields_area = !empty($active_modules['Simple_Mode']) && $current_area == 'A'
        ? 'P'
        : $current_area;

        $newuser_info = func_userinfo($this->createdUserId, $this->createdUsertype, false, NULL, $fields_area, false);

        $obj_user = new \XCUserSignature($newuser_info);
        $obj_user->updateSignature();

    }
}
