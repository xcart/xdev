<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC5\EngineBased\XC5\App\Config\Repo\v5_0_0;

/**
 * Class API
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Repo extends \XDev\Config\Repo
{
    protected function createGitignoreBuilder() {
// TODO: detect admin.php script thtough the API
        $builder = parent::createGitignoreBuilder();

        $builder
            ->addSection('xc5_allow', 'Allow X-Cart 5 files')
                ->addRule('!/classes/**')
                ->addRule('!/etc/**')
                ->addRule('!/Includes/**')
                ->addRule('!/lib/**')
                ->addRule('!/public/**') 
                ->addRule('!/skins/**')
                ->addRule('!/*.php')
                ->addRule('!/favicon.ico')
                ->addRule('!/LICENSE.txt')
                ->end()
            ->addSection('xc5_ignore', 'Ignore X-Cart 5 files')
                ->addRule('/etc/config.php')
                ->addRule('/etc/config.local.php')
                ->addRule('/.htaccess')
                ->addRule('/install.*.php')
        ;

        return $builder;

    }

    public function filterConfigFileContent($content)
    {
        $patcher = \XDev\EM::get('ConfigPatcher');
        $patcher->load($content);
        $patcher->applyPatchClearSecureData();

        return $patcher->getPatchedContent($content);;
    }

    public function getPreservedFiles()
    {
        return [
            [
                'file' => 'etc/config.php',
                'contentFilter' => 'filterConfigFileContent'
            ],
        ];
    }
}
