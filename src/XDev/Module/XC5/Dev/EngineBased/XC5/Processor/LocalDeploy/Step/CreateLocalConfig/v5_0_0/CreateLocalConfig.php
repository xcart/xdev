<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC5\Dev\EngineBased\XC5\Processor\LocalDeploy\Step\CreateLocalConfig\v5_0_0;

use XDev\Base\Processor\AStep;
use XDev\Module\XC5\Dev\EngineBased\XC5\Generator\LocalConfigFile\ALocalConfigFile;
use XDev\EM;

/**
 * Class CreateLocalConfig
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class CreateLocalConfig extends AStep
{
    public function getTitle()
    {
        return 'Creating local config file';
    }

    public function run()
    {
        $options = [
            ALocalConfigFile::PARAM_HTTP_HOST   => $this->getHttpHost(),
            ALocalConfigFile::PARAM_WEB_DIR     => $this->getWebDir(),
            ALocalConfigFile::PARAM_DB_HOST     => $this->getDbHost(),
            ALocalConfigFile::PARAM_DB_USER     => $this->getDbUser(),
            ALocalConfigFile::PARAM_DB_PASSWORD => $this->getDbPass(),
            ALocalConfigFile::PARAM_DB_NAME     => $this->getDbName(),
            ALocalConfigFile::PARAM_DB_PORT     => $this->getDbPort(),
            ALocalConfigFile::PARAM_DB_SOCKET   => $this->getDbSocket(),
        ];

        EM::get('Generator\LocalConfigFile', $options)->generateFile();
    }
}
