<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC5\Dev\EngineBased\XC5\Generator\LocalConfigFile;

/**
 * Class AConfigFile
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
abstract class ALocalConfigFile
{
    const PARAM_HTTP_HOST   = 'http_host';
    const PARAM_DB_HOST     = 'db_host';
    const PARAM_DB_NAME     = 'db_name';
    const PARAM_DB_USER     = 'db_user';
    const PARAM_DB_PASSWORD = 'db_password';
    const PARAM_DB_SOCKET   = 'db_socket';
    const PARAM_DB_PORT     = 'db_port';
    const PARAM_WEB_DIR     = 'web_dir';

    protected $options;

    public function __construct($options)
    {
        $this->options = $options;
    }

    protected function getOption($key)
    {
        return $this->options[$key];
    }
}
