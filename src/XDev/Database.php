<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev;

use Symfony\Component\Console\Exception\RuntimeException;
use XDev\Utils\Filesystem;
use Ifsnop\Mysqldump as IMysqldump;
use XDev\Config\DbDump as DbDumpConfig;

/**
 * Class Database
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Database extends \XDev\Core\Singleton
{

    protected function doMysqldumpDump($filename, $compress, $full = false)
    {
        $dbConfig = \XDev::getConfig('db');

        $mysql_str = sprintf('mysqldump -u%s -p%s -h%s %s %s %s',
            $dbConfig->getUser(),
            escapeshellcmd($dbConfig->getPassword()),
            $dbConfig->getHost(),
            $dbConfig->getPort() ? ('-P' . $dbConfig->getPort()) : '',
            $dbConfig->getSocket() ? ('--protocol=socket -S ' . $dbConfig->getSocket()) : '',
            $dbConfig->getDatabase()
        );

        $excluded_tables = \XDev::getConfig('dbDump')->getParam(DbDumpConfig::PARAM_EXCLUDE_TABLES);

        $tables_scheme_only = $tables_data_only = [];

        foreach ($this->getTables() as $k => $table) {

            if (isset($excluded_tables[$table])) {
                if (isset($excluded_tables[$table]['S']) && $excluded_tables[$table]['S'] == 'Y') {
                    $tables_scheme_only[] = $table;
                }

            } else {
                $tables_scheme_only[] = $table;
                $tables_data_only[] = $table;
            }

        }

        $cmd = [];

        $cmd[] = $mysql_str . implode(' ', $tables_scheme_only). ' --no-data';
        $cmd[] = $mysql_str . implode(' ', $tables_data_only). '  --no-create-info';

        if ($compress == \XDev\Config\DbDump::COMPRESS_GZIP) {
            $cmp = ' | gzip';

        } elseif ($compress == \XDev\Config\DbDump::COMPRESS_BZIP) {
            $cmp = ' | bzip2 -z';
        }

        $command = sprintf('(%s)%s > %s', implode(' && ', $cmd), $cmp, $filename);

        $sqlDir = \XDev\Config::getSQLDir();

        $process = \XDev\Utils\Shell::exec($command);

    }

    protected function transformColumnValueHook($table, $col, $value, $row)
    {
        $modifiers = \XDev::getConfig('dbDump')->getParam('modifiers');

        if (isset($modifiers[$table][$col])) {

            $result = $value;

            if (isset($modifiers[$table][$col][1]) && $modifiers[$table][$col][1]) {
                // Row-based condition

                parse_str($modifiers[$table][$col][1], $tmp);

                $row_matched = true;

                foreach ($tmp as $key => $value) {
                    if (isset($row[$key]) && $row[$key] != $value) {
                        $row_matched = false;
                        break;
                    }
                }

                if (!$row_matched) {
                    return $result;
                }
            }

            $has_variables = preg_match_all('/\{(\w+)\}/', $modifiers[$table][$col][0], $m);

            if ($has_variables) {
                foreach ($m[1] as $k => $column) {

                    if (isset($row[$column])) {
                        $result = str_replace($m[0][$k], $row[$column], $modifiers[$table][$col][0]);
                    }
                }

            } else {
                $result = $modifiers[$table][$col][0];
            }

            return $result;
        }

        return $value;
    }

    protected function doPdoDump($filename, $compress, $full = false)
    {
        $dbCnf = \XDev::getConfig('db');

        if ($compress == \XDev\Config\DbDump::COMPRESS_GZIP) {
            $compress = IMysqldump\Mysqldump::GZIP;

        } elseif ($compress == \XDev\Config\DbDump::COMPRESS_BZIP) {
            $compress = IMysqldump\Mysqldump::BZIP2;

        } else {
            $compress = IMysqldump\Mysqldump::NONE;
        }

        $tables_no_data = $tables_exclude = [];

        if (!$full) {
            $excluded_tables = \XDev::getConfig('dbDump')->getParam(DbDumpConfig::PARAM_EXCLUDE_TABLES);

            foreach ($this->getTables() as $k => $table) {

                if (isset($excluded_tables[$table])) {
                    if (isset($excluded_tables[$table]['S']) && $excluded_tables[$table]['S'] == 'Y') {
                        $tables_no_data[] = $table;
                    } else {
                        $tables_exclude[] = $table;
                    }
                } else {
                    $tables_with_data[] = $table;
                }

            }
        }

        $dumpSettings = [
            'exclude-tables' => $tables_exclude,
            'compress' => $compress,
            'no-data' => $tables_no_data,
            'no-create-info' => false,
        ];

        try {
            $dsn = "mysql:host={$dbCnf->getHost()};dbname={$dbCnf->getDatabase()}";

            if ($dbCnf->getPort()) {
                $dsn .= ';port=' . $dbCnf->getPort();
            }

            if ($dbCnf->getSocket()) {
                $dsn .= ';unix_socket=' . $dbCnf->getSocket();
            }

            $dumper = new IMysqldump\Mysqldump(
                $dsn,
                $dbCnf->getUser(),
                $dbCnf->getPassword(),
                $dumpSettings
            );

            $dumper->setTransformColumnValueHook(function ($table_name, $col_name, $value, $row) {
                return $this->transformColumnValueHook($table_name, $col_name, $value, $row);
            });

            $dumper->start($filename);

        } catch (\Exception $e) {
            throw new RuntimeException(
                'mysqldump-php error: ' . $e->getMessage()
            );

        }
    }

    protected function getTables() 
    {
        $db = \XDev::getDb();

        $database = \XDev::getConfig('db')->getParam('database');

        $query = (
            'SELECT TABLE_NAME'
            . ' FROM information_schema.TABLES'
            . " WHERE table_schema = '$database'"
        );

        $tables = $db->queryColumnn($query);

        return $tables;

    }
    /**
     * {@inheritDoc}
     */
    public function doDump($dumpFilename, $compression = null, $full = false)
    {

        $dir = dirname($dumpFilename);

        if (!Filesystem::exists($dir)) {

            Filesystem::mkdir($dir, 0700);
        }

        $dumpCfg = \XDev::getConfig('dbDump');

        $provider_name = $dumpCfg->getParam('dumpProvider');

        $method_name = 'do' . ucfirst($provider_name) . 'Dump';

        if (method_exists($this, $method_name)) {
            if ($compression === null) {
                $compression = $dumpCfg->getParam(DbDumpConfig::PARAM_COMPRESSION);
            }

            $this->$method_name($dumpFilename, $compression, $full);

        } else {
            throw new RuntimeException(
                'MySQL dump provider does not exist: ' . $provider_name . "\n"
                . 'Please specify a correct provider in ' . DbDumpConfig::getConfigFilename()
            );
        }

    }

}
