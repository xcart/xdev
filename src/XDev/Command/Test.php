<?php

namespace XDev\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use XDev\Utils\Git;
use Symfony\Component\Filesystem\Filesystem;
/**
 * Create branch command.
 */
class Test extends Command
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('test')
            ->setDescription('')
        ;
    }
    protected function test1(InputInterface $input, OutputInterface $output) {
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->test1();
    }
}
