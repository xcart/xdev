<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use XDev\Core\JsonAPI;

/**
 * Database dump command
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class DbDump extends \XDev\Base\Command\ASoftwareRelative
{

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('dd:dump')
            ->setDescription('Creates a database dump')
            ->addOption(
                'compress',
                '-c',
                InputOption::VALUE_OPTIONAL,
                'Compression method (bzip, gzip)',
                null
            )
        ;
    }

    protected function isJsonOutputEnabled()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($output->isVerbose()) {
            \XDev\Utils\Shell::setDebugMode(true);
        }

        $output->writeln('Creating a database dump...');

        $filename = \XDev::getConfig('DbDump')->getDumpFilename($input->getOption('compress'));

        \XDev\Database::getInstance()->doDump($filename, $input->getOption('compress'));

        $output->writeln('');
        $output->writeln('<fg=yellow;options=bold>Dump succesfully created: </><fg=green;>' . $filename . '</>');

        JsonAPI::setResult([
            'dumpFilename' => $filename,
            'dumpFilesize' => filesize($filename),
        ]);

    }
}
