<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Command;

use XDev\Command\ACommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use XDev\Utils\Git;
use XDev\Config\Main as MainConfig;
use XDev\Core\Instance;
use XDev\EM;

/**
 * Init command
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Init extends \XDev\Base\Command\ASoftwareRelative
{
    public function isInitRequired() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('init')
            ->addArgument('repoUrl', InputArgument::OPTIONAL, 'Repository URL')
            ->setDescription('Initializes X-Cart dev tools')
            ->addOption(
                'ver',
                null,
                InputOption::VALUE_OPTIONAL,
                'Software version',
                false
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $version = \XDev::getSoftwareVersion();

        if (\XDev::isInitialized()) {
            throw new RuntimeException('X-Dev Tools have been already initialized');
        }

        if (!$version) {

            if ($input->getOption('version') === false) {
                throw new RuntimeException('Cannot detect software version. Please specify --version option');
            }

            if (!$input->getOption('version')) {
                throw new RuntimeException(sprintf('Invalid version \'%s', $input->getOption('version')));
            }

            $version = $input->getOption('version');
        }

        if ($input->getOption('version') !== false && $version != $input->getOption('version')) {
            $output->writeln(sprintf('Warning! The provided version %s does not match the detected one: %s', $input->getOption('version'), $version));
        }

        $repo_url = $input->getArgument('repoUrl');

        if (!$repo_url) {
            $helper = $this->getHelper('question');
            $question = new Question('<fg=yellow;options=bold>Please enter the repository URL (or just hit ENTER to skip): </>');

            $repo_url = $helper->ask($input, $output, $question);

            if (!$repo_url) {
                $question = new ConfirmationQuestion('<fg=yellow;options=bold>Continue with no repository? (yes/no): </>', false,  '/^(y)/i');

                if (!$helper->ask($input, $output, $question)) {
                    $output->writeln('Operation canceled');
                    $output->writeln('');

                    return;
                }

                $output->writeln('Warning! No repository URL specified, some commands will be unavailable.');
                $output->writeln('');
            }
        }

        \XDev::getConfig('Local')->setCurrentInstance(\XDev\Config\Main::INSTANCE_NAME_LIVE);

        if ($repo_url) {
            $parsed_url = Git::parseRepoUrl($repo_url);
            $repo_url = $parsed_url['ssh_url'];

            \XDev\Utils\Git::testRepoUrl($repo_url);
            \XDev\Repo::getInstance()->init($repo_url);

            \XDev::getConfig('Main')
                ->setEngineCode(\XDev::getSoftwareEngineCode())
                ->setSoftwareVersion(\XDev::getSoftwareVersion())
                ->setRepositoryURL($repo_url)
            ;

            \XDev::getConfig('Main')->setRemoteInstanceParams(MainConfig::INSTANCE_NAME_LIVE, [
                Instance::PARAM_SOFTWARE_DIR    => \XDev::getSoftwareDir(),
                Instance::PARAM_HTTP_HOST       => EM::get('API')->getSoftwareHttpHost(),
                Instance::PARAM_HTTPS_HOST      => EM::get('API')->getSoftwareHttpsHost(),
                Instance::PARAM_SSL_ENABLED     => EM::get('API')->testHttps(),
                Instance::PARAM_WEB_DIR         => EM::get('API')->getSoftwareWebDir(),
            ]);

        }

        \XDev::getConfig('Local')->save();
        \XDev::getConfig('Main')->save();
        \XDev::getConfig('Db')->save();
        \XDev::getConfig('DbDump')->save();

        $output->writeln(sprintf('<fg=green;options=bold>X-Dev tools have been initialized in: <fg=yellow;options=bold>%s</></>', \XDev\Config::getXDevDir()));
        $output->writeln('');
    }
}
