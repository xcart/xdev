<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Helper\Table;

/**
 * Class DbDumpReset
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class DbDumpReset extends \XDev\Base\Command\ASoftwareRelative
{

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('dd:reset')
            ->setDescription('Excludes certain tabel from the SQL dump')
            ->addArgument('table', InputArgument::OPTIONAL, 'MySQL table name')
            ->addOption(
                'exclude-schema',
                '-s',
                InputOption::VALUE_OPTIONAL,
                'Also exclude table schema from the SQL dump',
                false
            )
            ->addOption(
                'all',
                '-a',
                InputOption::VALUE_OPTIONAL,
                'Remove all tables from dumpp',
                false
            );

        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $table_name = $input->getArgument('table');

        $dumpCnf = \XDev::getConfig('DbDump');

        if ($table_name !== null) {

            $dumpCnf->resetTable($table_name, $input->getOption('exclude-schema') !== false);
            $dumpCnf->save();

        } elseif ($table_name === null && $input->getOption('all') !== false) {

            $db = \XDev::getDB();

            foreach ($db->getTables() as $table_name) {
                $dumpCnf->resetTable($table_name, $input->getOption('exclude-schema') !== false);
            }

            $dumpCnf->save();
        }

    }
}
