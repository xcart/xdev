<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

use Symfony\Component\Console\Command\Command;
use XDev\Utils\Filesystem;

/**
 * Build command 
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class AppVersion extends Command {

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:version')
            ->setDescription('Application version')
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(\XDev::VERSION);

    }

}