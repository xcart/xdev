<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Helper\TableStyle;

/**
 * Class FilesDumpStatus
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class FilesDumpStatus extends Command
{
    const COMPRESS_GZIP = 'gzip';
    const COMPRESS_BZIP = 'bzip';
    const COMPRESS_NONE = 'none';

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('fd:status')
            ->setDescription('Displays files dump settings')
            ->addOption(
                'compress',
                '-c',
                InputOption::VALUE_OPTIONAL,
                'Compression method (bzip, gzip)',
                self::COMPRESS_GZIP
            )
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

      /*  $finder = new Finder();

        $finder->ignoreVCS(false);

        $finder
            ->depth('== 1')
            ->directories()
            ->ignoreDotFiles(false)
            ->in(\XDev::getSoftwareDir())
        ;*/

        $rows = [];

        $o = \XDev\Utils\Shell::exec('du -hb --max-depth=1 ' . \XDev::getSoftwareDir(), true);

        $raw = explode("\n", $o);

        foreach ($raw as $v) {
            $info = explode("\t", $v);

            if (!($filename_full = @$info[1]) || $filename_full == \XDev::getSoftwareDir()) {
                continue;
            }

            $filename = basename($filename_full);
            $size = $info[0];
            $rows[] = [$filename, round($size / 1024 / 1024, 2)];
        }

        usort($rows, function($a, $b){
            return $a[0] > $b[0];
        });

        $output->writeln('');
        $output->writeln('Dump files:');

        $table = new Table($output);
        $style = new TableStyle();
        $style->setPadType(STR_PAD_BOTH);

        $table
            ->setHeaders(['File', 'Size (Mb)'])
            ->setRows($rows)
        ;

        $table->render();

        $output->writeln('');
    }
}
