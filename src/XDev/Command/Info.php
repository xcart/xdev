<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

/**
 * Class Info
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Info extends \XDev\Base\Command\ASoftwareRelative
{

    public function isInitRequired() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('info')
            ->setDescription('Displays the system summary')
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $table = new Table($output);
        $table->setStyle('compact');
        $rows = [
            ['Server', ''],
            ['<fg=green;options=bold>Software:</>', sprintf('<comment>%s</comment>', \XDev::getSoftwareName())],
            ['<fg=green;options=bold>PHP:</>', sprintf('<comment>%s</comment>', phpversion())],
            ['<fg=green;options=bold>Git:</>', sprintf('<comment>%s</comment>', \XDev\Utils\Git::getGitVersion())],
        ];

        if (\XDev::isInitialized()) {
            $rows = array_merge($rows, [
                ['', ''],
                ['X-Dev', ''],
                ['<fg=green;options=bold>Instance:</>', sprintf('<fg=magenta;options=bold>%s</>', \XDev::getCurrentInstance())],
                ['<fg=green;options=bold>Repository:</>', sprintf('<comment>%s</comment>', \XDev\Utils\Git::getRepoUrl())],
                ['',''],
                ['Instances', ''],
            ]);

            foreach (\XDev::getConfig('Main')->getRemoteInstances() as $instance) {
                $rows[] = [sprintf('<fg=green;options=bold>%s:</>', $instance->getName()), sprintf('<comment>%s</comment>', $instance->getWebBaseURL())];
            }

        } else {
            $rows = array_merge($rows, [
                ['<fg=green;options=bold>X-DEV:</>', sprintf('<fg=red;options=bold>Not initialized</>', \XDev::getCurrentInstance())],
            ]);
        }

        $table
            ->setRows($rows)
        ;

        $table->render();

        $output->writeln('');
    }
}
