<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Exception\RuntimeException;
use XDev\Utils\Git;

/**
 * Class PushUpdate
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class PushUpdate extends \XDev\Base\Command\ASoftwareRelative
{

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('push-update')
            ->setDescription('Updates the repository')
            ->addOption(
                'db-dump',
                '-b',
                InputOption::VALUE_OPTIONAL,
                'Include database dump into the repository',
                false
            )
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $currentBranch = Git::getCurrentBranch();

        $initialCommitExists = Git::isInitialCommitExists();

        if ($initialCommitExists && !Git::checkCurrentBranchIsUpToDate()) {
            throw new RuntimeException(
                'Warning! Remote branch \'' . $currentBranch . '\' is ahead of local branch \'' . $currentBranch . '\'' . "\n"
                . 'Please first pull remote changes to the local branch ' . '\'' . $currentBranch . '\'' . "\n\n"
                . Git::getRemoteBranchAheadCommits()
            );
        }

        \XDev\Repo::getInstance()->backupPreservedFiles();

        if ($input->getOption('db-dump') !== false) {
            $output->writeln('Creating database dump...');
            \XDev\Database::getInstance()->doDump($dumpCfg->getDumpFilename());
        }

        $output->writeln('Adding files...');
        \XDev\Utils\Shell::exec('git add -A');

        if (\XDev::getConfig('dbDump')->isGitTrack() || $input->getOption('db-dump') !== false) {
            \XDev\Utils\Shell::exec('git add -f ' . \XDev::getConfig('dbDump')->getDumpFilename());
        }

        try {
            \XDev\Utils\Shell::exec('git diff --cached --name-only --exit-code');

            // Commit is not needed

            $output->writeln('Nothing to commit');
            $output->writeln('');

            return true;

        } catch (\Exception $e) {
            
        }

        if ($initialCommitExists) {
            $message = "[XDEV] sync with " . \XDev::getCurrentInstance();
        } else {
            $message = "Initial commit";
        }

        $output->writeln('Pushing changes...');

        if (\XDev\Utils\Git::getGitVersion() >= '2.10') {
            $progress_option = ' --progress';
        }

        \XDev\Utils\Shell::setTimeout(300);
        \XDev\Utils\Shell::exec('git commit -m "' . $message . '" --author="XDEV <xdev@x-cart.com>"');

        \XDev\Utils\Shell::setTranslateOutput(true);

        \XDev\Utils\Shell::exec("git push $progress_option -u origin " . $currentBranch);
    }
}
