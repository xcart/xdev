<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class DbDumpHookAdd
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class DbDumpHookAdd extends\XDev\Base\Command\ASoftwareRelative
{

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('dd:hook-add')
            ->setDescription('Adds data on-the-fly modifiers for SQL dump')
            ->addArgument('table', InputArgument::REQUIRED, 'MySQL table name')
            ->addArgument('column', InputArgument::REQUIRED, 'MySQL colunmn name')
            ->addArgument('modifier', InputArgument::REQUIRED, 'Data modifier')
            ->addArgument('condition', InputArgument::OPTIONAL, 'Row condition')
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $dumpCnf = \XDev::getConfig('DbDump');

        $table = $input->getArgument('table');
        $column = $input->getArgument('column');
        $modifier = $input->getArgument('modifier');
        $condition = $input->getArgument('condition');

        $dumpCnf->addModifier($table, $column, $modifier, $condition);

        $dumpCnf->save();

    }
}
