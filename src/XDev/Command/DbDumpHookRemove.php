<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class DbDumpHookRemove
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class DbDumpHookRemove extends \XDev\Base\Command\ASoftwareRelative
{

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('dd:hook-rm')
            ->setDescription('Removes hook for SQL dump')
            ->addArgument('table', InputArgument::REQUIRED, 'MySQL table name')
            ->addArgument('column', InputArgument::REQUIRED, 'MySQL colunmn name')
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $dumpCnf = \XDev::getConfig('dbDump');

        $table = $input->getArgument('table');
        $column = $input->getArgument('column');

        $dumpCnf->removeModifier($table, $column);

        $dumpCnf->save();

    }
}
