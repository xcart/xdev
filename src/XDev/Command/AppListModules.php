<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;
use XDev\Core\Symfony\Console\Helper as ConsoleHelper;

/**
 * List modules command
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class AppListModules extends \XDev\Base\Command\ACommand {

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:modules')
            ->setDescription('Lists installed modules')
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $modules = \XDev\Application::getInstance()->getModules();

        $rows = [];

        foreach ($modules as $module) {
            $rows[] = [ConsoleHelper::colorize($module->getPackageName(), ConsoleHelper::FG_GREEN), $module->getModuleDescription()];
        }

        $table = new Table($output);
        $table->setStyle('compact');

        $table
            ->setRows($rows)
            ->render()
        ;

        $output->writeln('');
    }

}