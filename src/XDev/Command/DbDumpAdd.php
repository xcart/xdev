<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Helper\Table;

/**
 * Class DbDumpAdd
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */

class DbDumpAdd extends \XDev\Base\Command\ASoftwareRelative
{

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('dd:add')
            ->setDescription('Includes certain tabel into the SQL dump')
            ->addArgument('table', InputArgument::OPTIONAL, 'MySQL table name')
           ->addOption(
                'schema-only',
                '-s',
                InputOption::VALUE_OPTIONAL,
                'Add only table schema into the SQL dump',
                false
            )
            ->addOption(
                'all',
                '-a',
                InputOption::VALUE_OPTIONAL,
                'Adds all tables from dumpp',
                false
            )
            ;
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $table_name = $input->getArgument('table');

        $dumpCnf = \XDev::getConfig('DbDump');

        if ($table_name !== null || $input->getOption('all') === false) {

            $dumpCnf->addTable($table_name, $input->getOption('schema-only') !== false);
            $dumpCnf->save();
 
        } elseif ($table_name === null && $input->getOption('all') !== false) {

            $db = \XDev::getDB();

            foreach ($db->getTables() as $k => $table_name) {

                $dumpCnf->addTable($table_name, $input->getOption('schema-only') !== false);

            }

            $dumpCnf->save();
        }

    }
}
