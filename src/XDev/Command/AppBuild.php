<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

use Symfony\Component\Console\Command\Command;
use XDev\Utils\Filesystem;

/**
 * Build command 
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class AppBuild extends Command {

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:build')
            ->setDescription('Builds phar files for core and modules')
        ;
    }
    protected function defineBoxConfig() 
    {
        return [
            'alias'         => 'xdev.phar',
            'alias_live'    => 'xdev_live.phar',
            'chmod'         => '0755',
            'directories'   => ['var'],
            'files'         => [
                '{xdev_dir_rel_base}installed_modules.yaml',
                '{xdev_dir_rel_base}software_connector.php',
            ],
            'finder' => [
                'workdir' => [
                    'name'          => '*.php',
                    'exclude'       => ['vendor'],
                    'exclude_live'  => ['vendor', 'XDev/Dev'],
                    'in' => 'src'
                ],
                'vendor' => [
                    'name'          => '*.php',
                    'exclude'       => ['Tests'],
                    'exclude_live'  => ['Tests', '{xdev_dir_rel_vendor}src/XDev/Dev'],
                    'in'            => 'vendor'
                ]
            ],
            'main' => '{xdev_dir_rel_base}bin/xdev',
            'output' => '{xdev_script}',
            'output_live' => '{xdev_live_script}',
            'stub' => true
        ];

    }

    protected function buildDev() 
    {
        return $this->build();
    }

    protected function buildLive() 
    {
        return $this->build(true);
    }

    protected function build($live_mode = false) 
    {
        $box_config = $this->defineBoxConfig();

        if (Filesystem::exists(\XDev\Config::getSelfSourcedir() . \XDev::DS . 'vendor')) {
            // Project was cloned
            $box_base_dir = realpath(\XDev\Config::getSelfSourcedir());

            $xdev_dir_rel = '';
            $xdev_dir_rel_vendor = '';

        } else {
            // Project was installed by composer
            $box_base_dir = realpath(\XDev\Config::getVendorDir() . \XDev::DS . '..');
            $box_config['directories'] = [];
            unset($box_config['finder']['workdir']);

            $xdev_dir_rel = rtrim(Filesystem::makePathRelative(\XDev\Config::getSelfSourceDir(), $box_base_dir), \XDev::DS);
            $xdev_dir_rel_vendor = rtrim(Filesystem::makePathRelative(\XDev\Config::getSelfSourceDir(), $box_base_dir . \XDev::DS . 'vendor'), \XDev::DS);
        }

        $vendor_dir  = $box_base_dir . \XDev::DS . 'vendor';

        $xdev_dir_rel = $xdev_dir_rel ? $xdev_dir_rel . \XDev::DS : '';
        $xdev_dir_rel_vendor = $xdev_dir_rel_vendor ? $xdev_dir_rel_vendor . \XDev::DS : '';
        $xdev_filename = \XDev\Application::getInstance()->getCompiledScriptPathXDev();
        $xdev_live_filename = \XDev\Application::getInstance()->getCompiledScriptPathXDevLive();

        array_walk_recursive($box_config, function (&$item, $key) use($xdev_dir_rel, $xdev_dir_rel_vendor, $xdev_filename, $xdev_live_filename)
        {
            if (is_string($item)) {
                $item = str_replace('{xdev_dir_rel_base}', $xdev_dir_rel, $item);
                $item = str_replace('{xdev_dir_rel_vendor}', $xdev_dir_rel_vendor, $item);
                $item = str_replace('{xdev_script}', $xdev_filename, $item);
                $item = str_replace('{xdev_live_script}', $xdev_live_filename, $item);
            }
        });

        $config_filename = 'box.json';

        if ($live_mode) {
            $box_config['alias'] = $box_config['alias_live'];
            $box_config['output'] = $box_config['output_live'];

            foreach ($box_config['finder'] as $key => $value) {
                if (isset($box_config['finder'][$key]['exclude_live'])) {
                    $box_config['finder'][$key]['exclude'] = $box_config['finder'][$key]['exclude_live'];
                }
            }

            $config_filename = 'box.live.json';

            foreach (\XDev\Application::getInstance()->getModules() as $module) {
                $module_relative_path = Filesystem::makePathRelative($module->getModuleDir(), $vendor_dir);

                $box_config['finder']['vendor']['exclude'][] = $module_relative_path . 'Dev';
            }

        }

        unset($box_config['alias_live']);
        unset($box_config['output_live']);

        foreach ($box_config['finder'] as $key => $value) {
            if (isset($box_config['finder'][$key]['exclude'])) {
                unset($box_config['finder'][$key]['exclude_live']);
            }
        }

        $box_config['finder'] = array_values($box_config['finder']);

        $config_filename_full = $box_base_dir . \XDev::DS . $config_filename;

        Filesystem::dumpFile($config_filename_full, json_encode($box_config, JSON_PRETTY_PRINT));

        \XDev\Utils\Shell::exec("cd $box_base_dir; box build -c $config_filename_full");
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        \XDev\Application::getInstance()->buildCache();

        $current_dir = getcwd();

        $this->buildDev();
        $this->buildLive();

    }

}