<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class FilesDump
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class FilesDump extends \XDev\Base\Command\ASoftwareRelative
{
    const COMPRESS_GZIP = 'gzip';
    const COMPRESS_BZIP = 'bzip';
    const COMPRESS_NONE = 'none';

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('files:dump')
            ->setDescription('Creates a database dump')
            ->addOption(
                'compress',
                '-c',
                InputOption::VALUE_OPTIONAL,
                'Compression method (bzip, gzip)',
                self::COMPRESS_GZIP
            )
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln('Creating files dump...');

        $db = \XDev\Core\Db::getInstance();

        $database = \XDev::getConfig('db')->database;

        $query = (
            'SELECT TABLE_NAME'
            . ' FROM information_schema.TABLES'
            . " WHERE table_schema = '$database'"
        );

        $tables = $db->queryColumnn($query);

        $excluded_tables = \XDev::getConfig('dbDump')->excludeTables;

        $tables_scheme_only = $tables_data_only = [];

        foreach ($tables as $k => $table) {

            if (isset($excluded_tables[$table])) {
                if (isset($excluded_tables[$table]['S']) && $excluded_tables[$table]['S'] == 'Y') {
                    $tables_scheme_only[] = $table;
                }

            } else {
                $tables_scheme_only[] = $table;
                $tables_data_only[] = $table;
            }

        }

        $dbConfig = \XDev::getConfig('db');

        $mysql_str = sprintf('mysqldump -u%s -p%s -h%s %s %s ',
            $dbConfig->username,
            escapeshellcmd($dbConfig->password),
            $dbConfig->host,
            $dbConfig->port ? ('-P' . $dbConfig->port) : '',
            $dbConfig->database
        );

        $cmd = [];

        $cmd[] = $mysql_str . implode(' ', $tables_scheme_only). ' --no-data';
        $cmd[] = $mysql_str . implode(' ', $tables_data_only). '  --no-create-info';

        $cmp = $ext = '';

        $filename = \XDev\Config\DbDump::getDumpFilename();

        if ($input->getOption('compress') == self::COMPRESS_GZIP) {
            $cmp = ' | gzip';
            $filename .= '.gz';

        } elseif ($input->getOption('compress') == self::COMPRESS_BZIP) {
            $filename .= '.bz2';
            $cmp = ' | bzip2 -z';
        }

        $command = sprintf('(%s)%s > %s', implode(' && ', $cmd), $cmp, $filename);

        $sqlDir = \XDev\Config::getSQLDir();

        $fileSystem = new Filesystem();

        if (!$fileSystem->exists($sqlDir)) {

             $fileSystem->mkdir($sqlDir, 0700);
        }

        \XDev\Utils\Shell::exec($command);

        $output->writeln('<fg=yellow;options=bold>Dump succesfully created: </><fg=green;>' . $filename . '</>');

    }
}
