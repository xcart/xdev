<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Helper\TableStyle;
use XDev\Core\Symfony\Console\Helper;
use XDev\Config\DbDump as DbDumpConfig;

/**
 * Class DbDumpStatus
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class DbDumpStatus extends \XDev\Base\Command\ASoftwareRelative
{
    const MAX_DISPLAY_ROWS = 10;

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('dd:status')
            ->setDescription('Displays the database dump settings')
            ->addOption(
                'all',
                '-a',
                InputOption::VALUE_OPTIONAL,
                'Display all rows in tables',
                false
            )
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $db = \XDev::getDB();

        $dbCfg = \XDev::getConfig('db');

        $query = (
            'SELECT TABLE_NAME, ENGINE, TABLE_ROWS, (data_length + index_length) AS size '
            . ' FROM information_schema.TABLES'
            . " WHERE table_schema = '{$dbCfg->getDatabase()}'"
            . ' ORDER BY (data_length + index_length) DESC;'
        );

        $rows = $rows_skipped = [];
        $excluded_tables = \XDev::getConfig('DbDump')->getParam(DbDumpConfig::PARAM_EXCLUDE_TABLES);
        $size = 0;
        $total_size = 0;

        foreach ($db->query($query) as $row) {
            $total_size += $row['size'];

            $row['schema'] = 'X';
            $row['data'] = 'X';

            $skip_table = false;
            $is_no_data = false;

            $color = false;

            if (isset($excluded_tables[$row['TABLE_NAME']])) {

                if (isset($excluded_tables[$row['TABLE_NAME']]['S']) && $excluded_tables[$row['TABLE_NAME']]['S'] == 'Y') {
                    $row['schema'] = 'X';
                    $color = '244'; // Light gray
                    $is_no_data = true;
                } else {
                    $row['schema'] = '';
                    $color = Helper::FG_DARK_GRAY; // Dark gray
                    $skip_table = true;
                }

                $row['data'] = '';

            } else {
                $size += $row['size'];

            }

            $row['size'] = round($row['size'] / 1024 / 1024, 2);

            if ($color) {
                $row = array_map(function ($v) use ($color) {
                    return Helper::colorize($v, $color);
                }, $row);
            }

            if ($skip_table) {
                $rows_skipped[] = $row;
            } else {
                $rows[] = $row;
            }
        }

        $rows = array_merge($rows, $rows_skipped);

        $total_rows = count($rows);
        $total_size = round($total_size / 1024 / 1024, 2);
        $size =  round($size / 1024 / 1024, 2);

        if ($total_rows > self::MAX_DISPLAY_ROWS && $input->getOption('all') === false) {
            $rows = array_slice($rows, 0, 10);
            $rows[] = ['...','...','...','...','...','...'];
            $rows[] = [new TableCell('', ['colspan' => 6])];
            $rows[] = [new TableCell("               --- To load all items ($total_rows) use -a option ---", ['colspan' => 6])];
        }

        $rows[] = new TableSeparator();
        $rows[] = [new TableCell(Helper::colorize('Total:', Helper::FG_GREEN), ['colspan' => 3]), new TableCell(Helper::colorize($size . ' / ' . $total_size, Helper::FG_GREEN), ['colspan' => 3])];

        $output->writeln('');
        $output->writeln('Dump tables:');

        $table = new Table($output);
        $style = new TableStyle();
        $style->setPadType(STR_PAD_BOTH);

        $table
            ->setHeaders(['Table', 'Engine', 'Rows', 'Size (Mb)', 'Schema', 'Data'])
            ->setRows($rows)
            ->setColumnStyle(4, $style)
            ->setColumnStyle(5, $style)
        ;

        $table->render();

        $output->writeln('');
        $output->writeln('Dump hooks:');

        if (\XDev::getConfig('DbDump')->getParam(DbDumpConfig::PARAM_MODIFIERS)) {
            $table = new Table($output);

            $rows = [];

            foreach (\XDev::getConfig('dbDump')->getParam(DbDumpConfig::PARAM_MODIFIERS) as $table_name => $columns) {
                foreach ($columns as $column_name => $modifiers) {
                    $rows[] = [$table_name, $column_name, $modifiers[0], isset($modifiers[1]) ? $modifiers[1] : ''];
                }
            }

            $table
                ->setHeaders(['Table', 'Column', 'Modifier', 'Condition'])
                ->setRows($rows)
            ;

            $table->render();

        } else {
            $output->writeln('No hooks defined.');
        }

        $output->writeln('');

    }
}
