<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

use XDev\EM;

/**
 * XDev
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 *
 */

class XDev
{
    const VERSION = '0.1.0';

    const DS = DIRECTORY_SEPARATOR;

    static $config;
    static $engine;
    static $engineDetector;
    static $db;
    static $detectSoftwareError = false;
    static $detectSoftwareErrorMessage = '';
    static $softwareDir = '';

    public static function getESD()
    {
        if (!self::$engineDetector) {
            self::$engineDetector = new \XDev\Core\EngineDetector();
        }

        return self::$engineDetector;
    }

    public static function getDetectSoftwareError()
    {
        return self::$detectSoftwareError;
    }

    public static function getDetectSoftwareErrorMessage()
    {
        return self::$detectSoftwareErrorMessage;
    }

    public static function setSoftwareDir($dir)
    {
        self::$softwareDir = $dir;
    }

    public static function getSoftwareDir()
    {
        return self::$softwareDir;
    }

    public static function addSoftwareEngine($class_name)
    {
        self::getESD()->addEngine($class_name);
    }

    public static function detectSoftwareEngine()
    {
        self::getESD()->setSearchDir(self::getSoftwareDir());

        return self::getESD()->detectEngine();

    }

    public static function setSoftwareEngine($engine)
    {

        if (self::$engine) {
            throw new \Exception('Engine has already been set');
        }

        EM::setEngineCode($engine->getCode());

        self::$engine = $engine;
    }

    public static function detectSoftware()
    {
        try {

            if (!self::isInitialized()) {
                $engine = self::detectSoftwareEngine();

                if (!$engine) {
                    throw new \Exception('Cannot detect software engine');

                } else {
                    self::setSoftwareEngine($engine);

                    $version = $engine->detectVersion();

                    if (!$version) {
                        throw new \Exception('Cannot detect software version');
                    }

                    self::setSoftwareVersion($version);
                }

            } else {

                if (!\XDev\Config\Main::getInstance()->configFileExists()) {
                    throw new \Exception('Cannot find the configuration file ' . \XDev\Config\Main::getInstance()->getConfigFilenameFull());
                }

                $engineCode = \XDev\Config\Main::getInstance()->getParam('engine');
                $version = \XDev\Config\Main::getInstance()->getParam('version');

                $engine = \XDev::getESD()->getEngineByCode($engineCode);

                self::setSoftwareEngine($engine);
                self::setSoftwareVersion($version);
           }

           self::$detectSoftwareErrorMessage = '';
           self::$detectSoftwareError = false;

           \XDev\Core\Hook::invoke('after-detect-software');

        } catch (\Exception $e) {
            self::$detectSoftwareError = true;
            self::$detectSoftwareErrorMessage = $e->getMessage();
        }

    }

    public static function getSoftwareEngineCode()
    {
        return self::getSoftwareEngine()->getCode();
    }

    public static function getSoftwareVersion()
    {
        if (!self::getSoftwareEngine()) {
            self::engineNotSetExeption();
        }

        return EM::getVersion();
    }

    public static function getSoftwareName()
    {
        if (!self::getSoftwareEngine()) {
            self::engineNotSetExeption();
        }

        return EM::get('API')->getSoftwareName();
    }

    public static function setSoftwareVersion($version)
    {

        if (!self::getSoftwareEngine()) {
            self::engineNotSetExeption();
        }

        EM::setVersion($version);

    }

    public static function getSoftwareEngine()
    {

        return self::$engine;

    }

    public static function getDB()
    {

        if (!self::$db) {
            $dbCfg = \XDev::getConfig('db');

            try {

                self::$db = new \XDev\Core\DbConnection(
                    $dbCfg->getParam('host'),
                    $dbCfg->getParam('username'),
                    $dbCfg->getParam('password'),
                    $dbCfg->getParam('database'),
                    $dbCfg->getParam('port'),
                    $dbCfg->getParam('socket')
                );

            } catch (\Exception $e) {

                throw new \Symfony\Component\Console\Exception\RuntimeException (
                    $e->getMessage() . "\n\n"
                    . 'Please specify the correct database access credentials in ' . $dbCfg->getConfigFilename()
                );
            }

        }

        return self::$db;

    }

    public static function isInitialized()
    {
        return self::getConfig('Local')->configFileExists();
    }

    public static function getCurrentInstance()
    {
        return self::getConfig('Local')->getCurrentInstance();
    }

    public static function loadHooks($name)
    {
        if (self::getCurrentInstance() != \XDev\Config\Main::INSTANCE_NAME_LIVE) {
            $filename = \XDev\Config::getXDevCommonDevDir() . \XDev::DS . 'hooks' . \XDev::DS . $name . '.php';
            include_once $filename;
        }

    }

    public static function getConfig($name)
    {
        if (!self::$config) {
            self::$config = new \XDev\Config();
        }

        $prop = lcfirst($name);

        return self::$config->getNode($prop);
    }

    private static function engineNotSetExeption()
    {
        throw new \Exception('Software engine has not been set. Use setSoftwareEngine() method.');
    }
}
