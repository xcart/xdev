# README #

## Installation

 1. `git clone` _this_ repository.
 2. Download composer: `curl -s https://getcomposer.org/installer | php`
 3. Install dependencies: `php composer.phar install`
 
## Global installation

Run `composer global require xcart/xdev`

## Creating a PHAR package

Simply run the following 

```
./bin/xdev app:build
```

After that you can find the *.phar files in the `output` folder.

## Usage

You can run commands as  

`./bin/xdev <command>`
or  
`php ./output/xdev.phar <command>` 
